package by.epam.javatraining.zarenok.finalproject.dao.impl;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import util.Util;
import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IUserDAO;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.entity.User;
import by.epam.javatraining.zarenok.finalproject.manager.util.ManagerUtil;

public class TestUserDAO {

	private Connection connection;
	private IUserDAO userDAO;
	private User user;

	/**
	 * Creates DAO object and add user to database.
	 */
	@Before
	public void setUp() throws Exception {
		connection = Util.getConnection();
		userDAO = ManagerUtil.getDaoFactory().getUserDAO(connection);
		user = generateUser();
		int id = userDAO.add(user);
		user.setId(id);
	}

	@After
	public void tearDown() throws Exception {
		Util.tearDown(connection);
		connection = null;
		userDAO = null;
	}

	/**
	 * Tests {@link UserDAO#findByLoginPassword(String, String)} method.
	 */
	@Test
	public void testFindByLoginPassword() throws DAOException {
		assertEquals(user, userDAO.findByLoginPassword(user.getLogin(),
				user.getPassword()));
	}

	/**
	 * Tests {@link UserDAO#findByLogin(String)} method.
	 * 
	 * @throws DAOException
	 */
	@Test
	public void testFindByLogin() throws DAOException {
		assertEquals(user, userDAO.findByLogin(user.getLogin()));
	}

	/**
	 * Tests {@link UserDAO#findById(int)} method.
	 */
	@Test
	public void testFindById() throws DAOException {
		assertEquals(user, userDAO.findById(user.getId()));
	}

	private User generateUser() {
		User user = new User();
		String login = "testUserDaoLogin";
		user.setLogin(login);
		String password = "testUserDaoPass";
		user.setPassword(password);
		user.setRole(Role.CLIENT);
		return user;
	}
}