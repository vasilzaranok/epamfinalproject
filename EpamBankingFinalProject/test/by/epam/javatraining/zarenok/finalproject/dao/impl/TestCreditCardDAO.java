package by.epam.javatraining.zarenok.finalproject.dao.impl;

import static org.junit.Assert.assertEquals;

import java.sql.Connection;
import java.text.ParseException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import util.Util;
import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IAccountInfoDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IClientInfoDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.ICreditCardDAO;
import by.epam.javatraining.zarenok.finalproject.entity.CreditCard;
import by.epam.javatraining.zarenok.finalproject.manager.util.ManagerUtil;

public class TestCreditCardDAO {

	private Connection connection;
	private ICreditCardDAO cardDAO;
	private IAccountInfoDAO accountDAO;
	private IClientInfoDAO clientDAO;

	@Before
	public void setUp() throws Exception {
		connection = Util.getConnection();
		cardDAO = ManagerUtil.getDaoFactory().getCreditCardDAO(connection);
		accountDAO = ManagerUtil.getDaoFactory().getAccountInfoDAO(connection);
		clientDAO = ManagerUtil.getDaoFactory().getClientInfoDAO(connection);
	}

	@After
	public void tearDown() throws Exception {
		Util.tearDown(connection);
		connection = null;
		cardDAO = null;
	}
	
	/**
	 * Tests {@link CreditCardDAO#findById(int)} method.
	 * @throws DAOException
	 * @throws ParseException
	 */
	@Test
	public void testFindById() throws DAOException, ParseException {
		int clientId = clientDAO.add(Util.generateClientInfo());
		int accountId = accountDAO.add(Util.generateAccount(), clientId);
		CreditCard card = Util.generateCard();
		int cardId = cardDAO.add(card, accountId);
		card.setId(cardId);
		assertEquals(card, cardDAO.findById(cardId));
	}
}
