package by.epam.javatraining.zarenok.finalproject.dao.impl;

import static org.junit.Assert.*;

import java.sql.Connection;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import util.Util;
import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IAccountInfoDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IClientInfoDAO;
import by.epam.javatraining.zarenok.finalproject.entity.AccountInfo;
import by.epam.javatraining.zarenok.finalproject.entity.ClientInfo;
import by.epam.javatraining.zarenok.finalproject.manager.util.ManagerUtil;

public class TestAccountInfoDAO {

	private Connection connection;
	private IAccountInfoDAO accountDAO;
	private IClientInfoDAO clientDAO;

	@Before
	public void setUp() throws Exception {
		connection = Util.getConnection();
		accountDAO = ManagerUtil.getDaoFactory().getAccountInfoDAO(connection);
		clientDAO = ManagerUtil.getDaoFactory().getClientInfoDAO(connection);
	}

	@After
	public void tearDown() throws Exception {
		Util.tearDown(connection);
		connection = null;
		accountDAO = null;
	}

	/**
	 * Tests {@link AccountInfoDAO#findById(int)} method.
	 * 
	 * @throws DAOException
	 */
	@Test
	public void testFindById() throws DAOException {
		AccountInfo account = Util.generateAccount();
		ClientInfo client = Util.generateClientInfo();
		int clientId = clientDAO.add(client);
		int id = accountDAO.add(account, clientId);
		account.setId(id);
		assertEquals(account, accountDAO.findById(id));
	}
}
