package by.epam.javatraining.zarenok.finalproject.dao.impl;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.util.List;
import java.util.Locale;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import util.Util;
import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IPaymentTemplateDAO;
import by.epam.javatraining.zarenok.finalproject.entity.PaymentTemplate;
import by.epam.javatraining.zarenok.finalproject.manager.util.ManagerUtil;

public class TestPaymentTemplateDAO {

	private static final Locale DEFAULT_TEST_LOCALE = Locale.US;
	private IPaymentTemplateDAO paymentTemplateDAO;
	private Connection connection;

	@Before
	public void setUp() throws Exception {
		connection = Util.getConnection();
		paymentTemplateDAO = ManagerUtil.getDaoFactory().getPaymentTemplateDAO(
				connection);
	}

	@After
	public void tearDown() throws Exception {
		Util.tearDown(connection);
		paymentTemplateDAO = null;
	}

	/**
	 * Tests {@link PaymentTemplateDAO#findAll(Locale)} and
	 * {@link PaymentTemplateDAO#findById(int, Locale)} methods.
	 */
	@Test
	public void testFind() throws DAOException {
		List<PaymentTemplate> list = paymentTemplateDAO
				.findAll(DEFAULT_TEST_LOCALE);
		if (list.isEmpty()) {
			fail("Unable to run test, there are"
					+ " no payment templates in database");
		}
		PaymentTemplate pTemplate = list.get(0);
		PaymentTemplate pTemplateActual = paymentTemplateDAO.findById(
				pTemplate.getId(), DEFAULT_TEST_LOCALE);
		assertEquals(pTemplate, pTemplateActual);
	}
}
