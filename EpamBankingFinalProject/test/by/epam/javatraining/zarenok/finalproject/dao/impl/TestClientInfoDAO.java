package by.epam.javatraining.zarenok.finalproject.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.sql.Connection;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import util.Util;
import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IClientInfoDAO;
import by.epam.javatraining.zarenok.finalproject.entity.ClientInfo;
import by.epam.javatraining.zarenok.finalproject.manager.util.ManagerUtil;

public class TestClientInfoDAO {

	private Connection connection;
	private IClientInfoDAO clientDAO;
	private ClientInfo client;
	
	/**
	 * Create DAO object and add client to database.
	 */
	@Before
	public void setUp() throws Exception {
		connection = Util.getConnection();
		clientDAO = ManagerUtil.getDaoFactory().getClientInfoDAO(connection);
		client = generateClientInfo();
		int id = clientDAO.add(client);
		client.setId(id);
	}

	@After
	public void tearDown() throws Exception {
		Util.tearDown(connection);
		clientDAO = null;
		client = null;
	}
	
	/**
	 * Tests {@link ClientInfoDAO#findById(int)} method.
	 */
	@Test
	public void testFindById() throws DAOException {
		assertEquals(client, clientDAO.findById(client.getId()));
	}
	
	/**
	 * Tests {@link ClientInfoDAO#findAll()} method.
	 */
	@Test
	public void testFindAll() throws DAOException {
		List<ClientInfo> list = clientDAO.findAll();
		assertTrue(list.contains(client));
	}
	
	/**
	 * Tests {@link ClientInfoDAO#update(ClientInfo)} method.
	 */
	@Test
	public void testUpdate() throws DAOException {
		String firstname = "testClientInfoDAOFN2";
		client.setFirstname(firstname);
		String middlename = "testClientInfoDAOMN2";
		client.setMiddlename(middlename);
		String passportNumber = "testClientInfoDAOPN2";
		client.setPassportNumber(passportNumber);
		String surname = "testClientInfoDAOSN2";
		client.setSurname(surname);
		clientDAO.update(client);
		assertEquals(client, clientDAO.findById(client.getId()));
	}
	
	private ClientInfo generateClientInfo() {
		return Util.generateClientInfo();
	}
}
