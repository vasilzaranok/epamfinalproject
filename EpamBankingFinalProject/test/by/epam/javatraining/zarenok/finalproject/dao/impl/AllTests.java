package by.epam.javatraining.zarenok.finalproject.dao.impl;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import by.epam.javatraining.zarenok.finalproject.db.util.ConnectionPoolManager;

@RunWith(Suite.class)
@SuiteClasses({ TestAccountInfoDAO.class, TestClientInfoDAO.class,
		TestCreditCardDAO.class, TestUserDAO.class, TestPaymentDAO.class,
		TestPaymentTemplateDAO.class })
public class AllTests {

	@BeforeClass
	public static void setUp() {
		ConnectionPoolManager.startPool();
	}

	@AfterClass
	public static void tearDown() {
		ConnectionPoolManager.stopPool();
	}
}
