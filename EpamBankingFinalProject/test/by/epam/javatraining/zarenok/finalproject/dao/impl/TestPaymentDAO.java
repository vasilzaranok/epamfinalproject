package by.epam.javatraining.zarenok.finalproject.dao.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.sql.Connection;
import java.text.ParseException;
import java.util.List;
import java.util.Locale;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import util.Util;
import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IAccountInfoDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IClientInfoDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.ICreditCardDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IPaymentInfoDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IPaymentTemplateDAO;
import by.epam.javatraining.zarenok.finalproject.entity.CreditCard;
import by.epam.javatraining.zarenok.finalproject.entity.PaymentInfo;
import by.epam.javatraining.zarenok.finalproject.entity.PaymentTemplate;
import by.epam.javatraining.zarenok.finalproject.manager.util.ManagerUtil;

public class TestPaymentDAO {

	private static final Locale DEFAULT_LOCALE = Locale.US;
	private Connection connection;
	private IPaymentInfoDAO paymentInfoDAO;
	private ICreditCardDAO cardDAO;
	private IAccountInfoDAO accountDAO;
	private IClientInfoDAO clientDAO;
	private IPaymentTemplateDAO paymentTemplateDAO;
	private int clientId;
	private int cardId;

	/**
	 * Create DAO objects and add client with account and credit card to
	 * database for payments testing.
	 * 
	 * @throws Exception
	 */
	@Before
	public void setUp() throws Exception {
		connection = Util.getConnection();
		paymentInfoDAO = ManagerUtil.getDaoFactory().getPaymentDAO(connection);
		cardDAO = ManagerUtil.getDaoFactory().getCreditCardDAO(connection);
		accountDAO = ManagerUtil.getDaoFactory().getAccountInfoDAO(connection);
		clientDAO = ManagerUtil.getDaoFactory().getClientInfoDAO(connection);
		paymentTemplateDAO = ManagerUtil.getDaoFactory().getPaymentTemplateDAO(
				connection);
		clientId = clientDAO.add(Util.generateClientInfo());
		int accountId = accountDAO.add(Util.generateAccount(), clientId);
		CreditCard card = Util.generateCard();
		cardId = cardDAO.add(card, accountId);
	}

	@After
	public void tearDown() throws Exception {
		Util.tearDown(connection);
	}

	/**
	 * Test {@link PaymentInfoDAO#add(int, int, PaymentInfo)} and
	 * {@link PaymentInfoDAO#delete(int)} methods. Checks if payment was added,
	 * and after that if that payment was deleted.
	 */
	@Test
	public void testAddDelete() throws DAOException, ParseException {
		PaymentInfo payment = Util.generatePaymentInfo();
		int paymentTemplateId = getPaymentTemplateId();
		paymentInfoDAO.add(cardId, paymentTemplateId, payment);
		List<PaymentInfo> payments = paymentInfoDAO.findByClientId(clientId);
		int expectedListSize = 1;
		assertEquals(expectedListSize, payments.size());
		paymentInfoDAO.delete(payments.get(0).getId());
		assertTrue(paymentInfoDAO.findByClientId(clientId).isEmpty());
	}

	private int getPaymentTemplateId() throws DAOException {
		List<PaymentTemplate> pTemplates = paymentTemplateDAO
				.findAll(DEFAULT_LOCALE);
		if (pTemplates.isEmpty()) {
			fail("Unable to run test, there are "
					+ "no payment templates in database");
		}
		int paymentTemplateId = pTemplates.get(0).getId();
		return paymentTemplateId;
	}
}
