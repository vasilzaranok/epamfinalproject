package by.epam.javatraining.zarenok.finalproject.manager;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import util.Util;
import by.epam.javatraining.zarenok.finalproject.entity.AccountInfo;
import by.epam.javatraining.zarenok.finalproject.entity.ClientInfo;
import by.epam.javatraining.zarenok.finalproject.entity.CreditCard;
import by.epam.javatraining.zarenok.finalproject.entity.Payment;
import by.epam.javatraining.zarenok.finalproject.entity.PaymentInfo;
import by.epam.javatraining.zarenok.finalproject.entity.PaymentTemplate;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.AddEntityResult;
import by.epam.javatraining.zarenok.finalproject.manager.util.ManagerFactory;

public class TestPaymentManager {

	private static final Locale DEFAULT_TEST_LOCALE = Locale.US;
	private static PaymentManager paymentManager = ManagerFactory.INSTANCE
			.getPaymentManager();
	private static ClientManager clientManager = ManagerFactory.INSTANCE
			.getClientManager();
	private static AccountManager accountManager = ManagerFactory.INSTANCE
			.getAccountManager();
	private int clientId;
	private int creditCardId;
	private int accountId;

	/**
	 * Add client with account and credit card to database.
	 */
	@Before
	public void setUp() throws Exception {
		ClientInfo clientInfo = Util.generateClientInfo();
		AddEntityResult result = clientManager.addClient(clientInfo);
		clientId = result.getId();
		AccountInfo accountInfo = Util.generateAccount();
		result = accountManager.addAccount(accountInfo, clientId);
		accountId = result.getId();
		CreditCard card = Util.generateCard();
		result = accountManager.addCard(card, accountId);
		creditCardId = result.getId();
	}

	@After
	public void tearDown() throws Exception {
		Util.deleteTestClient(clientId);
	}

	/**
	 * Tests make payment. Checks if balance was decreased by payment sum.
	 */
	@Test
	public void testMakePayment() throws ManagerException {
		PaymentInfo payment = Util.generatePaymentInfo();
		BigDecimal balanceBefore = accountManager.getAccountById(accountId)
				.getAccountInfo().getBalance();
		makePayment(payment);
		BigDecimal balanceAfter = accountManager.getAccountById(accountId)
				.getAccountInfo().getBalance();
		BigDecimal sum = balanceBefore.subtract(balanceAfter);
		assertEquals(0, payment.getSum().compareTo(sum));
	}

	/**
	 * Tests cancel payment. Checks if payment was delete from database and
	 * checks balance.
	 */
	@Test
	public void testCancelPayment() throws ManagerException {
		PaymentInfo payment = Util.generatePaymentInfo();
		BigDecimal balanceBefore = accountManager.getAccountById(accountId)
				.getAccountInfo().getBalance();
		makePayment(payment);
		cancelPayment();
		BigDecimal balanceAfter = accountManager.getAccountById(accountId)
				.getAccountInfo().getBalance();
		List<Payment> payments = paymentManager.getPayments(clientId,
				DEFAULT_TEST_LOCALE);
		assertTrue(payments.isEmpty());
		assertEquals(0, balanceBefore.compareTo(balanceAfter));
	}

	/**
	 * Cancel first payment.
	 */
	private void cancelPayment() throws ManagerException {
		List<Payment> payments = paymentManager.getPayments(clientId,
				DEFAULT_TEST_LOCALE);
		PaymentInfo paymentInfo = payments.get(0).getPaymentInfo();
		paymentManager.cancelPayment(paymentInfo.getId());
	}

	private void makePayment(PaymentInfo payment) throws ManagerException {
		List<PaymentTemplate> list = paymentManager
				.getPaymentTemplates(DEFAULT_TEST_LOCALE);
		if (!list.isEmpty()) {
			PaymentTemplate pTemplate = list.get(0);
			int pTemplateId = pTemplate.getId();
			paymentManager.makePayment(creditCardId, pTemplateId, payment);
		} else {
			fail("Unable to run test because there are"
					+ " no payment templates in database");
		}
	}
}