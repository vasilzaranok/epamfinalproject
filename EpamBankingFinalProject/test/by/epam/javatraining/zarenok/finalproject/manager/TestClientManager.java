package by.epam.javatraining.zarenok.finalproject.manager;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import util.Util;
import by.epam.javatraining.zarenok.finalproject.entity.ClientInfo;
import by.epam.javatraining.zarenok.finalproject.entity.User;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.AddEntityResult;
import by.epam.javatraining.zarenok.finalproject.manager.util.ManagerFactory;

public class TestClientManager {

	private static ClientManager clientManager = ManagerFactory.INSTANCE
			.getClientManager();
	private static UserManager userManager = ManagerFactory.INSTANCE
			.getUserManager();
	private int clientId;
	private ClientInfo clientInfo;

	/**
	 * Add client to database.
	 */
	@Before
	public void setUp() throws Exception {
		clientInfo = Util.generateClientInfo();
		AddEntityResult result = clientManager.addClient(clientInfo);
		clientId = result.getId();
		clientInfo.setId(clientId);
	}

	@After
	public void tearDown() throws Exception {
		Util.deleteTestClient(clientId);
	}

	/**
	 * Test sign in up client. Checks if user was linked to related client.
	 */
	@Test
	public void testSignUp() throws ManagerException {
		User user = generateUser();
		clientManager.signUpClient(clientInfo, user);
		user = userManager.getUser(user.getLogin(), user.getPassword());
		assertEquals(clientInfo, clientManager.getClientInfoByUser(user));
	}

	private User generateUser() {
		User user = new User();
		user.setLogin("11testLogin");
		user.setPassword("pass");
		return user;
	}

	/**
	 * Tests update client data. Checks if data was changed.
	 */
	@Test
	public void testUpdateData() throws ManagerException {
		String firstname = "testNewFN";
		clientInfo.setFirstname(firstname);
		String middlename = "testNewMN";
		clientInfo.setMiddlename(middlename);
		String surname = "testNewSN";
		clientInfo.setSurname(surname);
		String passportNumber = "testNewPN";
		clientInfo.setPassportNumber(passportNumber);
		clientManager.updateClientData(clientInfo);
		assertEquals(clientInfo, clientManager.getClientInfoById(clientId));
	}
}