package by.epam.javatraining.zarenok.finalproject.manager;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Locale;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import util.Util;
import by.epam.javatraining.zarenok.finalproject.entity.AccountInfo;
import by.epam.javatraining.zarenok.finalproject.entity.ClientInfo;
import by.epam.javatraining.zarenok.finalproject.entity.CreditCard;
import by.epam.javatraining.zarenok.finalproject.entity.PaymentInfo;
import by.epam.javatraining.zarenok.finalproject.entity.PaymentTemplate;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.AddEntityResult;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.PaymentResult;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.PutMoneyResult;
import by.epam.javatraining.zarenok.finalproject.manager.util.ManagerFactory;

public class TestAccountManager {

	private static final Locale DEFAULT_TEST_LOCALE = Locale.US;
	private static PaymentManager paymentManager = ManagerFactory.INSTANCE
			.getPaymentManager();
	private static ClientManager clientManager = ManagerFactory.INSTANCE
			.getClientManager();
	private static AccountManager accountManager = ManagerFactory.INSTANCE
			.getAccountManager();
	private int clientId;
	private int creditCardId;
	private int accountId;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	/**
	 * Add client with account and credit card in database to tests account
	 * logic.
	 * 
	 */
	@Before
	public void setUp() throws Exception {
		ClientInfo clientInfo = Util.generateClientInfo();
		AddEntityResult result = clientManager.addClient(clientInfo);
		clientId = result.getId();
		AccountInfo accountInfo = Util.generateAccount();
		result = accountManager.addAccount(accountInfo, clientId);
		accountId = result.getId();
		CreditCard card = Util.generateCard();
		result = accountManager.addCard(card, accountId);
		creditCardId = result.getId();
	}

	@After
	public void tearDown() throws Exception {
		Util.deleteTestClient(clientId);
	}

	/**
	 * Tests blocking and unblocking account. Checks that if account is blocked,
	 * client can't make payment, also checks that if account id unblocked
	 * client may make payment.
	 */
	@Test
	public void testBlockingAccount() throws ManagerException {
		accountManager.blockAccount(accountId);
		PaymentResult result = makePayment();
		assertEquals(result, PaymentResult.ACCOUNT_BLOCKED);
		accountManager.unblockAccount(accountId);
		PaymentResult resultAfterUnblock = makePayment();
		assertEquals(resultAfterUnblock, PaymentResult.SUCCESS);
	}

	private PaymentResult makePayment() throws ManagerException {
		PaymentInfo payment = Util.generatePaymentInfo();
		List<PaymentTemplate> list = paymentManager
				.getPaymentTemplates(DEFAULT_TEST_LOCALE);
		if (!list.isEmpty()) {
			PaymentTemplate pTemplate = list.get(0);
			int pTemplateId = pTemplate.getId();
			return paymentManager.makePayment(creditCardId, pTemplateId,
					payment);
		} else {
			fail("Unable to run test because there are"
					+ " no payment templates in database");
		}
		return null;
	}

	/**
	 * Tests {@link AccountManager#putMoneyIntoAccount(BigDecimal, int)} method.
	 * Checks if balance was increased by related sum.
	 */
	@Test
	public void testPutMoney() throws ManagerException {
		BigDecimal balanceBefore = accountManager.getAccountById(accountId)
				.getAccountInfo().getBalance();
		BigDecimal sum = new BigDecimal(10000);
		PutMoneyResult result = accountManager.putMoneyIntoAccount(sum,
				accountId);
		assertEquals(result, PutMoneyResult.SUCCESS);
		BigDecimal balanceAfter = accountManager.getAccountById(accountId)
				.getAccountInfo().getBalance();
		BigDecimal expectedBalance = balanceBefore.add(sum);
		assertEquals(0, expectedBalance.compareTo(balanceAfter));
	}
}