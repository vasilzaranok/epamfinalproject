package by.epam.javatraining.zarenok.finalproject.manager;

import static org.junit.Assert.assertEquals;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import util.Util;
import by.epam.javatraining.zarenok.finalproject.entity.ClientInfo;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.entity.User;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.AddEntityResult;
import by.epam.javatraining.zarenok.finalproject.manager.util.ManagerFactory;

public class TestUserManager {

	private static ClientManager clientManager = ManagerFactory.INSTANCE
			.getClientManager();
	private static UserManager userManager = ManagerFactory.INSTANCE
			.getUserManager();
	private int clientId;
	private ClientInfo clientInfo;
	
	/**
	 * Add client to database.
	 */
	@Before
	public void setUp() throws Exception {
		clientInfo = Util.generateClientInfo();
		AddEntityResult result = clientManager.addClient(clientInfo);
		clientId = result.getId();
	}

	@After
	public void tearDown() throws Exception {
		Util.deleteTestClient(clientId);
	}
	
	/**
	 * Tests get user after client sign in.
	 * @throws ManagerException
	 */
	@Test
	public void testGetUser() throws ManagerException {
		User user = new User();
		String login = "111testUser";
		user.setLogin(login);
		String password = "pass";
		user.setPassword(password);
		user.setRole(Role.CLIENT);
		clientManager.signUpClient(clientInfo, user);
		User userInSystem = userManager.getUser(login, password);
		assertEquals(user.getRole(), userInSystem.getRole());
	}
}