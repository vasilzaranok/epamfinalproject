package util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;

class DBCleaner {

	private static final String SQL_DELETE_PAYMENTS = "DELETE payment"
			+ " FROM payment JOIN creditcard ON"
			+ " payment.CreditCard_idCreditCard=creditcard.idCreditCard"
			+ " WHERE creditcard.Account_Client_idClient=?";
	private static final String SQL_DELETE_CARDS = "DELETE FROM"
			+ " creditcard WHERE Account_Client_idClient=?";
	private static final String SQL_DELETE_ACCOUNTS = "DELETE FROM"
			+ " account WHERE Clients_idClient=?";
	private static final String SQL_DELETE_CLIENT = "DELETE client, user"
			+ " FROM client LEFT JOIN user on client.Users_idUser=user.idUser"
			+ " where client.idClient=?";

	private DBCleaner() {
	}

	static void deletePayments(Connection connection, int clientId)
			throws SQLException {
		delete(connection, SQL_DELETE_PAYMENTS, clientId);
	}

	static void deleteCards(Connection connection, int clientId)
			throws SQLException {
		delete(connection, SQL_DELETE_CARDS, clientId);
	}

	static void deleteAccounts(Connection connection, int clientId)
			throws SQLException {
		delete(connection, SQL_DELETE_ACCOUNTS, clientId);
	}

	static void deleteClientInfo(Connection connection, int clientId)
			throws SQLException {
		delete(connection, SQL_DELETE_CLIENT, clientId);
	}

	private static void delete(Connection connection, String sql, int id)
			throws SQLException {
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			statement.executeUpdate();
		} finally {
			close(statement);
		}
	}

	private static void close(Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
			}
		}
	}
}