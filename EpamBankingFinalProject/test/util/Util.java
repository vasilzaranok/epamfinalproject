package util;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;

import by.epam.javatraining.zarenok.finalproject.entity.AccountInfo;
import by.epam.javatraining.zarenok.finalproject.entity.ClientInfo;
import by.epam.javatraining.zarenok.finalproject.entity.CreditCard;
import by.epam.javatraining.zarenok.finalproject.entity.PaymentInfo;
import by.epam.javatraining.zarenok.finalproject.manager.util.ManagerUtil;

public class Util {
	
	private Util() {
	}
	
	/**
	 * Rolling back changes after testing DAO method.
	 */
	public static void tearDown(Connection connection) throws Exception {
		connection.rollback();
		connection.setAutoCommit(true);
		connection.close();
	}
	
	public static ClientInfo generateClientInfo() {
		ClientInfo client = new ClientInfo();
		String firstname = "testClientInfoFN";
		client.setFirstname(firstname);
		String middlename = "testClientInfoMiddleName";
		client.setMiddlename(middlename);
		String passportNumber = "testClientInfoPassNum";
		client.setPassportNumber(passportNumber);
		String surname = "testClientInfoSurname";
		client.setSurname(surname);
		return client;
	}
	
	public static AccountInfo generateAccount() {
		AccountInfo account = new AccountInfo();
		account.setBlocked(false);
		String number = "testAccountNumber";
		account.setNumber(number);
		int intBalance = 250000;
		BigDecimal balance = new BigDecimal(intBalance);
		account.setBalance(balance);
		return account;
	}
	
	public static CreditCard generateCard() throws ParseException {
		CreditCard card = new CreditCard();
		String cardNumber = "testCardNumber";
		card.setNumber(cardNumber);
		card.setExpirationDate(generateDate());
		return card;
	}
	
	public static PaymentInfo generatePaymentInfo() {
		PaymentInfo payment = new PaymentInfo();
		String paymentData = "testPaymentData";
		payment.setPaymentData(paymentData );
		int val = 25000;
		BigDecimal sum = new BigDecimal(val);
		payment.setSum(sum);
		return payment;
	}
	
	public static Connection getConnection() throws Exception {
		Connection connection = ManagerUtil.getConnection();
		connection.setAutoCommit(false);
		return connection;
	}
	
	private static Date generateDate() throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("mm/yy");
		String expirationDate = "01/20";
		java.util.Date parsed = format.parse(expirationDate);
		return new Date(parsed.getTime());
	}

	/**
	 * Delete client from database after test.
	 */
	public static void deleteTestClient(int clientId) throws Exception {
		Connection connection = ManagerUtil.getConnection();
		DBCleaner.deletePayments(connection, clientId);
		DBCleaner.deleteCards(connection, clientId);
		DBCleaner.deleteAccounts(connection, clientId);
		DBCleaner.deleteClientInfo(connection, clientId);
		connection.close();
	}
}