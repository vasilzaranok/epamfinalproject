<%-- 
  - Author: Vasil Zaranok
  - Description: Show clinet's account page
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<fmt:setLocale value="${locale}" />
<fmt:setBundle basename="strings.pagecontent" />
<title><fmt:message key="admin.showAccount.title" /></title>
</head>
<body>
    <c:import url="/jsp/admin/navbar.jsp" />
    <br></br>
    <br></br>
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class="panel panel-default">
                    <!--  Account panel -->
					<div class="panel-body">
						<h2><fmt:message key="account.account" />:</h2>
						<p><b><fmt:message key="account.number" /></b> : ${account.accountInfo.number }</p>
						<p><b><fmt:message key="account.balance" /></b> : ${account.accountInfo.balance }</p>
						<p><b><fmt:message key="account.blocked.title" /></b> :
							<c:choose>
								<c:when test="${account.accountInfo.blocked}">
								   <fmt:message key="account.blocked.blocked" /> 
								</c:when>
								<c:otherwise>
								    <fmt:message key="account.blocked.notBlocked" />
								</c:otherwise>
							</c:choose>
						</p>
						<!-- Unblock account button -->
						<form method="POST" action="${pageContext.request.contextPath}/controller">
						    <%-- Id of that account --%>
							<input type="hidden" name="accountId"
								value="${account.accountInfo.id }" />
							<%-- Command parameter --%>
							<input type="hidden"
								name="command" value="admin_unblock_account" />
							<c:if test="${account.accountInfo.blocked }">
								<button type="submit" class="btn btn-primary">
								    <fmt:message key="admin.showAccount.unblock" />
								</button>
							</c:if>
						</form>
					</div>
				</div>
				<!-- Panel with credit cards -->
                <div class="panel panel-default">
                    <div class="panel-body">
                        <h2><fmt:message key="admin.showAccount.cards" /></h2>
                        <br></br>
                        <!-- Add credit card button -->
						<form method="post" action="${pageContext.request.contextPath}/controller">
						    <%-- Command parameter --%>
							<input type="hidden" name="command"
                                   value="admin_add_card" />
                            <%-- Step of command --%>
							<input type="hidden" name="addCardStep"
							       value="show" />
							<%-- Id of account to add card --%>
							<input type="hidden" name="accountId"
								   value="${account.accountInfo.id }" />
							<p></p>
							<button type="submit" class="btn btn-primary">
							 <fmt:message key="admin.showAccount.addCard" />
							</button>
						</form>
						<!-- Credit card list -->
						<ul class="list-group">
                            <c:forEach var="creditCard" items="${account.creditCards }" varStatus="status">
								<li class="list-group-item">
								<p><fmt:message key="card.number" /> : ${creditCard.number }</p>
								<p><fmt:message key="card.expirationDate" /> : <fmt:formatDate value="${creditCard.expirationDate }" pattern="MM/yy"/> </p>
								</li>
							</c:forEach>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>