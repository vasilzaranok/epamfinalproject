<%-- 
  - Author: Vasil Zaranok
  - Description: Administrator navigation bar 
  --%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" />
<fmt:setBundle basename="strings.pagecontent" />
<nav class="navbar navbar-inverse navbar-default">
	<div class="container-fluid">
		<div>
			<ul class="nav navbar-nav">
				<li class="active">
				    <!-- Link to clients -->
				    <a href="${pageContext.request.contextPath}/controller?command=admin_show_clients&page=1&onPageCount=5">
				        <fmt:message key="admin.navbar.clients" />
				    </a>
				</li>
				<li>
				    <!-- Russian language -->
				    <a href="${pageContext.request.contextPath}/controller?command=choose_language&lang=ru">
				        <fmt:message key="navbar.ru" />
				    </a>
				</li>
                <li>
                    <!-- English language -->
                    <a href="${pageContext.request.contextPath}/controller?command=choose_language&lang=us">
                        <fmt:message key="navbar.us" />
                    </a>
                </li>
				<li>
				    <!-- Log out -->
				    <a href="${pageContext.request.contextPath}/controller?command=logout">
				        <fmt:message key="navbar.logout" />
				    </a>
				</li>
			</ul>
		</div>
	</div>
</nav>