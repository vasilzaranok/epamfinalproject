<%-- 
  - Author: Vasil Zaranok
  - Description: Show all clients page
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="ctg" uri="customtags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="strings.pagecontent" prefix="admin.showClients." >
<title><fmt:message key="title" /></title>
</fmt:bundle>
</head>
<body>
    <c:import url="/jsp/admin/navbar.jsp" />
    <fmt:bundle basename="strings.pagecontent" >
    <br></br>
    <br></br>
    
	<div class="container">
		<h2><fmt:message key="admin.showClients.clients" /></h2>
		<br></br>
		<!-- Add client button -->
		<form method="get" action="${pageContext.request.contextPath}/controller" >
		<%-- Command parameter --%>
        <input type="hidden" name="command" value="admin_add_client" />
        <%-- Add client command step --%>
        <input type="hidden" name="addClientStep" value="show" />
        <!--  Add client button -->
        <button type="submit" class="btn btn-primary">
            <fmt:message key="admin.showClients.addClient" />
        </button>
        </form>
        <!-- Table with clients -->
		<table class="table table-hover">
			<thead>
				<tr>
					<th><fmt:message key="client.surname" /></th>
					<th><fmt:message key="client.firstname" /></th>
					<th><fmt:message key="client.middlename" /></th>
					<th><fmt:message key="client.passportNumber" /></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="clientInfo" items="${clientInfoList }"
					varStatus="status">
					<tr>
					   <td>${clientInfo.surname }</td>
					   <td>${clientInfo.firstname }</td>
					   <td>${clientInfo.middlename }</td>
					   <td>${clientInfo.passportNumber }</td>
					   <td>
					       <%-- Link to show client command --%>
					       <a href="${pageContext.request.contextPath}/controller?command=admin_show_client&clientId=${clientInfo.id }"
					          class="btn btn-primary btn-sm">
					           <fmt:message key="admin.showClients.show" />
					       </a>
					   </td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
		<!-- Pagination -->
		<ctg:pagination onPageCount="5" totalCount="${totalClients }"
		                link="${pageContext.request.contextPath}/controller?command=admin_show_clients"
		                pageParam="page"
		                onPageCountParam="onPageCount"/>
	</div>
	</fmt:bundle>
</body>
</html>