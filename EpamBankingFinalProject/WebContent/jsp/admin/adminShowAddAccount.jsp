<%-- 
  - Author: Vasil Zaranok
  - Description: Add client's account page
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<fmt:setLocale value="${locale}" />
<fmt:setBundle basename="strings.pagecontent" />
<title><fmt:message key="admin.addAccount.title" /></title>
</head>
<body>
    <c:import url="/jsp/admin/navbar.jsp" />
    <br></br>
    <br></br>
	<div class="container">
		<div class="row">
			<div class="col-md-offset-3 col-md-6">
				<h2><fmt:message key="admin.addAccount.description" /></h2>
				<!-- Alert if error occurs while add account -->
				<c:if test="${not empty userError}">
					<div class="alert alert-danger">${userError }</div>
				</c:if>
				<!--  Add account form -->
				<form method="POST" action="${pageContext.request.contextPath}/controller">
				    <%-- Command parameter --%>
					<input type="hidden" name="command" value="admin_add_account" />
					<%-- Step of add account command --%>
					<input type="hidden" name="addAccountStep" value="add" />
					<%-- Client's id to ad account --%>
					<input type="hidden" name="clientId" value="${clientId }" />
					<p>
					   <b><fmt:message key="account.number" />:</b>
					</p>
					<fmt:message key="admin.addAccount.patternDescription" 
					       var="patternDescription"/>
					<input type="text" name="number" pattern="[\d]{13}" 
					   title="${patternDescription }" class="form-control" required />
					<p><b><fmt:message key="account.balance" />:</b></p>
					<input type="number" name="balance" min="1" 
					        class="form-control" required  />
					<br></br>
					<!-- Add account button -->
					<div class="wrapper">
						<button type="submit" class="btn btn-primary">
						  <fmt:message key="admin.addAccount.addAccountButton" />
						</button>
					</div>
				</form>
			</div>
		</div>
	</div>
</body>
</html>