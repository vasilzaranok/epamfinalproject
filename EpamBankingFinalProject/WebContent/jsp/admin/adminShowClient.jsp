<%-- 
  - Author: Vasil Zaranok
  - Description: Show client page
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<fmt:setLocale value="${locale}" />
<fmt:setBundle basename="strings.pagecontent" />
<title><fmt:message key="admin.showClient.title" /></title>
</head>
<body>
	<c:import url="/jsp/admin/navbar.jsp" />
	<br></br>
	<br></br>

	<div class="container">
		<div class="row">
			<div class="col-md-offset-2 col-md-8">
				<div class="panel panel-default">
					<!-- Client's data panel -->
					<div class="panel-body">
						<h2><fmt:message key="admin.showClient.description" />:</h2>
						<p>
							<b><fmt:message key="client.surname" />:</b> ${clientInfo.surname }
						</p>
						<p>
							<b><fmt:message key="client.firstname" />:</b> ${clientInfo.firstname }
						</p>
						<p>
							<b><fmt:message key="client.middlename" />:</b> ${clientInfo.middlename }
						</p>
						<p>
							<b><fmt:message key="client.passportNumber" />:</b> ${clientInfo.passportNumber }
						</p>
						<!-- Update client's data button -->
						<form method="POST" action="${pageContext.request.contextPath}/controller">
						    <%-- Command parameter --%>
							<input type="hidden" name="command"
							       value="admin_update_client" />
							<%-- Update client command step --%>
							<input type="hidden" name="updateClientStep"
							       value="show" />
							<%-- Id of client to update --%>
							<input type="hidden" name="clientId"
							       value="${clientInfo.id }" />
							<!--  Update button -->
							<button type="submit" class="btn btn-primary">
							 <fmt:message key="admin.showClient.changeDataButton" />
							</button>
						</form>
					</div>
				</div>
				<!-- Panel with accounts -->
				<div class="panel panel-default">
					<div class="panel-body">
						<h2><fmt:message key="admin.showClient.accounts" />:</h2>
						<br></br>
						<!-- Add account button -->
						<form method="post" action="${pageContext.request.contextPath}/controller">
						    <%-- Command parameter --%>
							<input type="hidden" name="command"
							       value="admin_add_account" />
							<%-- Step of add account command --%>
							<input type="hidden" name="addAccountStep"
							       value="show" />
							<%-- Id of client to add account --%>
							<input type="hidden" name="clientId"
							       value="${clientInfo.id }" />
							<!-- Add account button -->
							<button type="submit" class="btn btn-primary">
							 <fmt:message key="admin.showClient.addAccountButton" />
							</button>
						</form>
						<!-- List with accounts -->
						<div class="list-group">
							<c:forEach var="account" items="${accountInfoList }"
								varStatus="status">
								<%-- Link to show account command--%>
								<a
									href="${pageContext.request.contextPath}/controller?command=admin_show_account&accountId=${account.id }"
									class="list-group-item">
									<p>
										<b><fmt:message key="account.number" /></b> :
										${account.number }
									</p>
									<p>
										<b><fmt:message key="account.balance" /></b> :
										${account.balance }
									</p>
									<p>
										<b><fmt:message key="account.blocked.title" /></b> :
										<c:choose>
											<c:when test="${account.blocked}">
												<fmt:message key="account.blocked.blocked" />
											</c:when>
											<c:otherwise>
												<fmt:message key="account.blocked.notBlocked" />
											</c:otherwise>
										</c:choose>
									</p>
								</a>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>