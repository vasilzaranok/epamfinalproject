<%-- 
  - Author: Vasil Zaranok
  - Description: Add credit card page
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<fmt:setLocale value="${locale}" />
<fmt:setBundle basename="strings.pagecontent" />
<title><fmt:message key="admin.showAddCard.title" /></title>
</head>
<body>
    <c:import url="/jsp/admin/navbar.jsp" />
    <br></br>
    <br></br>
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                    <h2>
                        <fmt:message key="admin.showAddCard.description" />
                    </h2>
                    <!-- Alert if error occurs while adding card -->
                    <c:if test="${not empty userError}">
                        <div class="alert alert-danger">${userError }</div>
                    </c:if>
                    <!-- Add credit card from -->
                    <form method="POST" action="${pageContext.request.contextPath}/controller">
                        <%-- Command parameter --%>
                        <input type="hidden" name="command" value="admin_add_card" />
                        <%-- Step of add card command --%>
                        <input type="hidden" name="addCardStep" value="add" />
                        <%-- Id of account to add card --%>
                        <input type="hidden" name="accountId" value="${accountId }" />
                        <p>
                            <b><fmt:message key="card.number" />:</b>
                        </p>
                        <fmt:message key="card.number" 
                                     var="patternDescription"/>
                        <fmt:message key="card.expirationDateFormat" 
                                     var="expirationDateFormat"/>
                        <input type="text" name="number" class="form-control"
                               pattern="[0-9][\d]{15}" 
                               title="${patternDescription }" required />
                        <p>
                            <b>
                            <fmt:message key="card.expirationDate" />
                             ${expirationDateFormat }:
                            </b>
                        </p>
                        <input type="text" name="expirationDate"
                                     class="form-control" 
                                     pattern="(1[0-2]|(0[1-9]))/[1-9][0-9]"
                                     title="${expirationDateFormat }" required /> 
                        <br></br>
                        <!-- Add card button -->
                        <div class="wrapper">
                            <button type="submit" class="btn btn-primary">
                                <fmt:message key="admin.showAddCard.addCardButton" />
                            </button>
                        </div>
                    </form>
            </div>
        </div>
    </div>
</body>
</html>