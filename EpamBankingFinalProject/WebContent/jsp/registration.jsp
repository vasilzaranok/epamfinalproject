<%-- 
  - Author: Vasil Zaranok
  - Description: Client registration page.
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<fmt:setLocale value="${locale}" />
<fmt:setBundle basename="strings.pagecontent" />
<title><fmt:message key="client.registration.title" /></title>
</head>
<body>
    <c:import url="/jsp/navbar.jsp" />
    <br></br>
    <br></br>
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                <h2><fmt:message key="client.registration.description" /></h2>
                <!-- Alert if error occurs -->
                <c:if test="${not empty userError}">
                    <div class="alert alert-danger">${userError }</div>
                </c:if>
                <!-- Registration form -->
                <form method="POST" action="${pageContext.request.contextPath}/controller">
                    <%-- Command parameter --%>
                    <input type="hidden" name="command" value="sign_up" />
                    <%-- Step of add client command --%>
                    <input type="hidden" name="addClientStep" value="add" />
                    <p>
                        <b><fmt:message key="login.login" />:</b>
                    </p>
                    <fmt:message key="client.registration.login.patternDescription"
                                 var="loginPatternDescription" />
                    <input type="text" name="login" 
                           pattern="[A-Za-z][A-Za-z0-9-_]{1,14}"
                           title="${loginPatternDescription }"
                           class="form-control" required/>
                    <p>
                        <b><fmt:message key="login.password" />:</b>
                    </p>
                    <fmt:message key="client.registration.password.patternDescription"
                                 var="passwordPatternDescription" />
                    <input type="password" name="password"
                           pattern="^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{4,15}$"
                           title="${passwordPatternDescription }"
                           class="form-control" required/>
                    
                    <fmt:message key="client.surname" var="surname"/>
                    <p>
                        <b>${surname }:</b>
                    </p>
                    <input type="text" name="surname"
                           pattern="[A-ZА-ЯЁё][a-zа-яё]{1,30}"
                           title="${surname }"
                           class="form-control" required />
                    <fmt:message key="client.firstname" var="firstname"/>
                    <p>
                        <b>${firstname }:</b>
                    </p>
                    <input type="text" name="firstname"
                           pattern="[A-ZА-ЯЁё][a-zа-яё]{1,30}"
                           title="${firstname }"
                           class="form-control" required />
                    <fmt:message key="client.middlename" var="middlename"/>
                    <p>
                        <b>${middlename }:</b>
                    </p>
                    <input type="text" name="middlename"
                           pattern="[A-ZА-ЯЁё][a-zа-яё]{1,30}"
                           title="${middlename }"
                           class="form-control" required />
                    <fmt:message key="client.passportNumber" var="passportNumber"/>
                    <p>
                        <b>${passportNumber }:</b>
                    </p>
                    <input type="text" name="passportNumber" 
                           pattern="[A-Z]{2}[0-9]{7}"
                           title="${passportNumber }"
                           class="form-control"
                           required />
                    <br></br>
                    <div class="wrapper">
                        <button type="submit" class="btn btn-primary">
                            <fmt:message key="client.registration.signUp" />
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</body>
</html>