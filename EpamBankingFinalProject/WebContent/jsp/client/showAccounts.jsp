<%-- 
  - Author: Vasil Zaranok
  - Description: Show all client's accounts page
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<fmt:setLocale value="${locale}" />
<fmt:setBundle basename="strings.pagecontent" />
<title><fmt:message key="client.showAccounts.title" /></title>
</head>
<body>
	<c:import url="/jsp/client/navbar.jsp" />
	<br></br>
	<br></br>
	<div class="container">
		<div class="row">
			<div class="col-md-offset-2 col-md-8">
				<div class="panel panel-default">
					<!-- Panel with acounts -->
					<div class="panel-body">
						<h2>
							<fmt:message key="client.showClient.accounts" />
							:
						</h2>
						<br></br>
						<!-- List with accounts -->
						<div class="list-group">
							<c:forEach var="account" items="${accountInfoList }"
								varStatus="status">
								<%-- Link to show account command --%>
								<a
									href="${pageContext.request.contextPath}/controller?command=show_account&accountId=${account.id }"
									class="list-group-item">
									<p>
										<b><fmt:message key="account.number" /></b> :
										${account.number }
									</p>
									<p>
										<b><fmt:message key="account.balance" /></b> :
										${account.balance }
									</p>
									<p>
										<b><fmt:message key="account.blocked.title" /></b> :
										<c:choose>
											<c:when test="${account.blocked}">
												<fmt:message key="account.blocked.blocked" />
											</c:when>
											<c:otherwise>
												<fmt:message key="account.blocked.notBlocked" />
											</c:otherwise>
										</c:choose>
									</p>
								</a>
							</c:forEach>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</body>
</html>