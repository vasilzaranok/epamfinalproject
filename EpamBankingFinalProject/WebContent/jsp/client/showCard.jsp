<%-- 
  - Author: Vasil Zaranok
  - Description: Show client's credit card page
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<fmt:setLocale value="${locale}" />
<fmt:setBundle basename="strings.pagecontent" />
<title><fmt:message key="client.showCard.title"/></title>
</head>
<body>
    <c:import url="/jsp/client/navbar.jsp" />
    <br></br>
    <br></br>
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class="panel panel-default">
                    <!-- Credit card panel -->
                    <div class="panel-body">
                        <h2><fmt:message key="card.card"/>:</h2>
                        <p><b><fmt:message key="card.number"/></b> : ${creditCard.number }</p>
                        <p><b><fmt:message key="card.expirationDate"/></b> : <fmt:formatDate value="${creditCard.expirationDate }" pattern="MM/yy"/></p>
                        
                        <!-- Make payment button -->
						<form action="${pageContext.request.contextPath}/controller">
						    <%-- Command parameter --%>
							<input type="hidden" name="command"
								value="show_payment_templates" />
						    <%-- Id of credit card to make payment --%>
						    <input type="hidden"
								name="creditCardId" value="${creditCard.id }" />
							<button type="submit" class="btn btn-primary"><fmt:message key="client.showCard.makePayment"/></button>
						</form>
                     </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>