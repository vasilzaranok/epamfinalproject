<%-- 
  - Author: Vasil Zaranok
  - Description: Show payment page
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<fmt:setLocale value="${locale}" />
<fmt:setBundle basename="strings.pagecontent" />
<title><fmt:message key="client.showPayment.title"/></title>
</head>
<body>
    <c:import url="/jsp/client/navbar.jsp" />
    <br></br>
    <br></br>
    <div class="container">
        <div class="row">
            <div class="col-md-offset-2 col-md-8">
                <div class="panel panel-default">
                    <div class="panel-body">
						<h1><fmt:message key="payment.payment"/></h1>
						<!-- Alert if error occurs while cancel payment -->
						<c:if test="${not empty userError}">
                            <div class="alert alert-danger">${userError }</div>
                        </c:if>
						<p>
						      <fmt:message key="payment.name"/> : 
						      ${payment.paymentTemplate.name }
						</p>
						<p>
						      ${payment.paymentTemplate.dataDescription }:
							  ${payment.paymentInfo.paymentData }
					   </p>
						<p>
						      <fmt:message key="payment.sum"/>:
						      ${payment.paymentInfo.sum }
						</p>
						<p>
						      <fmt:message key="payment.time"/>:
						       ${payment.paymentInfo.paymentTime }
						</p>
						<p>
						      <fmt:message key="card.card"/>: 
						      ${payment.creditCard.number }
						      </p>
						<p></p>
						<!-- Cancel payment button -->
						<form action="${pageContext.request.contextPath}/controller">
						    <%-- Command parameter --%>
                            <input type="hidden" name="command" 
                                   value="cancel_payment" />
                            <%-- Id of payment to cancel --%>
                            <input type="hidden" name="paymentId" 
                                   value="${payment.paymentInfo.id }" />
                            <button type="submit" class="btn btn-primary">
                                <fmt:message key="client.showPayment.cancelPayment"/>
                            </button>
                        </form>
                     </div>
                </div>
            </div>
        </div>
    </div>    
</body>
</html>