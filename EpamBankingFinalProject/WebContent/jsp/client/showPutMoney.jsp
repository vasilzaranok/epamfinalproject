<%-- 
  - Author: Vasil Zaranok
  - Description: Put money into account page
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<fmt:setLocale value="${locale}" />
<fmt:setBundle basename="strings.pagecontent" />
<title><fmt:message key="client.showPutMoney.title"/></title>
</head>
<body>
    <c:import url="/jsp/client/navbar.jsp" />
    <br></br>
    <br></br>
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6">
                <h2><fmt:message key="client.showPutMoney.description"/></h2>
                <!-- Alert if error occurs while put money -->
                <c:if test="${not empty userError}">
                     <div class="alert alert-danger">${userError }</div>
                </c:if>
                <!-- Put money form -->
				<form method="POST" action="${pageContext.request.contextPath}/controller">
					<%-- Command parameter --%>
					<input type="hidden" name="command" 
					       value="put_money_into_account" />
				    <%-- Put money command step --%>
				    <input type="hidden" name="putMoneyStep"
				           value="put" />
					<%-- Id of account to put money --%>
					<input type="hidden" name="accountId"
					       value="${accountId }" />
					<input type="number" name="putMoneySum"
					       class="form-control" min="1" required/>
					<!-- Put money button -->
					<button type="submit" class="btn btn-primary">
					   <fmt:message key="client.showPutMoney.putMoney"/>
					</button>
				</form>
            </div>
        </div>
    </div>
</body>
</html>