<%-- 
  - Author: Vasil Zaranok
  - Description: Make payment page
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<fmt:setLocale value="${locale}" />
<fmt:setBundle basename="strings.pagecontent" />
<title><fmt:message key="client.showMakePayment.title"/></title>
</head>
<body>
    <c:import url="/jsp/client/navbar.jsp" />
    <br></br>
    <br></br>
    <div class="container">
        <div class="row">
            <div class="col-md-offset-3 col-md-6">

				<h2><fmt:message key="payment.payment"/></h2>
				<!-- Alert if error occurs while making payment -->
				<c:if test="${not empty userError}">
                    <div class="alert alert-danger">${userError }</div>
                </c:if>
				<p>
				    <b><fmt:message key="payment.name"/> :</b> ${paymentTemplate.name }
				</p>
				<!-- Make payment form -->
				<form method="POST" action="${pageContext.request.contextPath}/controller">
					<%-- Command parameter --%>
					<input type="hidden" name="command"
					       value="make_payment" />
					<%-- Id of credit card to make payment --%>
					<input type="hidden" name="creditCardId"
					       value="${creditCardId }" />
					<%-- Id of payment template to make payment --%>
					<input type="hidden" name="paymentTemplateId"
						   value="${paymentTemplate.id }" />
					<p>
					   <b>
					       ${paymentTemplate.dataDescription } 
					       (${paymentTemplate.regexpDescription }) :
					   </b>
					</p>
					<input type="text" name="paymentData" class="form-control" 
					       pattern="${paymentTemplate.regexp }" 
					       title="${paymentTemplate.regexpDescription }" required/>
					<p>
					   <b><fmt:message key="payment.sum"/> :</b>
					</p>
					<input type="number" name="paymentSum"
					       class="form-control" min="1" required/>
					<p></p>
					<!-- Make payment button -->
					<button type="submit" class="btn btn-primary">
					   <fmt:message key="client.showMakePayment.makePayment"/>
					</button>
				</form>
            </div>
        </div>
    </div>
</body>
</html>