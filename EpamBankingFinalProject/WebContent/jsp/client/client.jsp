<%-- 
  - Author: Vasil Zaranok
  - Description: Client main page
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<fmt:setLocale value="${locale}" />
<fmt:setBundle basename="strings.pagecontent" />
<title><fmt:message key="client.home.title" /></title>
</head>
<body>
	<c:import url="/jsp/client/navbar.jsp" />
	<br></br>
	<br></br>
    
    <div class="container">
        <h1><fmt:message key="client.home.welcome"/> ${clientInfo.surname } ${clientInfo.firstname}.
        </h1>
    </div> 
</body>
</html>