<%-- 
  - Author: Vasil Zaranok
  - Description: Show all client's payment page
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8"
    pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<fmt:setLocale value="${locale}" />
<fmt:setBundle basename="strings.pagecontent" />
<title><fmt:message key="client.showPayments.title"/></title>
</head>
<body>
    <c:import url="/jsp/client/navbar.jsp" />
    <div class="container">
        <br></br>
        <br></br>
        <!-- Table with payments -->
        <table class="table table-hover">
            <thead>
                <tr>
                    <th><b><fmt:message key="payment.name"/> :</b></th>
                    <th><b><fmt:message key="payment.sum"/> :</b></th>
                    <th><b><fmt:message key="payment.time"/> :</b></th>
                    <th><b><fmt:message key="card.card"/> :</b></th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                <c:forEach var="payment" items="${payments }" varStatus="status">
                    <tr>
                       <td>${payment.paymentTemplate.name }</td>
                       <td>${payment.paymentInfo.sum }</td>
                       <td>${payment.paymentInfo.paymentTime }</td>
                       <td>${payment.creditCard.number }</td>
                       <td>
                           <%-- Link to show payment command --%>
                           <a href="${pageContext.request.contextPath}/controller?command=show_payment&paymentId=${payment.paymentInfo.id }"
                              class="btn btn-primary btn-sm">
                              <fmt:message key="client.showPayments.show"/>
                           </a>
                       </td>
                    </tr>
                </c:forEach>
            </tbody>
        </table>
        <!-- Pagination -->
        <ctg:pagination onPageCount="5" totalCount="${totalPayments }"
                        link="${pageContext.request.contextPath}/controller?command=show_payments"
                        pageParam="page"
                        onPageCountParam="onPageCount"/>
    </div>
</body>
</html>