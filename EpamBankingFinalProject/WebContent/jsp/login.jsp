<%-- 
  - Author: Vasil Zaranok
  - Description: Login page 
  --%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1" />
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<fmt:setLocale value="${locale}" />
<fmt:bundle basename="strings.pagecontent" prefix="login." >
<title><fmt:message key="title" /></title>
</fmt:bundle>
</head>
<body>
    <fmt:bundle basename="strings.pagecontent" prefix="login." >
	<c:import url="/jsp/navbar.jsp" />
    <br></br>
    <br></br>
	<div class="container">
		<div class="row">
			<div class="col-md-offset-4 col-md-4">
				<div class="form-login">
					<h4>
						<fmt:message key="welcome" />
					</h4>
					<!-- Alert if user not found -->
					<c:if test="${not empty userNotFound}">
						<div class="alert alert-danger">${userNotFound }</div>
					</c:if>
					<!-- Alert if client sign in -->
					<c:if test="${not empty registrationMessage}">
                        <div class="alert alert-success">${registrationMessage }</div>
                    </c:if>
                    <!-- Login form -->
					<form name="loginForm" method="POST" action="${pageContext.request.contextPath}/controller">
						<%-- Command parameter --%>
						<input type="hidden" name="command" value="login" />
						<fmt:message key="login" var="loginString" />
						<input type="text" name="login"
							   class="form-control input-sm chat-input"
							   placeholder="${ loginString }" required />
					    <br></br>
						<fmt:message key="password" var="passwordString" />
						<input type="password" name="password"
							   class="form-control input-sm chat-input"
							   placeholder="${ passwordString }" required />
					    <br></br>
						<div class="wrapper">
							<button type="submit" class="btn btn-primary">
								<fmt:message key="button.login" />
							</button>
							<a href="${pageContext.request.contextPath}/jsp/registration.jsp" 
							   class="btn btn-info" role="button">
							   <fmt:message key="button.registration" />
							</a>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</fmt:bundle>
</body>
</html>