<%-- 
  - Author: Vasil Zaranok
  - Description: Guest navigation bar 
  --%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${locale}" />
<fmt:setBundle basename="strings.pagecontent" />
<nav class="navbar navbar-inverse navbar-default">
	<div class="container-fluid">
		<div>
			<ul class="nav navbar-nav">
				<li>
				    <!-- Russian language -->
				    <a href="${pageContext.request.contextPath}/controller?command=choose_language&lang=ru">
				        <fmt:message key="navbar.ru" />
				    </a>
				</li>
                <li>
                    <!-- English language -->
                    <a href="${pageContext.request.contextPath}/controller?command=choose_language&lang=us">
                        <fmt:message key="navbar.us" />
                    </a>
                </li>
			</ul>
		</div>
	</div>
</nav>