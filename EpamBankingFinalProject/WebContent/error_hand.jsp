<%@ page isErrorPage="true" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<html>
<head>
<title>Error</title>
<link rel="stylesheet"
	href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" />
</head>
<body>

	<div class="container">
		<h1>Error!</h1>
		<div class="alert alert-danger">
			<strong>Error!</strong> An error occurs on server while processing
			request. Try repeat action.
		</div>
	</div>

</body>
</html>
