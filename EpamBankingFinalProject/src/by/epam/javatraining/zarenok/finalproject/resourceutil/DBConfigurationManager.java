package by.epam.javatraining.zarenok.finalproject.resourceutil;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * DBConfigurationManager allows get database configuration properties.
 * 
 * @author Vasil Zaranok
 *
 */
public class DBConfigurationManager {
	private static ResourceBundle resourceBundle;

	private static void init() {
		resourceBundle = ResourceBundle.getBundle("prop.db", Locale
				.getDefault(), Thread.currentThread().getContextClassLoader());
	}

	private DBConfigurationManager() {
	}

	/**
	 * Gets configuration property.
	 * 
	 * @param key
	 *            related property key
	 * @return configuration property
	 */
	public static String getProperty(String key) {
		if (resourceBundle == null) {
			init();
		}
		return resourceBundle.getString(key);
	}
}
