package by.epam.javatraining.zarenok.finalproject.resourceutil;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * MessageManager allows get database message properties.
 * 
 * @author Vasil Zaranok
 *
 */
public class MessageManager {

	private MessageManager() {
	}

	/**
	 * Gets message property.
	 * 
	 * @param key
	 *            related property key
	 * @return configuration property
	 */
	public static String getProperty(String key, Locale locale) {
		if (locale == null) {
			locale = Locale.US;
		}
		ResourceBundle resourceBundle = ResourceBundle.getBundle(
				"strings.messages", locale, Thread.currentThread()
						.getContextClassLoader());
		return resourceBundle.getString(key);
	}
}
