package by.epam.javatraining.zarenok.finalproject.resourceutil;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * ConfigurationManager allows get application configuration properties.
 * 
 * @author Vasil Zaranok
 *
 */
public class ConfigurationManager {

	private static ResourceBundle resourceBundle;

	private static void init() {
		resourceBundle = ResourceBundle.getBundle("prop.config", Locale
				.getDefault(), Thread.currentThread().getContextClassLoader());
	}

	private ConfigurationManager() {
	}

	/**
	 * Gets configuration property.
	 * 
	 * @param key
	 *            related property key
	 * @return configuration property
	 */
	public static String getProperty(String key) {
		if (resourceBundle == null) {
			init();
		}
		return resourceBundle.getString(key);
	}
}