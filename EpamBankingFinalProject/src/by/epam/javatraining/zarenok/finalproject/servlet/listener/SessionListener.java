package by.epam.javatraining.zarenok.finalproject.servlet.listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * {@link SessionListener} allows watch to session events.
 * 
 * @author Vasil Zaranok
 *
 */
@WebListener
public class SessionListener implements HttpSessionAttributeListener {

	private static final Logger logger = LogManager
			.getLogger(SessionListener.class);

	@Override
	public void attributeRemoved(HttpSessionBindingEvent ev) {
		logger.info("removed: " + ev.getClass().getSimpleName() + " : "
				+ ev.getName() + " : " + ev.getValue());
	}

	@Override
	public void attributeAdded(HttpSessionBindingEvent ev) {
		logger.info("add: " + ev.getClass().getSimpleName() + " : "
				+ ev.getName() + " : " + ev.getValue());
	}

	@Override
	public void attributeReplaced(HttpSessionBindingEvent ev) {
		logger.info("replaced: " + ev.getClass().getSimpleName() + " : "
				+ ev.getName() + " : " + ev.getValue());
	}
}
