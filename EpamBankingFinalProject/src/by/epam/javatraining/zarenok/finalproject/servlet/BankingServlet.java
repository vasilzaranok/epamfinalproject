package by.epam.javatraining.zarenok.finalproject.servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.db.util.ConnectionPoolManager;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;

/**
 * BankingServlet using as controller and process user request.
 * 
 * @author Vasil Zaranok
 *
 */
@WebServlet(name = "BankingServlet", urlPatterns = { "/controller" })
public class BankingServlet extends HttpServlet {

	private static final String ERROR = "Error while processing request";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private static Logger logger = LogManager.getLogger(BankingServlet.class);

	/**
	 * {@inheritDoc} This implementation starts connection pool.
	 */
	@Override
	public void init() throws ServletException {
		try {
			super.init();
			ConnectionPoolManager.startPool();
			logger.info("Servlet init");
		} catch (Exception e) {
			logger.error("Error while servlet init", e);
		}
	}

	/**
	 * {@inheritDoc} This implementation stops connection pool
	 */
	@Override
	public void destroy() {
		try {
			super.destroy();
			ConnectionPoolManager.stopPool();
			logger.info("Servlet destroyed");
		} catch (Exception e) {
			logger.error("Error while destroying servlet", e);
		}
	}

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
			processRequest(req, resp);
	}

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
			processRequest(req, resp);
	}

	/**
	 * Process user request.
	 */
	private void processRequest(HttpServletRequest request,
			HttpServletResponse response) {
		String page = null;
		SessionRequestWrapper requestWrapper = new SessionRequestWrapper();
		requestWrapper.extractValues(request);
		try {
			ActionCommand command = RequestHelper.INSTANCE
					.getCommand(requestWrapper);
			page = command.execute(requestWrapper);
			if (page != null) {
				requestWrapper.insertAttributes(request);
				RequestDispatcher dispatcher = getServletContext()
						.getRequestDispatcher(page);
				dispatcher.forward(request, response);
			} else {
				redirectToError(request, response);
			}
		} catch (ActionCommandException e) {
			try {
				redirectToError(request, response);
			} catch (Exception e1) {
				logger.error("Error while redirecting to error page", e1);
			}
			logger.error(ERROR, e);
		} catch (Exception e) {
			logger.error(ERROR, e);
		}
	}

	/**
	 * Redirect to error page
	 * 
	 * @param request
	 * @param response
	 * @throws IOException
	 */
	private void redirectToError(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String page;
		page = ConfigurationManager.getProperty("path.page.error");
		response.sendRedirect(request.getContextPath() + page);
	}
}