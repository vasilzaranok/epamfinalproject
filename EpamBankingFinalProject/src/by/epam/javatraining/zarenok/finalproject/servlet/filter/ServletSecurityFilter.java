package by.epam.javatraining.zarenok.finalproject.servlet.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.entity.User;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.servlet.RequestHelper;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;
import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommand.*;

/**
 * {@link ServletSecurityFilter} deny access to controller commands if user
 * haven't sufficient rights to execute this commands.
 * 
 * @author Vasil Zaranok
 *
 */
@WebFilter(urlPatterns = { "/controller" }, servletNames = { "BankingServlet" })
public class ServletSecurityFilter implements Filter {

	private static Logger logger = LogManager
			.getLogger(ServletSecurityFilter.class);

	public void destroy() {
	}

	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse resp = (HttpServletResponse) response;
		HttpSession session = req.getSession();
		User user = (User) session.getAttribute(USER);
		if (user == null) {
			user = new User();
			user.setRole(Role.GUEST);
			session.setAttribute(USER, user);
		}
		try {
			SessionRequestWrapper requestWrapper = new SessionRequestWrapper();
			requestWrapper.extractValues(req);
			ActionCommand command = RequestHelper.INSTANCE
					.getCommand(requestWrapper);
			if (!command.hasAccess(user.getRole())) {
				returnToLogin(request, req, resp);
				return;
			}
		} catch (ActionCommandException e) {
			redirectToError(req, resp);
			logger.error("Error while filter page", e);
			return;
		}
		chain.doFilter(request, response);
	}

	private void redirectToError(HttpServletRequest request,
			HttpServletResponse response) throws IOException {
		String page;
		page = ConfigurationManager.getProperty("path.page.error");
		response.sendRedirect(request.getContextPath() + page);
	}

	private void returnToLogin(ServletRequest request, HttpServletRequest req,
			HttpServletResponse resp) throws ServletException, IOException {
		String loginPage = ConfigurationManager.getProperty("path.page.login");
		forwardToPage(request, req, resp, loginPage);
		return;
	}

	protected void forwardToPage(ServletRequest request,
			HttpServletRequest req, HttpServletResponse resp, String page)
			throws ServletException, IOException {
		RequestDispatcher dispatcher = request.getServletContext()
				.getRequestDispatcher(page);
		dispatcher.forward(req, resp);
	}

	public void init(FilterConfig fConfig) throws ServletException {
	}

}
