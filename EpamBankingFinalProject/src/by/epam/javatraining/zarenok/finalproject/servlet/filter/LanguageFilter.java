package by.epam.javatraining.zarenok.finalproject.servlet.filter;

import java.io.IOException;
import java.util.Locale;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet Filter implementation class LanguageFilter sets locale session
 * attribute to right JSP i18n.
 */
@WebFilter(urlPatterns = { "/*" }, initParams = { @WebInitParam(name = "encoding", value = "UTF-8", description = "Encoding Param") })
public class LanguageFilter implements Filter {

	/**
	 * Default constructor.
	 */
	public LanguageFilter() {
	}

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response,
			FilterChain chain) throws IOException, ServletException {
		HttpServletRequest httpRequest = (HttpServletRequest) request;
		HttpServletResponse httpResponse = (HttpServletResponse) response;
		Locale locale = (Locale) httpRequest.getSession()
				.getAttribute("locale");
		if (locale == null) {
			if (Locale.US.equals(Locale.getDefault())
					|| Locale.getDefault().equals(new Locale("ru", "RU"))) {
				locale = Locale.getDefault();
			} else {
				locale = Locale.US;
			}
			httpRequest.getSession().setAttribute("locale", locale);
		}
		httpResponse.setLocale(locale);
		chain.doFilter(request, response);
	}

	/**
	 * @see Filter#init(FilterConfig)
	 */
	public void init(FilterConfig fConfig) throws ServletException {
	}
}
