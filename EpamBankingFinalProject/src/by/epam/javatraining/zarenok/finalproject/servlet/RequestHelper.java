package by.epam.javatraining.zarenok.finalproject.servlet;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandEnum;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;

/**
 * RequestHelper allows get ActionCommand object to execute by request
 * parameters.
 * 
 * @author Vasil Zaranok
 *
 */
public enum RequestHelper {
	INSTANCE;

	private static final String COMMAND = "command";

	/**
	 * Returns ActionCommand by request parameters.
	 * 
	 * @param request
	 *            {@link SessionRequestWrapper} object with request parameters
	 * @return ActionCommand by request parameters
	 * @throws ActionCommandException
	 *             if request command parameter null or unknown
	 */
	public ActionCommand getCommand(SessionRequestWrapper request)
			throws ActionCommandException {
		String action = request.getParameter(COMMAND);
		if (action == null) {
			throw new ActionCommandException("Parameter '" + COMMAND
					+ "' is null");
		}
		try {
			ActionCommand command = ActionCommandEnum.valueOf(
					action.toUpperCase()).getCommand();
			return command;
		} catch (Exception e) {
			throw new ActionCommandException(
					"Error while getting action command", e);
		}
	}
}
