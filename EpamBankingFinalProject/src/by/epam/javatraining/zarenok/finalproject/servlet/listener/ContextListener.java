package by.epam.javatraining.zarenok.finalproject.servlet.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * {@link ContextListener} allows watch to context events.
 * 
 * @author Vasil Zaranok
 *
 */
@WebListener
public class ContextListener implements ServletContextListener {

	private static Logger logger = LogManager.getLogger(ContextListener.class);

	@Override
	public void contextInitialized(ServletContextEvent ev) {
		logger.info("Context initialized");
	}

	@Override
	public void contextDestroyed(ServletContextEvent ev) {
		logger.info("Context destroyed");
	}
}
