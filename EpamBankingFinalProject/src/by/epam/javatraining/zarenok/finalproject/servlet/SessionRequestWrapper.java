package by.epam.javatraining.zarenok.finalproject.servlet;

import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

/**
 * SessionRequestWrapper wraps HttpServletRequest and HttpSession objects and
 * protect them from direct access.
 * 
 * @author Vasil Zaranok
 *
 */
public class SessionRequestWrapper {
	private Map<String, Object> requestAttributes;
	private Map<String, String[]> requestParameters;
	private Map<String, Object> sessionAttributes;
	private boolean invalidateSession = false;

	public SessionRequestWrapper() {
		requestAttributes = new HashMap<String, Object>();
	}

	/**
	 * Invalidate session. @see {@link HttpSession#invalidate()}
	 */
	public void invalidateSession() {
		invalidateSession = true;
	}

	/**
	 * Extract values from the request to wrapper.
	 * 
	 * @param request
	 *            request from that extract values to wrapper
	 */
	public void extractValues(HttpServletRequest request) {
		requestParameters = request.getParameterMap();
		Enumeration<String> sessionAttrNames = request.getSession()
				.getAttributeNames();
		sessionAttributes = new HashMap<>();
		while (sessionAttrNames.hasMoreElements()) {
			String attrName = sessionAttrNames.nextElement();
			Object attrObject = request.getSession().getAttribute(attrName);
			sessionAttributes.put(attrName, attrObject);
		}
	}

	/**
	 * Insert attributes to the request, and invalidate session if
	 * {@link #invalidateSession} was called.
	 * 
	 * @param request
	 *            request to insert
	 */
	public void insertAttributes(HttpServletRequest request) {
		for (Map.Entry<String, Object> entry : requestAttributes.entrySet()) {
			request.setAttribute(entry.getKey(), entry.getValue());
		}
		if (!invalidateSession) {
			for (Map.Entry<String, Object> entry : sessionAttributes.entrySet()) {
				request.getSession().setAttribute(entry.getKey(),
						entry.getValue());
			}
		} else {
			request.getSession().invalidate();
		}
	}

	/**
	 * Returns parameter from request. See @see
	 * {@link HttpServletRequest#getParameter(String)}.
	 * 
	 * @param paramName
	 * @return
	 */
	public String getParameter(String paramName) {
		if (requestParameters.get(paramName) != null) {
			return requestParameters.get(paramName)[0];
		} else {
			return null;
		}
	}

	/**
	 * See @see {@link HttpServletRequest#getParameterValues(String)}.
	 */
	public String[] getParameterValues(String paramName) {
		return requestParameters.get(paramName);
	}

	/**
	 * Gets session attribute. See @see {@link HttpSession#getAttribute(String)}
	 */
	public Object getSessionAttribute(String attributeName) {
		return sessionAttributes.get(attributeName);
	}
	
	/**
	 * Sets session attributes. See @see {@link HttpSession#setAttribute(String, Object)}.
	 */
	public void setSessionAttribute(String attributeName, Object attributeValue) {
		sessionAttributes.put(attributeName, attributeValue);
	}
	
	/**
	 * Sets request attribute. See @see {@link HttpServletRequest#setAttribute(String, Object)}
	 */
	public void setAttribute(String attributeName, Object attributeValue) {
		requestAttributes.put(attributeName, attributeValue);
	}
}
