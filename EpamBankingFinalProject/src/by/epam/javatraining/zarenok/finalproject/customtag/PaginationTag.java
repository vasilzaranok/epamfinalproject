package by.epam.javatraining.zarenok.finalproject.customtag;

import static java.lang.Math.max;
import static java.lang.Math.min;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.TagSupport;

/**
 * PaginationTag allows to use custom tag to display pagination when use long
 * lists.
 * 
 * @author Vasil Zaranok
 *
 */
public class PaginationTag extends TagSupport {

	private static final int MAX_PREV_LINKS = 5;
	private static final int MAX_NEXT_LINKS = 5;
	private static final String PAGE = "page";
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private int onPageCount;
	private int totalCount;
	private String link;
	private String pageParam;
	private String onPageCountParam;

	public String getPageParam() {
		return pageParam;
	}

	public void setPageParam(String currentPageParam) {
		this.pageParam = currentPageParam;
	}

	public String getOnPageCountParam() {
		return onPageCountParam;
	}

	public void setOnPageCountParam(String onPageCountParam) {
		this.onPageCountParam = onPageCountParam;
	}

	public int getOnPageCount() {
		return onPageCount;
	}

	public void setOnPageCount(int onPageCount) {
		this.onPageCount = onPageCount;
	}

	public int getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(int totalPages) {
		this.totalCount = totalPages;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	@Override
	public int doStartTag() throws JspException {
		int page = Integer
				.parseInt(pageContext.getRequest().getParameter(PAGE));
		try {
			JspWriter out = pageContext.getOut();
			out.write(getPaginationHtml(page));
		} catch (IOException e) {
			throw new JspException(e.getMessage());
		}
		return SKIP_BODY;
	}
	
	/**
	 * Builds HTML view of tag.
	 * @param currentPage number of current page 
	 * @return HTML string with pagination
	 */
	private String getPaginationHtml(int currentPage) {
		StringBuilder sBuilder = new StringBuilder();
		sBuilder.append("<ul class=\"pagination\">");
		int first = getFirstNumber(currentPage);
		int last = getLastNumber(currentPage);
		sBuilder.append(getLiHtml("&laquo;", first));
		for (int i = first; i <= last; i++) {
			if (i == currentPage) {
				sBuilder.append(getLiHtml(String.valueOf(i), i, true));
			} else {
				sBuilder.append(getLiHtml(String.valueOf(i), i));
			}
		}
		sBuilder.append(getLiHtml("&raquo;", last));
		sBuilder.append("</ul>");
		return sBuilder.toString();
	}
	
	/**
	 * Builds element of pagination list.
	 * @param body of li element
	 * @param page page to create hyper link
	 * @param active current page or not
	 * @return HTML string with li element
	 */
	private String getLiHtml(String body, int page, boolean active) {
		StringBuilder sBuilder = new StringBuilder();
		if (active) {
			sBuilder.append("<li class=\"active\">");
		} else {
			sBuilder.append("<li>");
		}
		sBuilder.append("<a href=" + createLinkForPage(page) + " >");
		sBuilder.append(body);
		sBuilder.append("</a>");
		sBuilder.append("</li>");
		return sBuilder.toString();
	}

	private String getLiHtml(String body, int page) {
		return getLiHtml(body, page, false);
	}
	
	/**
	 * Builds right hyper link for page with parameters.
	 * @param page
	 * @return string with link
	 */
	private String createLinkForPage(int page) {
		String prefix = null;
		if (link.contains("?")) {
			prefix = link + "&";
		} else {
			prefix = link + "?";
		}
		return prefix + pageParam + "=" + page + "&" + onPageCountParam + "="
				+ onPageCount;
	}
	
	/**
	 * Gets last number of list.
	 * @param page current page
	 * @return last number of list
	 */
	private int getLastNumber(int page) {
		int maxPage;
		if (totalCount % onPageCount == 0) {
			maxPage = max(1, totalCount / onPageCount);
		} else {
			maxPage = max(1, totalCount / onPageCount + 1);
		}
		return min(maxPage, page + MAX_NEXT_LINKS);
	}
	
	/**
	 * Gets first number of list.
	 * @param page current page
	 * @return first number of list
	 */
	private int getFirstNumber(int page) {
		return max(1, min(1, page - MAX_PREV_LINKS));
	}

	@Override
	public int doEndTag() throws JspException {
		return EVAL_PAGE;
	}
}
