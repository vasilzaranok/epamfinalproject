package by.epam.javatraining.zarenok.finalproject.commands.admin;

import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.EXCEPTION_ERROR_LOGIC;

import java.util.Locale;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.ClientInfo;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.manager.ClientManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.AddEntityResult;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.resourceutil.MessageManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;
import by.epam.javatraining.zarenok.finalproject.servlet.listener.SessionListener;

/**
 * AdminAddClientCommand allows process adding client request.
 * @author Vasil Zaranok
 *
 */
public class AdminAddClientCommand implements ActionCommand {
	
	private static final Logger logger = LogManager
			.getLogger(SessionListener.class);
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		String step = requestWrapper.getParameter("addClientStep");
		switch (step) {
		case "show":
			return ConfigurationManager
					.getProperty("path.page.adminShowAddClient");
		case "add":
			try {
				return addClient(requestWrapper);
			} catch (ManagerException e) {
				throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
			}
		default:
			throw new ActionCommandException("Unsupported command step : "
					+ step);
		}
	}

	private String addClient(SessionRequestWrapper requestWrapper)
			throws ManagerException {
		ClientInfo clientInfo = getClient(requestWrapper);
		ClientManager clientManager = managerFactory.getClientManager();
		AddEntityResult state = clientManager.addClient(clientInfo);
		if (state.isSuccess()) {
			return goToSuccess(requestWrapper, clientManager,
					state.getId());
		} else {
			return goToError(requestWrapper);
		}
	}

	private String goToSuccess(SessionRequestWrapper requestWrapper,
			ClientManager clientManager, int id) throws ManagerException {
		requestWrapper.setAttribute(CLIENT_ID, id);
		ClientInfo newClient = clientManager.getClientInfoById(id);
		requestWrapper.setAttribute(CLIENT_INFO, newClient);
		return ConfigurationManager.getProperty("path.page.adminShowClient");
	}

	private String goToError(SessionRequestWrapper requestWrapper) {
		Locale locale = (Locale) requestWrapper.getSessionAttribute(LOCALE);
		requestWrapper.setAttribute(ERROR,
				MessageManager.getProperty("admin.addClient.exist", locale));
		return ConfigurationManager.getProperty("path.page.adminShowAddClient");
	}

	private ClientInfo getClient(SessionRequestWrapper requestWrapper) {
		String firstname = requestWrapper.getParameter("firstname");
		String surname = requestWrapper.getParameter("surname");
		String middlename = requestWrapper.getParameter("middlename");
		String passportNumber = requestWrapper.getParameter("passportNumber");
		ClientInfo clientInfo = new ClientInfo();
		clientInfo.setFirstname(firstname);
		clientInfo.setSurname(surname);
		clientInfo.setMiddlename(middlename);
		clientInfo.setPassportNumber(passportNumber);
		logger.debug(clientInfo.toString());
		return clientInfo;
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * Only administrators have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		if (role == Role.ADMIN) {
			return true;
		} else {
			return false;
		}
	}
}
