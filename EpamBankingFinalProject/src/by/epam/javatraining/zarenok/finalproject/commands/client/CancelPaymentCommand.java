package by.epam.javatraining.zarenok.finalproject.commands.client;

import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.EXCEPTION_ERROR_LOGIC;

import java.util.Locale;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.ClientInfo;
import by.epam.javatraining.zarenok.finalproject.entity.Payment;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.entity.User;
import by.epam.javatraining.zarenok.finalproject.manager.ClientManager;
import by.epam.javatraining.zarenok.finalproject.manager.PaymentManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.PaymentCancelResult;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.resourceutil.MessageManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;

/**
 * {@link CancelPaymentCommand} allows process cancel payment request.
 * 
 * @author Vasil Zaranok
 *
 */
public class CancelPaymentCommand implements ActionCommand {

	private static final String UNSUPPORTED_STATE = "Unsupported chancel payment state!";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		int paymentId = Integer.parseInt(requestWrapper
				.getParameter(PAYMENT_ID));
		PaymentManager paymentManager = managerFactory.getPaymentManager();
		try {
			PaymentCancelResult state = paymentManager.cancelPayment(paymentId);
			if (state == PaymentCancelResult.SUCCESS) {
				return success(requestWrapper);
			} else if (state == PaymentCancelResult.ACCOUNT_BLOCKED) {
				return blocked(requestWrapper, paymentId, paymentManager);
			} else {
				throw new ActionCommandException(UNSUPPORTED_STATE);
			}
		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
	}

	private String blocked(SessionRequestWrapper requestWrapper, int paymentId,
			PaymentManager paymentManager) throws ManagerException {
		Locale locale = (Locale) requestWrapper.getSessionAttribute(LOCALE);
		Payment payment = paymentManager.getPayment(paymentId, locale);
		requestWrapper.setAttribute(PAYMENT, payment);
		requestWrapper.setAttribute(ERROR, MessageManager.getProperty(
				"client.chancelPayment.accountBlocked", locale));
		return ConfigurationManager.getProperty("path.page.showPayment");
	}

	private String success(SessionRequestWrapper requestWrapper)
			throws ManagerException {
		Locale locale = (Locale) requestWrapper.getSessionAttribute(LOCALE);
		ClientManager clientManager = managerFactory.getClientManager();
		requestWrapper.setSessionAttribute(LOCALE, locale);
		User user = (User) requestWrapper.getSessionAttribute(USER);
		ClientInfo clientInfo = clientManager.getClientInfoByUser(user);
		requestWrapper.setAttribute(CLIENT_INFO, clientInfo);
		return ConfigurationManager.getProperty("path.page.client");
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Only clients have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		if (role == Role.CLIENT) {
			return true;
		} else {
			return false;
		}
	}
}
