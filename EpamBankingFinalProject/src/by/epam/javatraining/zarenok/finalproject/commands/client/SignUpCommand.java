package by.epam.javatraining.zarenok.finalproject.commands.client;

import java.util.Locale;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.ClientInfo;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.entity.User;
import by.epam.javatraining.zarenok.finalproject.manager.ClientManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.SignUpResult;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.resourceutil.MessageManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;
import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.*;

/**
 * SignUpCommand allows process sign up request.
 * @author Vasil Zaranok
 *
 */
public class SignUpCommand implements ActionCommand {

	private static final String REGISTRATION_MESSAGE = "registrationMessage";
	private static final String PAGE_LOGIN = "path.page.login";
	private static final String PAGE_REGISTRATION = "path.page.registration";
	private static final String UNSUPPORTED_SIGN_UP_STATE = "Unsupported sign up state : ";
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		Locale locale = (Locale) requestWrapper.getSessionAttribute(LOCALE);
		ClientInfo clientInfo = getClient(requestWrapper);
		User user = getUser(requestWrapper);
		ClientManager clientManager = managerFactory.getClientManager();
		try {
			SignUpResult state = clientManager.signUpClient(clientInfo, user);
			switch (state) {
			case CLIENT_NOT_EXIST:
				return fail(requestWrapper, locale, "client.signup.clientNotExist");
			case LOGIN_EXIST:
				return fail(requestWrapper, locale, "client.signup.loginExist");
			case SUCCESS:
				return success(requestWrapper, locale);
			default:
				throw new ActionCommandException(UNSUPPORTED_SIGN_UP_STATE
						+ state);
			}
		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
	}
	
	private String fail(SessionRequestWrapper requestWrapper, Locale locale, String errorKey) {
		String message = MessageManager.getProperty(
				errorKey, locale);
		requestWrapper.setAttribute(ERROR, message);
		return ConfigurationManager.getProperty(PAGE_REGISTRATION);
	}

	private String success(SessionRequestWrapper requestWrapper, Locale locale) {
		String message;
		message = MessageManager.getProperty("client.signup.success",
				locale);
		requestWrapper.setAttribute(REGISTRATION_MESSAGE, message);
		return ConfigurationManager.getProperty(PAGE_LOGIN);
	}

	private User getUser(SessionRequestWrapper requestWrapper) {
		String login = requestWrapper.getParameter("login");
		String password = requestWrapper.getParameter("password");
		User user = new User();
		user.setLogin(login);
		user.setPassword(password);
		return user;
	}

	private ClientInfo getClient(SessionRequestWrapper requestWrapper) {
		String firstname = requestWrapper.getParameter("firstname");
		String surname = requestWrapper.getParameter("surname");
		String middlename = requestWrapper.getParameter("middlename");
		String passportNumber = requestWrapper.getParameter("passportNumber");
		ClientInfo clientInfo = new ClientInfo();
		clientInfo.setFirstname(firstname);
		clientInfo.setSurname(surname);
		clientInfo.setMiddlename(middlename);
		clientInfo.setPassportNumber(passportNumber);
		return clientInfo;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * All users have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		return true;
	}
}
