package by.epam.javatraining.zarenok.finalproject.commands.admin;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.Account;
import by.epam.javatraining.zarenok.finalproject.entity.CreditCard;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.manager.AccountManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.AddEntityResult;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.resourceutil.MessageManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;
import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.*;

/**
 * AdminAddCardCommand allows process adding card request.
 * @author Vasil Zaranok
 *
 */
public class AdminAddCardCommand implements ActionCommand {
	
	/**
	 * @inheritDoc
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		int accountId = Integer.parseInt(requestWrapper
				.getParameter(ACCOUNT_ID));
		String step = requestWrapper.getParameter("addCardStep");
		switch (step) {
		case "add":
			return add(requestWrapper, accountId);
		case "show":
			return show(requestWrapper, accountId);
		default:
			throw new ActionCommandException("Unsupported command step : "
					+ step);
		}
	}

	private String show(SessionRequestWrapper requestWrapper, int accountId) {
		requestWrapper.setAttribute(ACCOUNT_ID, accountId);
		return ConfigurationManager
				.getProperty("path.page.adminShowAddCard");
	}

	private String add(SessionRequestWrapper requestWrapper, int accountId)
			throws ActionCommandException {
		try {
			CreditCard card = getCard(requestWrapper);
			AccountManager accountManager = managerFactory.getAccountManager();
			AddEntityResult state = accountManager.addCard(card, accountId);
			if (state.isSuccess()) {
				return goToSuccess(accountId, requestWrapper, accountManager);
			} else {
				return goToError(accountId, requestWrapper);
			}
		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
	}

	private String goToSuccess(int accountId,
			SessionRequestWrapper requestWrapper, AccountManager accountManager)
			throws ManagerException {
		requestWrapper.setAttribute(ACCOUNT_ID, accountId);
		Account account = accountManager.getAccountById(accountId);
		requestWrapper.setAttribute(ACCOUNT, account);
		return ConfigurationManager
				.getProperty("path.page.adminShowAccount");
	}

	private String goToError(int accountId, SessionRequestWrapper requestWrapper) {
		Locale locale = (Locale) requestWrapper.getSessionAttribute(LOCALE);
		requestWrapper.setAttribute(ERROR,
				MessageManager.getProperty("admin.addCard.exist", locale));
		return show(requestWrapper, accountId);
	}

	private CreditCard getCard(SessionRequestWrapper requestWrapper)
			throws ManagerException {
		String number = requestWrapper.getParameter("number");
		String expirationDateString = requestWrapper
				.getParameter("expirationDate");
		SimpleDateFormat format = new SimpleDateFormat("mm/yy");
		java.util.Date parsed;
		try {
			parsed = format.parse(expirationDateString);
		} catch (ParseException e) {
			throw new ManagerException(ERROR, e);
		}
		Date date = new Date(parsed.getTime());
		CreditCard card = new CreditCard();
		card.setNumber(number);
		card.setExpirationDate(date);
		return card;
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * Only administrators have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		if (role == Role.ADMIN) {
			return true;
		} else {
			return false;
		}
	}
}
