package by.epam.javatraining.zarenok.finalproject.commands.client;

import java.util.Locale;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.PaymentTemplate;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.manager.PaymentManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;
import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.*;

/**
 * {@link ShowMakePaymentCommand} allows process show make payment page request.
 * @author Vasil Zaranok
 *
 */
public class ShowMakePaymentCommand implements ActionCommand {
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		int creditCardId = Integer.parseInt(requestWrapper
				.getParameter(CREDIT_CARD_ID));
		int paymentTemplateId = Integer.parseInt(requestWrapper
				.getParameter(PAYMENT_TEMPLATE_ID));
		PaymentManager paymentManager = managerFactory.getPaymentManager();
		Locale locale = (Locale) requestWrapper.getSessionAttribute(LOCALE);
		try {
			PaymentTemplate paymentTemplate = paymentManager
					.getPaymentTemplate(paymentTemplateId, locale);
			requestWrapper.setAttribute(CREDIT_CARD_ID, creditCardId);
			requestWrapper.setAttribute(PAYMENT_TEMPLATE, paymentTemplate);
			return ConfigurationManager
					.getProperty("path.page.showMakePayment");
		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * Only clients have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		if (role == Role.CLIENT) {
			return true;
		} else {
			return false;
		}
	}
}
