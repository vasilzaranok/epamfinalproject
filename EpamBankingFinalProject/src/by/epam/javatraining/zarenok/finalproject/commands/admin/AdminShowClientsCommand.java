package by.epam.javatraining.zarenok.finalproject.commands.admin;

import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.EXCEPTION_ERROR_LOGIC;

import java.util.List;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.ClientInfo;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.manager.ClientManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.GetPartResult;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;

/**
 * AdminShowClientsCommand allows to process show all clients request.
 * @author Vasil Zaranok
 *
 */
public class AdminShowClientsCommand implements ActionCommand {

	private static final String TOTAL_CLIENTS = "totalClients";
	private static final String ON_PAGE_COUNT = "onPageCount";
	private static final String PAGE = "page";
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		int page = getPage(requestWrapper);
		int onPageCount = getOnPageCount(requestWrapper);
		int from = onPageCount * (page - 1);
		int count = onPageCount;
		ClientManager clientManager = managerFactory.getClientManager();
		try {
			GetPartResult<ClientInfo> state = clientManager.getClientInfoList(
					from, count);
			List<ClientInfo> clientInfoList = state.getList();
			int totalClients = state.getTotalCount();
			requestWrapper.setAttribute(CLIENTS, clientInfoList);
			requestWrapper.setAttribute(TOTAL_CLIENTS, totalClients);
			return ConfigurationManager.getProperty("path.page.adminShowClients");
		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
	}
	
	private int getOnPageCount(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		int onPageCount = Integer.parseInt(requestWrapper
				.getParameter(ON_PAGE_COUNT));
		if (onPageCount < 1) {
			throw new ActionCommandException("Illegal parameter '"
					+ ON_PAGE_COUNT + "' (< 1) : " + onPageCount);
		}
		return onPageCount;
	}

	private int getPage(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		int page;
		try {
			page = Integer.parseInt(requestWrapper.getParameter(PAGE));
		} catch (NumberFormatException e) {
			throw new ActionCommandException("Illegal parameter '" + PAGE
					+ "': " + requestWrapper.getParameter(PAGE));
		}
		if (page < 1) {
			throw new ActionCommandException("Illegal parameter '" + PAGE
					+ "' (< 1) : " + page);
		}
		return page;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Only administrators have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		if (role == Role.ADMIN) {
			return true;
		} else {
			return false;
		}
	}
}
