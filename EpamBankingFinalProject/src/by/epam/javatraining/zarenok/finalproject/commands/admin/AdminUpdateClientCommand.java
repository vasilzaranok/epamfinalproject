package by.epam.javatraining.zarenok.finalproject.commands.admin;

import java.util.List;
import java.util.Locale;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.AccountInfo;
import by.epam.javatraining.zarenok.finalproject.entity.ClientInfo;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.manager.AccountManager;
import by.epam.javatraining.zarenok.finalproject.manager.ClientManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.UpdateClientDataResult;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.resourceutil.MessageManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;
import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.*;
import static by.epam.javatraining.zarenok.finalproject.manager.operationresult.UpdateClientDataResult.*;

/**
 * {@link AdminUpdateClientCommand} allows process update client's data request.
 * 
 * @author Vasil Zaranok
 *
 */
public class AdminUpdateClientCommand implements ActionCommand {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		int clientId = Integer.parseInt(requestWrapper.getParameter(CLIENT_ID));
		String step = requestWrapper.getParameter("updateClientStep");
		try {
			switch (step) {
			case "show":
				return showUpdateDialog(requestWrapper, clientId);
			case "update":
				return updateClientData(clientId, requestWrapper);
			default:
				throw new ActionCommandException("Unsupported command step : "
						+ step);
			}
		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
	}

	private String showUpdateDialog(SessionRequestWrapper requestWrapper,
			int clientId) throws ManagerException {
		requestWrapper.setAttribute(CLIENT_ID, clientId);
		ClientManager clientManager = managerFactory.getClientManager();
		ClientInfo client = clientManager.getClientInfoById(clientId);
		requestWrapper.setAttribute(CLIENT_INFO, client);
		return ConfigurationManager
				.getProperty("path.page.adminUpdateClientData");
	}

	private String updateClientData(int clientId,
			SessionRequestWrapper requestWrapper)
			throws ActionCommandException, ManagerException {
		ClientInfo client = getClient(clientId, requestWrapper);
		ClientManager clientManager = managerFactory.getClientManager();
		UpdateClientDataResult state = clientManager.updateClientData(client);
		if (state == SUCCESS) {
			return success(clientId, requestWrapper, clientManager);
		} else if (state == ANOTHER_CLIENT_EXIST) {
			return anotherClientExist(clientId, requestWrapper, clientManager);
		} else {
			throw new ActionCommandException(
					"Unsupported update client state: " + state.toString());
		}
	}

	private String anotherClientExist(int clientId,
			SessionRequestWrapper requestWrapper, ClientManager clientManager)
			throws ManagerException {
		Locale locale = (Locale) requestWrapper.getSessionAttribute(LOCALE);
		requestWrapper.setAttribute(ERROR,
				MessageManager.getProperty("admin.updateClient.exist", locale));
		requestWrapper.setAttribute(CLIENT_ID, clientId);
		ClientInfo clientToUpdate = clientManager.getClientInfoById(clientId);
		requestWrapper.setAttribute(CLIENT_INFO, clientToUpdate);
		return ConfigurationManager
				.getProperty("path.page.adminUpdateClientData");
	}

	private String success(int clientId, SessionRequestWrapper requestWrapper,
			ClientManager clientManager) throws ManagerException {
		ClientInfo clientInfo;
		clientInfo = clientManager.getClientInfoById(clientId);
		AccountManager accountManager = managerFactory.getAccountManager();
		List<AccountInfo> accountInfoList = accountManager
				.getAccountInfoList(clientId);
		requestWrapper.setAttribute(ACCOUNTS, accountInfoList);
		requestWrapper.setAttribute(CLIENT_INFO, clientInfo);
		return ConfigurationManager.getProperty("path.page.adminShowClient");
	}

	private ClientInfo getClient(int clientId,
			SessionRequestWrapper requestWrapper) {
		String firstname = requestWrapper.getParameter("firstname");
		String surname = requestWrapper.getParameter("surname");
		String middlename = requestWrapper.getParameter("middlename");
		String passportNumber = requestWrapper.getParameter("passportNumber");
		ClientInfo client = new ClientInfo();
		client.setId(clientId);
		client.setFirstname(firstname);
		client.setSurname(surname);
		client.setMiddlename(middlename);
		client.setPassportNumber(passportNumber);
		return client;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Only administrators have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		if (role == Role.ADMIN) {
			return true;
		} else {
			return false;
		}
	}
}