package by.epam.javatraining.zarenok.finalproject.commands.client;

import java.util.Locale;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.Payment;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.manager.PaymentManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;
import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.*;

/**
 * {@link ShowPaymentCommand} allows process show payment request.
 * @author Vasil Zaranok
 *
 */
public class ShowPaymentCommand implements ActionCommand {
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		int paymentId = Integer.parseInt(requestWrapper
				.getParameter(PAYMENT_ID));
		PaymentManager paymentManager = managerFactory.getPaymentManager();
		try {
			Locale locale = (Locale) requestWrapper.getSessionAttribute(LOCALE);
			Payment payment = paymentManager.getPayment(paymentId, locale);
			requestWrapper.setAttribute(PAYMENT, payment);
			return ConfigurationManager.getProperty("path.page.showPayment");
		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * Only clients have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		if (role == Role.CLIENT) {
			return true;
		} else {
			return false;
		}
	}
}
