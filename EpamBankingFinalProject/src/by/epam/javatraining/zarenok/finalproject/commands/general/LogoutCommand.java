package by.epam.javatraining.zarenok.finalproject.commands.general;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;

/**
 * LogoutCommand allows process log out request.
 * @author Vasil Zaranok
 *
 */
public class LogoutCommand implements ActionCommand {
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper) {
		requestWrapper.invalidateSession();
		return ConfigurationManager.getProperty("path.page.login");
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * All users have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		return true;
	}
}