package by.epam.javatraining.zarenok.finalproject.commands;

import by.epam.javatraining.zarenok.finalproject.commands.admin.AdminAddAccountCommand;
import by.epam.javatraining.zarenok.finalproject.commands.admin.AdminAddCardCommand;
import by.epam.javatraining.zarenok.finalproject.commands.admin.AdminAddClientCommand;
import by.epam.javatraining.zarenok.finalproject.commands.admin.AdminShowAccountCommand;
import by.epam.javatraining.zarenok.finalproject.commands.admin.AdminShowClientCommand;
import by.epam.javatraining.zarenok.finalproject.commands.admin.AdminShowClientsCommand;
import by.epam.javatraining.zarenok.finalproject.commands.admin.AdminUnblockAccountCommand;
import by.epam.javatraining.zarenok.finalproject.commands.admin.AdminUpdateClientCommand;
import by.epam.javatraining.zarenok.finalproject.commands.client.BlockAccountCommand;
import by.epam.javatraining.zarenok.finalproject.commands.client.CancelPaymentCommand;
import by.epam.javatraining.zarenok.finalproject.commands.client.MakePaymentCommand;
import by.epam.javatraining.zarenok.finalproject.commands.client.PutMoneyIntoAccountCommand;
import by.epam.javatraining.zarenok.finalproject.commands.client.ShowAccountCommand;
import by.epam.javatraining.zarenok.finalproject.commands.client.ShowAccountsCommand;
import by.epam.javatraining.zarenok.finalproject.commands.client.ShowCardCommand;
import by.epam.javatraining.zarenok.finalproject.commands.client.ShowMakePaymentCommand;
import by.epam.javatraining.zarenok.finalproject.commands.client.ShowPaymentCommand;
import by.epam.javatraining.zarenok.finalproject.commands.client.ShowPaymentTemplatesCommand;
import by.epam.javatraining.zarenok.finalproject.commands.client.ShowPaymentsCommand;
import by.epam.javatraining.zarenok.finalproject.commands.client.SignUpCommand;
import by.epam.javatraining.zarenok.finalproject.commands.general.ChooseLanguageCommand;
import by.epam.javatraining.zarenok.finalproject.commands.general.LoginCommand;
import by.epam.javatraining.zarenok.finalproject.commands.general.LogoutCommand;
import by.epam.javatraining.zarenok.finalproject.servlet.RequestHelper;

/**
 * ActionCommandEnum allows {@link RequestHelper} get ActionCommand objects
 * @author Vasil Zaranok
 *
 */
public enum ActionCommandEnum {
	LOGIN {
		{
			command = new LoginCommand();
		}
	},
	LOGOUT {
		{
			command = new LogoutCommand();
		}
	},
	CHOOSE_LANGUAGE {
		{
			command = new ChooseLanguageCommand();
		}
	},
	SIGN_UP {
		{
			command = new SignUpCommand();
		}
	},
	SHOW_ACCOUNTS {
		{
			command = new ShowAccountsCommand();
		}
	},
	SHOW_CREDIT_CARD {
		{
			command = new ShowCardCommand();
		}
	},
	SHOW_ACCOUNT {
		{
			command = new ShowAccountCommand();
		}
	},
	SHOW_PAYMENT_TEMPLATES {
		{
			command = new ShowPaymentTemplatesCommand();
		}
	},
	BLOCK_ACCOUNT {
		{
			command = new BlockAccountCommand();
		}
	},
	PUT_MONEY_INTO_ACCOUNT {
		{
			command = new PutMoneyIntoAccountCommand();
		}
	},
	SHOW_MAKE_PAYMENT {
		{
			command = new ShowMakePaymentCommand();
		}
	},
	MAKE_PAYMENT {
		{
			command = new MakePaymentCommand();
		}
	},
	SHOW_PAYMENT {
		{
			command = new ShowPaymentCommand();
		}
	},
	SHOW_PAYMENTS {
		{
			command = new ShowPaymentsCommand();
		}
	},
	CANCEL_PAYMENT {
		{
			command = new CancelPaymentCommand();
		}
	},
	ADMIN_SHOW_CLIENTS {
		{
			command = new AdminShowClientsCommand();
		}
	},
	ADMIN_SHOW_CLIENT {
		{
			command = new AdminShowClientCommand();
		}
	},
	ADMIN_SHOW_ACCOUNT {
		{
			command = new AdminShowAccountCommand();
		}
	},
	ADMIN_UNBLOCK_ACCOUNT {
		{
			command = new AdminUnblockAccountCommand();
		}
	},
	ADMIN_ADD_CLIENT {
		{
			command = new AdminAddClientCommand();
		}
	},
	ADMIN_ADD_ACCOUNT {
		{
			command = new AdminAddAccountCommand();
		}
	},
	ADMIN_ADD_CARD {
		{
			command = new AdminAddCardCommand();
		}
	},
	ADMIN_UPDATE_CLIENT {
		{
			command = new AdminUpdateClientCommand();
		}
	};
	ActionCommand command;
	
	/**
	 * Gets command by enum value
	 * @return ActionCommand command
	 */
	public ActionCommand getCommand() {
		return command;
	}
}
