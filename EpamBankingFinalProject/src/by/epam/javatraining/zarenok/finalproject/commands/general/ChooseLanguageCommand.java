package by.epam.javatraining.zarenok.finalproject.commands.general;

import java.util.List;
import java.util.Locale;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.ClientInfo;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.entity.User;
import by.epam.javatraining.zarenok.finalproject.manager.ClientManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;
import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.*;

/**
 * ChooseLanguageCommand allows process choose language request.
 * @author Vasil Zaranok
 *
 */
public class ChooseLanguageCommand implements ActionCommand {
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		String lang = requestWrapper.getParameter("lang");
		switch (lang) {
		case "us":
			return goHome(requestWrapper, Locale.US);
		case "ru":
			Locale locale = new Locale("ru", "RU");
			return goHome(requestWrapper, locale);
		default:
			return goHome(requestWrapper, Locale.US);
		}
	}

	private String goHome(SessionRequestWrapper requestWrapper, Locale locale)
			throws ActionCommandException {
		User user = (User) requestWrapper.getSessionAttribute(USER);
		switch (user.getRole()) {
		case CLIENT:
			return goHomeClient(requestWrapper, locale, user);
		case ADMIN:
			return goHomeAdmin(requestWrapper, locale);
		case GUEST:
			return goHomeGuest(requestWrapper, locale);
		default:
			throw new ActionCommandException(
					"Unsupported user role in choose lang command : "
							+ user.getRole());
		}
	}

	private String goHomeGuest(SessionRequestWrapper requestWrapper,
			Locale locale) {
		requestWrapper.setSessionAttribute(LOCALE, locale);
		return ConfigurationManager.getProperty("path.page.login");
	}

	private String goHomeAdmin(SessionRequestWrapper requestWrapper,
			Locale locale) throws ActionCommandException {
		ClientManager clientManager = managerFactory.getClientManager();
		requestWrapper.setSessionAttribute(LOCALE, locale);
		try {
			List<ClientInfo> clientInfoList = clientManager
					.getClientInfoList();
			requestWrapper.setAttribute(CLIENTS, clientInfoList);
		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
		return ConfigurationManager.getProperty("path.page.admin");
	}

	private String goHomeClient(SessionRequestWrapper requestWrapper,
			Locale locale, User user)
			throws ActionCommandException {
		ClientManager clientManager = managerFactory.getClientManager();
		requestWrapper.setSessionAttribute(LOCALE, locale);
		try {
			ClientInfo clientInfo = clientManager.getClientInfoByUser(user);
			requestWrapper.setAttribute(CLIENT_INFO, clientInfo);
		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
		return ConfigurationManager.getProperty("path.page.client");
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * All users have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		return true;
	}
}