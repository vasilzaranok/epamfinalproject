package by.epam.javatraining.zarenok.finalproject.commands;

/**
 * Thrown when errors occur at while command execution.
 * @author Vasil Zaranok
 *
 */
public class ActionCommandException extends Exception {
	
	public static final String EXCEPTION_ERROR_LOGIC = "Error while logic execution!";
	private static final long serialVersionUID = 1L;
	
	public ActionCommandException() { }

	public ActionCommandException(String message, Throwable cause) {
		super(message, cause);
	}

	public ActionCommandException(String message) {
		super(message);
	}

	public ActionCommandException(Throwable cause) {
		super(cause);
	}
}
