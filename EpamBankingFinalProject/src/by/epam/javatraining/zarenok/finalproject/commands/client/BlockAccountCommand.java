package by.epam.javatraining.zarenok.finalproject.commands.client;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.Account;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.manager.AccountManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;
import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.*;

/**
 * {@link BlockAccountCommand} allows process block client's account request.
 * 
 * @author Vasil Zaranok
 *
 */
public class BlockAccountCommand implements ActionCommand {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		int accountId = Integer.parseInt(requestWrapper
				.getParameter(ACCOUNT_ID));
		AccountManager accountManager = managerFactory.getAccountManager();
		try {
			accountManager.blockAccount(accountId);
			requestWrapper.setAttribute(ACCOUNT_ID, accountId);
			Account account = accountManager.getAccountById(accountId);
			requestWrapper.setAttribute(ACCOUNT, account);
			return ConfigurationManager.getProperty("path.page.showAccount");
		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Only clients have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		if (role == Role.CLIENT) {
			return true;
		} else {
			return false;
		}
	}
}
