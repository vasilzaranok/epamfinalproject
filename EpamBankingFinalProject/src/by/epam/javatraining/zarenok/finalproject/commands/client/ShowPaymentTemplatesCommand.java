package by.epam.javatraining.zarenok.finalproject.commands.client;

import java.util.List;
import java.util.Locale;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.PaymentTemplate;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.manager.PaymentManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;
import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.*;

/**
 * {@link ShowPaymentTemplatesCommand} allows process show payment templates
 * request.
 * 
 * @author Vasil Zaranok
 *
 */
public class ShowPaymentTemplatesCommand implements ActionCommand {

	private static final String PAYMENT_TEMPLATES = "paymentTemplates";
	private static final String CREDIT_CARD_ID = "creditCardId";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		int creditcardId = Integer.parseInt(requestWrapper
				.getParameter(CREDIT_CARD_ID));
		PaymentManager paymentManager = managerFactory.getPaymentManager();
		Locale locale = (Locale) requestWrapper.getSessionAttribute(LOCALE);
		try {
			List<PaymentTemplate> paymentTemplates = paymentManager
					.getPaymentTemplates(locale);
			requestWrapper.setAttribute(PAYMENT_TEMPLATES, paymentTemplates);
			requestWrapper.setAttribute(CREDIT_CARD_ID, creditcardId);
			return ConfigurationManager
					.getProperty("path.page.showPaymentTemplates");
		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Only clients have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		if (role == Role.CLIENT) {
			return true;
		} else {
			return false;
		}
	}
}
