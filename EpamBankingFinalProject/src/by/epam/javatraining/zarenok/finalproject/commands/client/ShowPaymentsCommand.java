package by.epam.javatraining.zarenok.finalproject.commands.client;

import java.util.List;
import java.util.Locale;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.Payment;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.manager.PaymentManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.GetPartResult;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;
import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.*;

/**
 * {@link ShowPaymentsCommand} allows process show all client payments request.
 * @author Vasil Zaranok
 *
 */
public class ShowPaymentsCommand implements ActionCommand {

	private static final String TOTAL_PAYMENTS = "totalPayments";
	private static final String ON_PAGE_COUNT = "onPageCount";
	private static final String PAGE = "page";
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		int page = getPage(requestWrapper);
		int onPageCount = getOnPageCount(requestWrapper);
		int from = onPageCount * (page - 1);
		int count = onPageCount;
		int clientId = (int) requestWrapper.getSessionAttribute(CLIENT_ID);
		PaymentManager manager = managerFactory.getPaymentManager();
		try {
			Locale locale = (Locale) requestWrapper.getSessionAttribute(LOCALE);
			GetPartResult<Payment> state = manager.getPayments(from, count,
					clientId, locale);
			List<Payment> payments = state.getList();
			requestWrapper.setAttribute(TOTAL_PAYMENTS, state.getTotalCount());
			requestWrapper.setAttribute(PAYMENTS, payments);
			return ConfigurationManager.getProperty("path.page.showPayments");
		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
	}

	private int getOnPageCount(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		int onPageCount = Integer.parseInt(requestWrapper
				.getParameter(ON_PAGE_COUNT));
		if (onPageCount < 1) {
			throw new ActionCommandException("Illegal parameter '"
					+ ON_PAGE_COUNT + "' (< 1) : " + onPageCount);
		}
		return onPageCount;
	}

	private int getPage(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		int page;
		try {
			page = Integer.parseInt(requestWrapper.getParameter(PAGE));
		} catch (NumberFormatException e) {
			throw new ActionCommandException("Illegal parameter '" + PAGE
					+ "': " + requestWrapper.getParameter(PAGE));
		}
		if (page < 1) {
			throw new ActionCommandException("Illegal parameter '" + PAGE
					+ "' (< 1) : " + page);
		}
		return page;
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * Only clients have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		if (role == Role.CLIENT) {
			return true;
		} else {
			return false;
		}
	}
}
