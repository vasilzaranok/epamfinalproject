package by.epam.javatraining.zarenok.finalproject.commands.admin;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.Account;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.manager.AccountManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;
import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.*;

/**
 * AdminShowAccountCommand allows process show account request.
 * @author Vasil Zaranok
 *
 */
public class AdminShowAccountCommand implements ActionCommand {
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		int accountId = Integer.parseInt(requestWrapper
				.getParameter(ACCOUNT_ID));
		AccountManager accountManager = managerFactory.getAccountManager();
		Account account = null;
		try {
			account = accountManager.getAccountById(accountId);
		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
		requestWrapper.setAttribute(ACCOUNT, account);
		return ConfigurationManager.getProperty("path.page.adminShowAccount");
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * Only administrators have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		if (role == Role.ADMIN) {
			return true;
		} else {
			return false;
		}
	}
}
