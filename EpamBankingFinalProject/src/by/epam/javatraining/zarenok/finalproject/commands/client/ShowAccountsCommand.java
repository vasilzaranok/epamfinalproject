package by.epam.javatraining.zarenok.finalproject.commands.client;

import java.util.List;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.AccountInfo;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.manager.AccountManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;
import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.*;

/**
 * {@link ShowAccountsCommand} allows process show all client's account request.
 * 
 * @author Vasil Zaranok
 *
 */
public class ShowAccountsCommand implements ActionCommand {

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		int id = (int) requestWrapper.getSessionAttribute(CLIENT_ID);
		AccountManager accountManager = managerFactory.getAccountManager();
		try {
			List<AccountInfo> accountInfoList = accountManager
					.getAccountInfoList(id);
			requestWrapper.setAttribute(ACCOUNTS, accountInfoList);
			return ConfigurationManager.getProperty("path.page.showAccounts");
		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Only clients have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		if (role == Role.CLIENT) {
			return true;
		} else {
			return false;
		}
	}
}
