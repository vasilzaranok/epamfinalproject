package by.epam.javatraining.zarenok.finalproject.commands.admin;

import java.math.BigDecimal;
import java.util.Locale;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.Account;
import by.epam.javatraining.zarenok.finalproject.entity.AccountInfo;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.manager.AccountManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.AddEntityResult;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.resourceutil.MessageManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;
import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.*;

/**
 * AdminAddAccountCommand allows process adding account request.
 * 
 * @author Vasil Zaranok
 *
 */
public class AdminAddAccountCommand implements ActionCommand {

	private static final String BALANCE = "balance";
	private static final String NUMBER = "number";

	/**
	 * @inheritDoc
	 * 
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		int clientId = Integer.parseInt(requestWrapper.getParameter(CLIENT_ID));
		String step = requestWrapper.getParameter("addAccountStep");
		switch (step) {
		case "add":
			return add(requestWrapper, clientId);
		case "show":
			return show(requestWrapper, clientId);
		default:
			throw new ActionCommandException("Unsupported command step : "
					+ step);
		}
	}

	private String show(SessionRequestWrapper requestWrapper, int clientId) {
		requestWrapper.setAttribute(CLIENT_ID, clientId);
		return ConfigurationManager
				.getProperty("path.page.adminShowAddAccount");
	}

	private String add(SessionRequestWrapper requestWrapper, int clientId)
			throws ActionCommandException {
		try {
			AccountInfo accountInfo = getAccount(requestWrapper);
			AccountManager accountManager = managerFactory.getAccountManager();
			AddEntityResult state = accountManager.addAccount(accountInfo,
					clientId);
			if (state.isSuccess()) {
				return goToSuccess(requestWrapper, clientId, state.getId());
			} else {
				return goToError(requestWrapper, clientId);
			}
		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
	}

	private String goToSuccess(SessionRequestWrapper requestWrapper,
			int clientId, int id) throws ManagerException {
		requestWrapper.setAttribute(CLIENT_ID, clientId);
		AccountManager accountManager = managerFactory.getAccountManager();
		Account account = accountManager.getAccountById(id);
		requestWrapper.setAttribute(ACCOUNT, account);
		return ConfigurationManager.getProperty("path.page.adminShowAccount");
	}

	private String goToError(SessionRequestWrapper requestWrapper, int clientId) {
		Locale locale = (Locale) requestWrapper.getSessionAttribute(LOCALE);
		requestWrapper.setAttribute(CLIENT_ID, clientId);
		requestWrapper.setAttribute(ERROR,
				MessageManager.getProperty("admin.addAccount.exist", locale));
		return ConfigurationManager
				.getProperty("path.page.adminShowAddAccount");
	}

	private AccountInfo getAccount(SessionRequestWrapper requestWrapper) {
		String number = requestWrapper.getParameter(NUMBER);
		String balanceString = requestWrapper.getParameter(BALANCE);
		BigDecimal balance = new BigDecimal(balanceString);
		AccountInfo accountInfo = new AccountInfo();
		accountInfo.setBalance(balance);
		accountInfo.setNumber(number);
		return accountInfo;
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Only administrators have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		if (role == Role.ADMIN) {
			return true;
		} else {
			return false;
		}
	}
}
