package by.epam.javatraining.zarenok.finalproject.commands.admin;

import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.EXCEPTION_ERROR_LOGIC;

import java.util.List;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.AccountInfo;
import by.epam.javatraining.zarenok.finalproject.entity.ClientInfo;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.manager.AccountManager;
import by.epam.javatraining.zarenok.finalproject.manager.ClientManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;

/**
 * AdminShowClientCommand allows process show client request.
 * @author Vasil Zaranok
 *
 */
public class AdminShowClientCommand implements ActionCommand {

	private static final String CLIENT_INFO = "clientInfo";
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		int id = Integer.parseInt(requestWrapper.getParameter(CLIENT_ID));
		ClientManager clientManager = managerFactory.getClientManager();
		try {
			ClientInfo clientInfo = clientManager.getClientInfoById(id);
			AccountManager accountManager = managerFactory.getAccountManager();
			List<AccountInfo> accountInfoList = accountManager
					.getAccountInfoList(id);
			requestWrapper.setAttribute(ACCOUNTS, accountInfoList);
			requestWrapper.setAttribute(CLIENT_INFO, clientInfo);
			return ConfigurationManager
					.getProperty("path.page.adminShowClient");
		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * Only administrators have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		if (role == Role.ADMIN) {
			return true;
		} else {
			return false;
		}
	}
}