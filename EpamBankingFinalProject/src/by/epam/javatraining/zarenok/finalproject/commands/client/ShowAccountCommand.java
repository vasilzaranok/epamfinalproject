package by.epam.javatraining.zarenok.finalproject.commands.client;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.Account;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.manager.AccountManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;
import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.*;

/**
 * {@link ShowAccountCommand} allows process show client's account request.
 * @author Vasil Zaranok
 *
 */
public class ShowAccountCommand implements ActionCommand {
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		int id = Integer.parseInt(requestWrapper.getParameter(ACCOUNT_ID));
		AccountManager accountManager = managerFactory.getAccountManager();
		try {
			Account account = accountManager.getAccountById(id);
			requestWrapper.setAttribute(ACCOUNT, account);
			return ConfigurationManager.getProperty("path.page.showAccount");
		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * Only clients have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		if (role == Role.CLIENT) {
			return true;
		} else {
			return false;
		}
	}
}
