package by.epam.javatraining.zarenok.finalproject.commands;

import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.manager.util.ManagerFactory;
import by.epam.javatraining.zarenok.finalproject.servlet.BankingServlet;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;

/**
 * ActionCommand allows {@link BankingServlet } servlet process requests. Every
 * command gets parameters from request, do logic, sets attributes and returns
 * path to page for further processing.
 * 
 * @author Vasil Zaranok
 *
 */
public interface ActionCommand {
	final String USER = "user";
	final String CLIENT_ID = "clientId";
	final String CLIENT_INFO = "clientInfo";
	final String ACCOUNT_ID = "accountId";
	final String CREDIT_CARD = "creditCard";
	final String CREDIT_CARD_ID = "creditCardId";
	final String PAYMENT = "payment";
	final String PAYMENT_TEMPLATE = "paymentTemplate";
	final String PAYMENT_TEMPLATE_ID = "paymentTemplateId";
	final String PAYMENT_ID = "paymentId";
	final String ACCOUNT = "account";
	final String ACCOUNTS = "accountInfoList";
	final String PAYMENTS = "payments";
	final String CLIENTS = "clientInfoList";
	final String LOCALE = "locale";
	final String ERROR = "userError";
	ManagerFactory managerFactory = ManagerFactory.INSTANCE;

	/**
	 * Execute action command.
	 * 
	 * @param requestWrapper
	 *            {@link SessionRequestWrapper} object with request parameters
	 * @return path to page that should be displayed after executing command
	 * @throws ActionCommandException
	 *             if an error occurs while command execution
	 */
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException;

	/**
	 * Checks access user to command.
	 * 
	 * @param role
	 *            role of checked user
	 * @return true if user has access; false otherwise
	 */
	public boolean hasAccess(Role role);
}
