package by.epam.javatraining.zarenok.finalproject.commands.client;

import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.EXCEPTION_ERROR_LOGIC;

import java.math.BigDecimal;
import java.util.Locale;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.Account;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.manager.AccountManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.PutMoneyResult;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.resourceutil.MessageManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;

/**
 * {@link PutMoneyIntoAccountCommand} allows process put money into client's
 * account request.
 * 
 * @author Vasil Zaranok
 *
 */
public class PutMoneyIntoAccountCommand implements ActionCommand {

	private static final String PUT_MONEY_SUM = "putMoneySum";
	private static final String PUT_MONEY_STEP = "putMoneyStep";

	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		int accountId = Integer.parseInt(requestWrapper
				.getParameter(ACCOUNT_ID));
		String step = requestWrapper.getParameter(PUT_MONEY_STEP);
		switch (step) {
		case "show":
			return show(requestWrapper, accountId);
		case "put":
			return putMoney(requestWrapper, accountId);
		default:
			throw new ActionCommandException("Unsupported command step : "
					+ step);
		}
	}

	private String show(SessionRequestWrapper requestWrapper, int accountId) {
		requestWrapper.setAttribute(ACCOUNT_ID, accountId);
		return ConfigurationManager.getProperty("path.page.showPutMoney");
	}

	private String putMoney(SessionRequestWrapper requestWrapper, int accountId)
			throws ActionCommandException {
		try {
			String val = requestWrapper.getParameter(PUT_MONEY_SUM);
			BigDecimal sum = new BigDecimal(val);
			AccountManager accountManager = managerFactory.getAccountManager();
			PutMoneyResult state = accountManager.putMoneyIntoAccount(sum,
					accountId);
			switch (state) {
			case SUCCESS:
				return success(requestWrapper, accountId);
			case ACCOUNT_BLOCKED:
				return blocked(requestWrapper, accountId);
			default:
				throw new ActionCommandException(
						"Unsupported put money state : " + state.toString());
			}
		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
	}

	private String success(SessionRequestWrapper requestWrapper, int accountId)
			throws ManagerException {
		requestWrapper.setAttribute(ACCOUNT_ID, accountId);
		AccountManager accountManager = managerFactory.getAccountManager();
		Account account = accountManager.getAccountById(accountId);
		requestWrapper.setAttribute(ACCOUNT, account);
		return ConfigurationManager.getProperty("path.page.showAccount");
	}

	private String blocked(SessionRequestWrapper requestWrapper, int accountId) {
		Locale locale = (Locale) requestWrapper.getSessionAttribute(LOCALE);
		requestWrapper.setAttribute(ERROR, MessageManager.getProperty(
				"client.putMonet.accountBlocked", locale));
		return show(requestWrapper, accountId);
	}

	/**
	 * {@inheritDoc}
	 * 
	 * Only clients have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		if (role == Role.CLIENT) {
			return true;
		} else {
			return false;
		}
	}
}
