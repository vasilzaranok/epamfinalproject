package by.epam.javatraining.zarenok.finalproject.commands.client;

import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.EXCEPTION_ERROR_LOGIC;

import java.math.BigDecimal;
import java.util.Locale;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.CreditCard;
import by.epam.javatraining.zarenok.finalproject.entity.PaymentInfo;
import by.epam.javatraining.zarenok.finalproject.entity.PaymentTemplate;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.manager.AccountManager;
import by.epam.javatraining.zarenok.finalproject.manager.PaymentManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.PaymentResult;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.resourceutil.MessageManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;

/**
 * {@link MakePaymentCommand} allows process make payment request.
 * @author Vasil Zaranok
 *
 */
public class MakePaymentCommand implements ActionCommand {

	private static final String UNSUPPORTED_PAYMENT_STATE = "Unsupported payment state!";
	private static final String PAYMENT_DATA = "paymentData";
	private static final String PAYMENT_SUM = "paymentSum";
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		PaymentManager paymentManager = managerFactory.getPaymentManager();
		int creditCardId = Integer.parseInt(requestWrapper
				.getParameter(CREDIT_CARD_ID));
		int paymentTemplateId = Integer.parseInt(requestWrapper
				.getParameter(PAYMENT_TEMPLATE_ID));
		PaymentInfo payment = getPayment(requestWrapper);
		try {
			PaymentResult state = paymentManager.makePayment(creditCardId,
					paymentTemplateId, payment);
			switch (state) {
			case SUCCESS:
				return success(requestWrapper, creditCardId);
			case ACCOUNT_BLOCKED:
				return blocked(requestWrapper, creditCardId, paymentTemplateId);
			case NOT_ENOUGH_MONEY:
				return notEnoughMoney(requestWrapper, creditCardId,
						paymentTemplateId);
			default:
				throw new ActionCommandException(UNSUPPORTED_PAYMENT_STATE);
			}

		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
	}

	private String notEnoughMoney(SessionRequestWrapper requestWrapper,
			int creditCardId, int paymentTemplateId) throws ManagerException {
		Locale locale = (Locale) requestWrapper.getSessionAttribute(LOCALE);
		String msg = MessageManager.getProperty(
				"client.makePayment.notEnoughMoney", locale);
		return failPayment(requestWrapper, creditCardId, paymentTemplateId,
				msg, locale);
	}

	private String blocked(SessionRequestWrapper requestWrapper,
			int creditCardId, int paymentTemplateId) throws ManagerException {
		Locale locale = (Locale) requestWrapper.getSessionAttribute(LOCALE);
		String message = MessageManager.getProperty(
				"client.makePayment.accountBlocked", locale);
		return failPayment(requestWrapper, creditCardId, paymentTemplateId,
				message, locale);
	}

	private String success(SessionRequestWrapper requestWrapper,
			int creditCardId) throws ManagerException {
		requestWrapper.setAttribute(CREDIT_CARD_ID, creditCardId);
		AccountManager accountManager = managerFactory.getAccountManager();
		CreditCard creditCard = accountManager.getCreditCardById(creditCardId);
		requestWrapper.setAttribute(CREDIT_CARD, creditCard);
		return ConfigurationManager.getProperty("path.page.showCard");
	}

	private PaymentInfo getPayment(SessionRequestWrapper requestWrapper) {
		String paymentData = requestWrapper.getParameter(PAYMENT_DATA);
		String paymentString = requestWrapper.getParameter(PAYMENT_SUM);
		BigDecimal sum = new BigDecimal(paymentString);
		PaymentInfo payment = new PaymentInfo();
		payment.setPaymentData(paymentData);
		payment.setSum(sum);
		return payment;
	}

	private String failPayment(SessionRequestWrapper requestWrapper,
			int creditCardId, int paymentTemplateId, String message,
			Locale locale) throws ManagerException {
		PaymentManager paymentManager = managerFactory.getPaymentManager();
		PaymentTemplate paymentTemplate = paymentManager.getPaymentTemplate(
				paymentTemplateId, locale);
		requestWrapper.setAttribute(CREDIT_CARD_ID, creditCardId);
		requestWrapper.setAttribute(PAYMENT_TEMPLATE, paymentTemplate);
		requestWrapper.setAttribute(ERROR, message);
		return ConfigurationManager.getProperty("path.page.showMakePayment");
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * Only clients have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		if (role == Role.CLIENT) {
			return true;
		} else {
			return false;
		}
	}
}
