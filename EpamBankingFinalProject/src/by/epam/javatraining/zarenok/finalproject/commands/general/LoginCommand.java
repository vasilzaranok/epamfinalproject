package by.epam.javatraining.zarenok.finalproject.commands.general;

import java.util.Locale;

import by.epam.javatraining.zarenok.finalproject.commands.ActionCommand;
import by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException;
import by.epam.javatraining.zarenok.finalproject.entity.ClientInfo;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.entity.User;
import by.epam.javatraining.zarenok.finalproject.manager.ClientManager;
import by.epam.javatraining.zarenok.finalproject.manager.UserManager;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.resourceutil.ConfigurationManager;
import by.epam.javatraining.zarenok.finalproject.resourceutil.MessageManager;
import by.epam.javatraining.zarenok.finalproject.servlet.SessionRequestWrapper;
import static by.epam.javatraining.zarenok.finalproject.commands.ActionCommandException.*;

/**
 * LoginCommand allows process log in request.
 * @author Vasil Zaranok
 *
 */
public class LoginCommand implements ActionCommand {

	private static final String USER_NOT_FOUND = "userNotFound";
	private static final String PARAM_LOGIN = "login";
	private static final String PARAM_PASSWORD = "password";
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public String execute(SessionRequestWrapper requestWrapper)
			throws ActionCommandException {
		String login = requestWrapper.getParameter(PARAM_LOGIN);
		String password = requestWrapper.getParameter(PARAM_PASSWORD);
		UserManager userManager = managerFactory.getUserManager();
		try {
			User user = userManager.getUser(login, password);
			if (user == null) {
				return failLogin(requestWrapper);
			}
			switch (user.getRole()) {
			case CLIENT:
				return loginClient(user, requestWrapper);
			case ADMIN:
				return loginAdmin(user, requestWrapper);
			default:
				return failLogin(requestWrapper);
			}
		} catch (ManagerException e) {
			throw new ActionCommandException(EXCEPTION_ERROR_LOGIC, e);
		}
	}

	private String failLogin(SessionRequestWrapper requestWrapper) {
		Locale locale = (Locale) requestWrapper.getSessionAttribute(LOCALE);
		String message = MessageManager.getProperty("login.notfound", locale);
		requestWrapper.setAttribute(USER_NOT_FOUND, message);
		return ConfigurationManager.getProperty("path.page.login");
	}

	private String loginAdmin(User user, SessionRequestWrapper requestWrapper)
			throws ManagerException {
		requestWrapper.setSessionAttribute(USER, user);
		return ConfigurationManager.getProperty("path.page.admin");
	}

	private String loginClient(User user, SessionRequestWrapper requestWrapper)
			throws ManagerException {
		ClientManager clientManager = managerFactory.getClientManager();
		ClientInfo clientInfo = clientManager.getClientInfoByUser(user);
		requestWrapper.setAttribute(CLIENT_INFO, clientInfo);
		requestWrapper.setSessionAttribute(USER, user);
		int id = clientInfo.getId();
		requestWrapper.setSessionAttribute(CLIENT_ID, id);
		return ConfigurationManager.getProperty("path.page.client");
	}
	
	/**
	 * {@inheritDoc}
	 * 
	 * All users have access.
	 */
	@Override
	public boolean hasAccess(Role role) {
		return true;
	}
}
