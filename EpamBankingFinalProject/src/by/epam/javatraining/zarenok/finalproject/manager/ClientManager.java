package by.epam.javatraining.zarenok.finalproject.manager;

import static by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException.ERROR_DB;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IClientInfoDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IUserDAO;
import by.epam.javatraining.zarenok.finalproject.db.util.ConnectionPoolException;
import by.epam.javatraining.zarenok.finalproject.entity.ClientInfo;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.entity.User;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.AddEntityResult;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.GetPartResult;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.SignUpResult;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.UpdateClientDataResult;
import by.epam.javatraining.zarenok.finalproject.manager.util.ManagerUtil;

/**
 * A ClientManager class provides operations with client data and allows get
 * information about clients.
 * 
 * @author Vasil Zaranok
 *
 */
public enum ClientManager {

	/**
	 * An instance of ClientManager
	 */
	INSTANCE;

	/**
	 * Returns a  {@link ClientInfo} object(information about client) by
	 * payment system user.
	 * 
	 * @param user
	 *  {@link User} object of user.
	 * @return a  {@link ClientInfo} object(information about client) by
	 *         payment system user
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database or user is null
	 */
	public ClientInfo getClientInfoByUser(User user) throws ManagerException {
		if (user == null) {
			throw new ManagerException("Argument user is null");
		}
		Connection connection = null;
		try {
			connection = getConnection();
			IClientInfoDAO clientInfoDAO = ManagerUtil.getDaoFactory()
					.getClientInfoDAO(connection);
			ClientInfo clientInfo = clientInfoDAO.findByUserId(user.getId());
			return clientInfo;
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	/**
	 * Returns list of  {@link ClientInfo} objects(information about client)
	 * of clients, that exist in payment system.
	 * 
	 * @return list of  {@link ClientInfo} objects(information about client)
	 *         of clients, that exist in payment system
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database
	 */
	public List<ClientInfo> getClientInfoList() throws ManagerException {
		Connection connection = null;
		try {
			connection = getConnection();
			IClientInfoDAO clientInfoDAO = ManagerUtil.getDaoFactory()
					.getClientInfoDAO(connection);
			List<ClientInfo> list = clientInfoDAO.findAll();
			return list;
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	/**
	 * Returns portion of list of  {@link ClientInfo} objects(information
	 * about client) of clients, that exist in payment system.
	 * 
	 * @param from
	 *            from(inclusive) index of portion
	 * @param count count of elements
	 * @return  {@link GetPartResult} object, that include specified range
	 *         within this list and total number of clients
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database
	 */
	public GetPartResult<ClientInfo> getClientInfoList(int from, int count)
			throws ManagerException {
		Connection connection = null;
		try {
			connection = getConnection();
			IClientInfoDAO clientInfoDAO = ManagerUtil.getDaoFactory()
					.getClientInfoDAO(connection);
			List<ClientInfo> list = clientInfoDAO.findAll(from, count);
			int total = clientInfoDAO.totalCount();
			return new GetPartResult<ClientInfo>(list, total);
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	/**
	 * Returns a  {@link ClientInfo} object(information about client), of
	 * related client.
	 * 
	 * @param id
	 *            id of related client
	 * @return a  {@link ClientInfo} object(information about client), of
	 *         related client
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database
	 */
	public ClientInfo getClientInfoById(int id) throws ManagerException {
		Connection connection = null;
		try {
			connection = getConnection();
			IClientInfoDAO clientInfoDAO = ManagerUtil.getDaoFactory()
					.getClientInfoDAO(connection);
			ClientInfo clientInfo = clientInfoDAO.findById(id);
			return clientInfo;
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	/**
	 * Add client to payment system.
	 * 
	 * @param clientInfo
	 *            {@link ClientInfo} object, information about client to add
	 * @return  {@link AddEntityResult} object represents result of adding
	 *         client
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database or @param clientInfo is null
	 */
	public AddEntityResult addClient(ClientInfo clientInfo)
			throws ManagerException {
		if (clientInfo == null) {
			throw new ManagerException("Argument clientInfo is null");
		}
		Connection connection = null;
		try {
			connection = getConnection();
			IClientInfoDAO clientInfoDAO = ManagerUtil.getDaoFactory()
					.getClientInfoDAO(connection);
			if (clientInfoDAO.tryAdd(clientInfo)) {
				int id = clientInfoDAO.add(clientInfo);
				return AddEntityResult.createSuccess(id);
			} else {
				return AddEntityResult.createEntityExist();
			}
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	/**
	 * Update client data in payment system.
	 * 
	 * @param client
	 *  {@link ClientInfo} object of client, that should replace
	 *      existing(with same id) client object in database
	 * @return  {@link UpdateClientDataResult} object represents result of
	 *         update operation
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database or client is null
	 */
	public UpdateClientDataResult updateClientData(ClientInfo client)
			throws ManagerException {
		if (client == null) {
			throw new ManagerException("Argument client is null");
		}
		Connection connection = null;
		try {
			connection = getConnection();
			IClientInfoDAO clientInfoDAO = ManagerUtil.getDaoFactory()
					.getClientInfoDAO(connection);
			if (clientInfoDAO.tryUpdate(client)) {
				clientInfoDAO.update(client);
				return UpdateClientDataResult.SUCCESS;
			} else {
				return UpdateClientDataResult.ANOTHER_CLIENT_EXIST;
			}
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	/**
	 * Sign up client in payment system. Linked user with related client in
	 * payment system.
	 * 
	 * @param clientInfo
	 *            client to sign up
	 * @param user
	 *            user to linked with related client
	 * @return  {@link UpdateClientDataResult} object represents result of
	 *         singing up client operation
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database and if clientInfo or user is null
	 * 
	 */
	public SignUpResult signUpClient(ClientInfo clientInfo, User user)
			throws ManagerException {
		if (clientInfo == null) {
			throw new ManagerException("Argument clientInfo is null");
		} else if (user == null) {
			throw new ManagerException("Argument user is null");
		}
		Connection connection = null;
		try {
			connection = getConnection();
			IUserDAO userDAO = ManagerUtil.getDaoFactory().getUserDAO(
					connection);
			User userInDatabase = userDAO.findByLogin(user.getLogin());
			if (userInDatabase != null) {
				return SignUpResult.LOGIN_EXIST;
			}
			IClientInfoDAO clientInfoDAO = ManagerUtil.getDaoFactory()
					.getClientInfoDAO(connection);
			ClientInfo clientInDatabase = clientInfoDAO.findByData(clientInfo);
			if (clientInDatabase == null) {
				return SignUpResult.CLIENT_NOT_EXIST;
			}
			connection.setAutoCommit(false);
			user.setRole(Role.CLIENT);
			int id = userDAO.add(user);
			clientInfoDAO.setUserById(clientInDatabase.getId(), id);
			connection.commit();
			return SignUpResult.SUCCESS;
		} catch (DAOException | ConnectionPoolException | SQLException e) {
			ManagerUtil.rollback(connection);
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.setAutoCommit(connection);
			ManagerUtil.closeConnection(connection);
		}
	}

	private Connection getConnection() throws ConnectionPoolException {
		return ManagerUtil.getConnection();
	}
}
