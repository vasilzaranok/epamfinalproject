package by.epam.javatraining.zarenok.finalproject.manager.operationresult;

import java.util.List;

/**
 * GetPartResult represents result of getting part of big lists in database
 * operation, GetPartState allow get partial list, and total number of entities
 * in database.
 * 
 * @author Vasil Zaranok
 *
 * @param <E> entity type, which list to get
 */
public class GetPartResult<E> {
	private List<E> list;
	int total;

	public GetPartResult(List<E> list, int total) {
		this.list = list;
		this.total = total;
	}

	public List<E> getList() {
		return list;
	}

	public int getTotalCount() {
		return total;
	}
}
