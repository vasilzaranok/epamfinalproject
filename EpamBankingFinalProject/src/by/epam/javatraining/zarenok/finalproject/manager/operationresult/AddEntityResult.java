package by.epam.javatraining.zarenok.finalproject.manager.operationresult;

/**
 * AddEntityResult represents result of insert entity to database operation,
 * if entity was inserted, AddEntityState allow get id of inserted entity.
 * 
 * @author Vasil Zaranok
 *
 */
public class AddEntityResult {
	private int id;
	private boolean success;

	private AddEntityResult() {
		this.success = false;
	}

	private AddEntityResult(int id) {
		this.id = id;
		this.success = true;
	}

	public static AddEntityResult createSuccess(int id) {
		return new AddEntityResult(id);
	}

	public static AddEntityResult createEntityExist() {
		return new AddEntityResult();
	}

	public boolean isSuccess() {
		return success;
	}

	public int getId() {
		if (!success)
			throw new IllegalStateException(
					"Getting id from unsuccessful state : ");
		return id;
	}
}
