package by.epam.javatraining.zarenok.finalproject.manager.operationresult;

/**
 * PaymentResult represents result of make payment operation.
 * 
 * @author Vasil Zaranok
 *
 */
public enum PaymentResult {

	/**
	 * Payment hasn't been made because not enough money on account.
	 * to make this payment
	 */
	NOT_ENOUGH_MONEY,
	
	/**
	 * Payment hasn't been made because account is blocked.
	 */
	ACCOUNT_BLOCKED,
	
	/**
	 * Payment has been made.  
	 */
	SUCCESS
}
