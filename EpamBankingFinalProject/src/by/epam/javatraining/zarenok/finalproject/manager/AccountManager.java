package by.epam.javatraining.zarenok.finalproject.manager;

import static by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException.ERROR_DB;

import java.math.BigDecimal;
import java.sql.Connection;
import java.util.List;

import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IAccountInfoDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.ICreditCardDAO;
import by.epam.javatraining.zarenok.finalproject.db.util.ConnectionPoolException;
import by.epam.javatraining.zarenok.finalproject.entity.Account;
import by.epam.javatraining.zarenok.finalproject.entity.AccountInfo;
import by.epam.javatraining.zarenok.finalproject.entity.CreditCard;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.AddEntityResult;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.PutMoneyResult;
import by.epam.javatraining.zarenok.finalproject.manager.util.ManagerUtil;

/**
 * An Account Manager class provides operations with account and can get related
 * information about account.
 * 
 * @author Vasil Zaranok
 * 
 */
public enum AccountManager {
	/**
	 * An singleton instance of Account Manager
	 */
	INSTANCE;

	/**
	 * Returns list with  {@link AccountInfo}(information about client
	 * account) objects, which belong to the client.
	 * 
	 * @param clientId
	 *            id of client, whose  {@link AccountInfo} objects to get
	 * @return list with AccountInfo objects, which belong to the client
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database
	 */
	public List<AccountInfo> getAccountInfoList(int clientId)
			throws ManagerException {
		Connection connection = null;
		try {
			connection = getConnection();
			IAccountInfoDAO accountInfoDAO = ManagerUtil.getDaoFactory()
					.getAccountInfoDAO(connection);
			List<AccountInfo> accountInfos = accountInfoDAO
					.findByClientId(clientId);
			return accountInfos;
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	/**
	 * Returns an  {@link Account} object(information about client account
	 * and all credit cards, linked to this account) object.
	 * 
	 * @param id
	 *            id of account
	 * @return an  {@link Account} object(information about client account
	 *         and all credit cards, linked to this account) object
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database
	 */
	public Account getAccountById(int id) throws ManagerException {
		Connection connection = null;
		try {
			connection = getConnection();
			IAccountInfoDAO accountInfoDAO = ManagerUtil.getDaoFactory()
					.getAccountInfoDAO(connection);
			AccountInfo accountInfo = accountInfoDAO.findById(id);
			ICreditCardDAO creditCardDAO = ManagerUtil.getDaoFactory()
					.getCreditCardDAO(connection);

			List<CreditCard> cards = creditCardDAO.findByAccountId(id);
			Account account = new Account();
			account.setAccountInfo(accountInfo);
			account.setCreditCards(cards);
			return account;
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	/**
	 * Returns list of CreditCard(information about client credit card) objects,
	 * which belong to the client
	 * 
	 * @param clientId
	 *            id of client, whose credit cards to get
	 * @return a list of CreditCard(information about client credit card)
	 *         objects, which belong to the client
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database
	 */
	public List<CreditCard> getAllCreditCards(int clientId)
			throws ManagerException {
		Connection connection = null;
		try {
			connection = getConnection();
			ICreditCardDAO creditCardDAO = ManagerUtil.getDaoFactory()
					.getCreditCardDAO(connection);
			List<CreditCard> cards = creditCardDAO.findByClientId(clientId);
			return cards;
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	private void accountSetBlocked(boolean blocked, int id)
			throws ManagerException {
		Connection connection = null;
		try {
			connection = getConnection();
			IAccountInfoDAO accountInfoDAO = ManagerUtil.getDaoFactory()
					.getAccountInfoDAO(connection);
			accountInfoDAO.setBlocked(blocked, id);
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	/**
	 * Block the client account. Client can't make any transaction while account
	 * is blocked.
	 * 
	 * @param id
	 *            id of account, which to block
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database
	 */
	public void blockAccount(int id) throws ManagerException {
		accountSetBlocked(true, id);
	}

	/**
	 * Put money into client account.
	 * 
	 * @param sum
	 *            sum of money, which to put
	 * @param id
	 *            id of client account
	 * @return a PutMoneyState object, that represents result of put money into
	 *         client account operation:  {@link PutMoneyResult#SUCCESS} or 
	 *         {@link PutMoneyResult#ACCOUNT_BLOCKED}
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database or if sum is illegal(null or not positive)
	 */
	public PutMoneyResult putMoneyIntoAccount(BigDecimal sum, int id)
			throws ManagerException {
		if (sum == null) {
			throw new ManagerException("Argument sum is null");
		} else if (sum.compareTo(BigDecimal.ZERO) <= 0) {
			throw new ManagerException("Illegal argument sum : " + sum
					+ ", sum must be positive");
		}
		Connection connection = null;
		try {
			connection = getConnection();
			IAccountInfoDAO accountInfoDAO = ManagerUtil.getDaoFactory()
					.getAccountInfoDAO(connection);
			AccountInfo accountInfo = accountInfoDAO.findById(id);
			if (!accountInfo.isBlocked()) {
				accountInfoDAO.increaseBalance(sum, id);
				return PutMoneyResult.SUCCESS;
			} else {
				return PutMoneyResult.ACCOUNT_BLOCKED;
			}
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	/**
	 * Returns a  {@link CreditCard} object(information about client credit
	 * card) by its id.
	 * 
	 * @param id
	 *            id of credit card
	 * @return a  {@link CreditCard} object(information about client credit
	 *         card) by its id.
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database
	 */
	public CreditCard getCreditCardById(int id) throws ManagerException {
		Connection connection = null;
		try {
			connection = getConnection();
			ICreditCardDAO creditCardDAO = ManagerUtil.getDaoFactory()
					.getCreditCardDAO(connection);
			CreditCard card = creditCardDAO.findById(id);
			return card;
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	/**
	 * Add account to related client.
	 * 
	 * @param accountInfo
	 *            account that to add.
	 * @param clientId
	 *            id of client
	 * @return  {@link AddEntityResult} object, represents result of adding
	 *         account to client
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database or accountInfo is null
	 */
	public AddEntityResult addAccount(AccountInfo accountInfo, int clientId)
			throws ManagerException {
		if (accountInfo == null) {
			throw new ManagerException("Argument accountInfo is null");
		}
		Connection connection = null;
		try {
			connection = getConnection();
			IAccountInfoDAO accountInfoDAO = ManagerUtil.getDaoFactory()
					.getAccountInfoDAO(connection);
			if (accountInfoDAO.tryAdd(accountInfo)) {
				int id = accountInfoDAO.add(accountInfo, clientId);
				return AddEntityResult.createSuccess(id);
			} else {
				return AddEntityResult.createEntityExist();
			}
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	/**
	 * Add credit card to related account.
	 * 
	 * @param card
	 *            credit card that to add.
	 * @param accountId
	 *            id of client account.
	 * @return an {@link AddEntityResult} object represents result of adding
	 *         credit card
	 * @throws {@link ManagerException} if an error occurs while working
	 *         with database or card is null
	 */
	public AddEntityResult addCard(CreditCard card, int accountId)
			throws ManagerException {
		if (card == null) {
			throw new ManagerException("Argument card is null");
		}
		Connection connection = null;
		try {
			connection = getConnection();
			ICreditCardDAO creditCardDAO = ManagerUtil.getDaoFactory()
					.getCreditCardDAO(connection);
			if (creditCardDAO.tryAdd(card)) {
				int id = creditCardDAO.add(card, accountId);
				return AddEntityResult.createSuccess(id);
			} else {
				return AddEntityResult.createEntityExist();
			}
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	/**
	 * Unblock client account. After unblock account client can make all
	 * transactions
	 * 
	 * @param accountId
	 *            id of account, which to unblock
	 * @throws ManagerException
	 *             if an error occurs while working with database
	 */
	public void unblockAccount(int accountId) throws ManagerException {
		accountSetBlocked(false, accountId);
	}

	private Connection getConnection() throws ConnectionPoolException {
		return ManagerUtil.getConnection();
	}
}
