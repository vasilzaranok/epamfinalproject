package by.epam.javatraining.zarenok.finalproject.manager;

import static by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException.ERROR_DB;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IAccountInfoDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.ICreditCardDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IPaymentInfoDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IPaymentTemplateDAO;
import by.epam.javatraining.zarenok.finalproject.db.util.ConnectionPoolException;
import by.epam.javatraining.zarenok.finalproject.entity.AccountInfo;
import by.epam.javatraining.zarenok.finalproject.entity.CreditCard;
import by.epam.javatraining.zarenok.finalproject.entity.Payment;
import by.epam.javatraining.zarenok.finalproject.entity.PaymentInfo;
import by.epam.javatraining.zarenok.finalproject.entity.PaymentTemplate;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.GetPartResult;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.PaymentCancelResult;
import by.epam.javatraining.zarenok.finalproject.manager.operationresult.PaymentResult;
import by.epam.javatraining.zarenok.finalproject.manager.util.ManagerUtil;

/**
 * A PaymentManager class provides operations with payments and allows getting
 * related information about payments in payment system.
 * 
 * @author Vasil Zaranok
 *
 */
public enum PaymentManager {

	/**
	 * An instance of PaymentManager
	 */
	INSTANCE;

	private static final String ERROR_WHILE_TRANSACTION = "Error while transaction : ";
	private Calendar calendar = Calendar.getInstance();

	/**
	 * Returns list of  {@link PaymentTemplate} objects, which contain
	 * information that allow client make payment.
	 * 
	 * @param locale
	 *            locale to specify language of strings in PaymentTemplate
	 *            object
	 * @return list of  {@link PaymentTemplate} objects, which contain
	 *         information that allow client make payment.
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database
	 */
	public List<PaymentTemplate> getPaymentTemplates(Locale locale)
			throws ManagerException {
		Connection connection = null;
		try {
			connection = getConnection();
			IPaymentTemplateDAO paymentTemplateDAO = ManagerUtil
					.getDaoFactory().getPaymentTemplateDAO(connection);
			List<PaymentTemplate> paymentTemplates = paymentTemplateDAO
					.findAll(locale);
			return paymentTemplates;
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ManagerException.ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	/**
	 * Returns a  {@link PaymentTemplate} object, which contain information
	 * that allow client make related payment.
	 * 
	 * @param paymentTemplateId
	 *            id of related payment template
	 * @param locale
	 *            locale to specify language of strings in PaymentTemplate
	 *            object
	 * @return a  {@link PaymentTemplate} objects, which contain information
	 *         that allow client make payment
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database
	 */
	public PaymentTemplate getPaymentTemplate(int paymentTemplateId,
			Locale locale) throws ManagerException {
		Connection connection = null;
		try {
			connection = getConnection();
			IPaymentTemplateDAO paymentTemplateDAO = ManagerUtil
					.getDaoFactory().getPaymentTemplateDAO(connection);
			PaymentTemplate paymentTemplate = paymentTemplateDAO.findById(
					paymentTemplateId, locale);
			return paymentTemplate;
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	/**
	 * Make payment in payment system.
	 * 
	 * @param creditCardId
	 *            id of credit card, using which client make the payment
	 * @param paymentTemplateId
	 *            id of related payment template
	 * @param payment
	 *  {@link PaymentInfo} object with sum and payment data.
	 * @return a  {@link PaymentResult} object represents result of make
	 *         payment operation:  {@link PaymentResult#SUCCESS}, 
	 *         {@link PaymentResult#NOT_ENOUGH_MONEY} or 
	 *         {@link PaymentResult#ACCOUNT_BLOCKED}
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database or payment is null
	 */
	public PaymentResult makePayment(int creditCardId, int paymentTemplateId,
			PaymentInfo payment) throws ManagerException {
		if (payment == null) {
			throw new ManagerException("Argument payment is null");
		}
		Connection connection = null;
		try {
			connection = getConnection();
			IAccountInfoDAO accountInfoDAO = ManagerUtil.getDaoFactory()
					.getAccountInfoDAO(connection);
			AccountInfo accountInfo = accountInfoDAO
					.findByCreditCardId(creditCardId);
			if (!accountInfo.isBlocked()) {
				if (accountInfo.getBalance().compareTo(payment.getSum()) >= 0) {
					connection.setAutoCommit(false);
					accountInfoDAO.decreaseBalance(payment.getSum(),
							accountInfo.getId());
					Date sqlDate = new Date(calendar.getTime().getTime());
					payment.setPaymentTime(sqlDate);
					IPaymentInfoDAO paymentDAO = ManagerUtil.getDaoFactory()
							.getPaymentDAO(connection);
					paymentDAO.add(creditCardId, paymentTemplateId, payment);
					connection.commit();
					return PaymentResult.SUCCESS;
				} else {
					return PaymentResult.NOT_ENOUGH_MONEY;
				}
			} else {
				return PaymentResult.ACCOUNT_BLOCKED;
			}
		} catch (DAOException e) {
			ManagerUtil.rollback(connection);
			throw new ManagerException(ERROR_DB, e);
		} catch (SQLException e1) {
			ManagerUtil.rollback(connection);
			throw new ManagerException(ERROR_WHILE_TRANSACTION, e1);
		} catch (ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.setAutoCommit(connection);
			ManagerUtil.closeConnection(connection);
		}
	}

	/**
	 * Returns list of client payments.
	 * 
	 * @param clientId
	 *            id of related client
	 * @param locale
	 *            locale to specify language of strings in PaymentTemplate
	 *            object
	 * @return list with  {@link Payment} objects, which represents payments
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database
	 */
	public List<Payment> getPayments(int clientId, Locale locale)
			throws ManagerException {
		Connection connection = null;
		try {
			connection = getConnection();
			IPaymentInfoDAO paymentDAO = ManagerUtil.getDaoFactory()
					.getPaymentDAO(connection);
			List<PaymentInfo> list = paymentDAO.findByClientId(clientId);
			return createPayments(list, connection, locale);
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	private List<Payment> createPayments(List<PaymentInfo> list,
			Connection connection, Locale locale) throws DAOException {
		ICreditCardDAO creditCardDAO = ManagerUtil.getDaoFactory()
				.getCreditCardDAO(connection);
		IPaymentTemplateDAO paymentTemplateDAO = ManagerUtil.getDaoFactory()
				.getPaymentTemplateDAO(connection);
		List<Payment> result = new ArrayList<Payment>(list.size());
		for (PaymentInfo paymentInfo : list) {
			Payment payment = new Payment();
			payment.setPaymentInfo(paymentInfo);
			CreditCard creditCard = creditCardDAO.findByPaymentId(paymentInfo
					.getId());
			payment.setCreditCard(creditCard);
			PaymentTemplate paymentTemplate = paymentTemplateDAO
					.findByPaymentId(paymentInfo.getId(), locale);
			payment.setPaymentTemplate(paymentTemplate);
			result.add(payment);
		}
		return result;
	}

	/**
	 * Returns portion of list of  {@link Payment} objects(information about
	 * payments) of related client, that exist in payment system.
	 * 
	 * @param from
	 *            from(inclusive) index of portion
	 * @param count count of elements
	 * @param clientId
	 *            id of related client
	 * @param locale
	 *            locale to specify language of strings in PaymentTemplate
	 *            object
	 * @return  {@link GetPartResult} object, that include specified range
	 *         within this list and total number of payments, that related
	 *         clients made
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database
	 */
	public GetPartResult<Payment> getPayments(int from, int count, int clientId,
			Locale locale) throws ManagerException {
		Connection connection = null;
		try {
			connection = getConnection();
			IPaymentInfoDAO paymentDAO = ManagerUtil.getDaoFactory()
					.getPaymentDAO(connection);
			List<PaymentInfo> list = paymentDAO.findByClientId(from, count,
					clientId);
			List<Payment> result = createPayments(list, connection, locale);
			int totalPayments = paymentDAO.totalPaymentsByClientId(clientId);
			GetPartResult<Payment> state = new GetPartResult<Payment>(result,
					totalPayments);
			return state;
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	/**
	 * Returns payment by its id and specified locale
	 * 
	 * @param paymentId
	 * @param locale
	 *            locale to specify language of strings in PaymentTemplate
	 *            object
	 * @return  {@link Payment} object, represents payment
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database
	 */
	public Payment getPayment(int paymentId, Locale locale)
			throws ManagerException {
		Connection connection = null;
		try {
			connection = getConnection();
			IPaymentInfoDAO paymentDAO = ManagerUtil.getDaoFactory()
					.getPaymentDAO(connection);
			ICreditCardDAO creditCardDAO = ManagerUtil.getDaoFactory()
					.getCreditCardDAO(connection);
			IPaymentTemplateDAO paymentTemplateDAO = ManagerUtil
					.getDaoFactory().getPaymentTemplateDAO(connection);
			PaymentInfo paymentInfo = paymentDAO.findById(paymentId);
			Payment payment = new Payment();
			payment.setPaymentInfo(paymentInfo);
			CreditCard creditCard = creditCardDAO.findByPaymentId(paymentInfo
					.getId());
			payment.setCreditCard(creditCard);
			PaymentTemplate paymentTemplate = paymentTemplateDAO
					.findByPaymentId(paymentInfo.getId(), locale);
			payment.setPaymentTemplate(paymentTemplate);
			return payment;
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	/**
	 * Cancel payment in payment system.
	 * 
	 * @param paymentId
	 *            id of payment, that should be canceled
	 * @return  {@link PaymentCancelResult} object, represents result of
	 *         cancel payment operation: 
	 *         {@link PaymentCancelResult#SUCCESS} or 
	 *         {@link PaymentCancelResult#ACCOUNT_BLOCKED}
	 * @throws  {@link ManagerException} if an error occurs while working
	 *         with database
	 */
	public PaymentCancelResult cancelPayment(int paymentId)
			throws ManagerException {
		Connection connection = null;
		try {
			connection = getConnection();
			IPaymentInfoDAO paymentDAO = ManagerUtil.getDaoFactory()
					.getPaymentDAO(connection);
			PaymentInfo paymentInfo = paymentDAO.findById(paymentId);
			IAccountInfoDAO accountInfoDAO = ManagerUtil.getDaoFactory()
					.getAccountInfoDAO(connection);
			AccountInfo accountInfo = accountInfoDAO.findByPaymentId(paymentId);
			if (!accountInfo.isBlocked()) {
				connection.setAutoCommit(false);
				paymentDAO.delete(paymentId);
				accountInfoDAO.increaseBalance(paymentInfo.getSum(),
						accountInfo.getId());
				connection.commit();
				return PaymentCancelResult.SUCCESS;
			} else {
				return PaymentCancelResult.ACCOUNT_BLOCKED;
			}
		} catch (DAOException e) {
			ManagerUtil.rollback(connection);
			throw new ManagerException(ERROR_DB, e);
		} catch (SQLException e1) {
			ManagerUtil.rollback(connection);
			throw new ManagerException(ERROR_WHILE_TRANSACTION, e1);
		} catch (ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.setAutoCommit(connection);
			ManagerUtil.closeConnection(connection);
		}
	}

	private Connection getConnection() throws ConnectionPoolException {
		return ManagerUtil.getConnection();
	}
}
