package by.epam.javatraining.zarenok.finalproject.manager.util;

import by.epam.javatraining.zarenok.finalproject.manager.AccountManager;
import by.epam.javatraining.zarenok.finalproject.manager.ClientManager;
import by.epam.javatraining.zarenok.finalproject.manager.PaymentManager;
import by.epam.javatraining.zarenok.finalproject.manager.UserManager;

/**
 * ManagerFactory to create manager objects.
 * @author Vasil Zaranok
 *
 */
public enum ManagerFactory {
	
	/**
	 * Singleton instance
	 */
	INSTANCE;
	
	/**
	 * Returns {@link UserManager} object.
	 * @return {@link UserManager} object
	 */
	public UserManager getUserManager() {
		return UserManager.INSTANCE;
	}
	
	/**
	 * Returns {@link ClientManager} object.
	 * @return {@link ClientManager} object
	 */
	public ClientManager getClientManager() {
		return ClientManager.INSTANCE;
	}
	
	/**
	 * Returns {@link PaymentManager} object.
	 * @return {@link PaymentManager} object
	 */
	public PaymentManager getPaymentManager() {
		return PaymentManager.INSTANCE;
	}
	
	/**
	 * Returns {@link AccountManager} object.
	 * @return {@link AccountManager} object
	 */
	public AccountManager getAccountManager() {
		return AccountManager.INSTANCE;
	}
}
