package by.epam.javatraining.zarenok.finalproject.manager.exception;

/**
 * Thrown when errors occur at manager layer.
 * @author Vasil Zaranok
 *
 */
public class ManagerException extends Exception {
	
	public final static String ERROR_CLOSING_CONNECTION = "Error while closing connection : ";
	public final static String ERROR_DB = "Error while working with database : ";

	private static final long serialVersionUID = 1L;
	
	public ManagerException() {	}

	public ManagerException(String message, Throwable cause) {
		super(message, cause);
	}

	public ManagerException(String message) {
		super(message);
	}

	public ManagerException(Throwable cause) {
		super(cause);
	}
}
