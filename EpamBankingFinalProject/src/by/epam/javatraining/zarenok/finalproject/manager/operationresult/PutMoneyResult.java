package by.epam.javatraining.zarenok.finalproject.manager.operationresult;

/**
 * PutMoneyResult represents result of put money into account operation.
 * @author Vasil Zaranok
 *
 */
public enum PutMoneyResult {
	
	/**
	 * Operation was successfully completed
	 */
	SUCCESS, 
	
	/**
	 * Operation was failed because account is blocked
	 */
	ACCOUNT_BLOCKED
}
