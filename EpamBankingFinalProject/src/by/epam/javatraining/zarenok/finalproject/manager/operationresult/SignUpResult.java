package by.epam.javatraining.zarenok.finalproject.manager.operationresult;

/**
 * SignUpResult represents result of sign up client operation. 
 * @author Vasil Zaranok
 *
 */
public enum SignUpResult {
	
	/**
	 * Client wasn't signed up because his login exists.
	 */
	LOGIN_EXIST,
	
	/**
	 * Client wasn't signed up because client with related data doesn't exist in payment system.
	 */
	CLIENT_NOT_EXIST, 
	
	/**
	 * Client was signed up.
	 */
	SUCCESS
}
