package by.epam.javatraining.zarenok.finalproject.manager.operationresult;

/** 
 * PaymentChancelResult represents result of cancel payment operation.
 * 
 * @author Vasil Zaranok
 *
 */
public enum PaymentCancelResult {
	
	/**
	 * Payment hasn't been canceled because account was blocked.
	 */
	ACCOUNT_BLOCKED,
	
	/**
	 * Payment has been canceled
	 */
	SUCCESS
}
