package by.epam.javatraining.zarenok.finalproject.manager.operationresult;

/**
 * UpdateClientDataResult represents result of update client data operation.
 * 
 * @author Vasil Zaranok
 *
 */
public enum UpdateClientDataResult {

	/**
	 * Client data was successfully updated.
	 */
	SUCCESS,

	/**
	 * Client data wasn't update because another client, which data conflicts
	 * with new data, that to save, exist.
	 */
	ANOTHER_CLIENT_EXIST
}
