package by.epam.javatraining.zarenok.finalproject.manager.util;

import static by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException.ERROR_CLOSING_CONNECTION;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epam.javatraining.zarenok.finalproject.dao.factory.DAOFactory;
import by.epam.javatraining.zarenok.finalproject.dao.factory.DAOFactoryType;
import by.epam.javatraining.zarenok.finalproject.db.util.ConnectionPoolException;
import by.epam.javatraining.zarenok.finalproject.db.util.ConnectionPoolManager;

/**
 * ManagerUtil class for manager layer.
 * @author Vasil Zaranok
 *
 */
public class ManagerUtil {
	private static Logger logger = LogManager.getLogger(ManagerUtil.class);
	
	private ManagerUtil() {
	}

	/**
	 * Gets connection for managers.
	 * @return connection for managers
	 * @throws ConnectionPoolException
	 */
	public static Connection getConnection() throws ConnectionPoolException {
		return ConnectionPoolManager.getPool().getConnection();
	}

	/**
	 * Return concrete DAO factory to create DAO objects.
	 * @return concrete DAO factory
	 */
	public static DAOFactory getDaoFactory() {
		return DAOFactoryType.MY_SQL.getDaoFactory();
	}
	
	/**
	 * Safe close connection.
	 * @param connection to close
	 */
	public static void closeConnection(Connection connection) {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException e) {
				logger.error(ERROR_CLOSING_CONNECTION, e);
			}
		}
	}
	
	/**
	 * Rollback connection.
	 * @param connection to rollback
	 */
	public static void rollback(Connection connection) {
		if (connection != null) {
			try {
				if (!connection.getAutoCommit()) {
					connection.rollback();
				}
			} catch (SQLException e) {
				logger.error("Error while rollback transaction : ", e);
			}
		}
	}

	/**
	 * Sets autocommit for connection.
	 * @param connection for set
	 */
	public static void setAutoCommit(Connection connection) {
		if (connection != null) {
			try {
				connection.setAutoCommit(true);
			} catch (SQLException e) {
				logger.error("Error while commit transaction : ", e);
			}
		}
	}
}