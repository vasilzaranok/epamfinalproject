package by.epam.javatraining.zarenok.finalproject.manager;

import static by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException.ERROR_DB;

import java.sql.Connection;

import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IUserDAO;
import by.epam.javatraining.zarenok.finalproject.db.util.ConnectionPoolException;
import by.epam.javatraining.zarenok.finalproject.entity.User;
import by.epam.javatraining.zarenok.finalproject.manager.exception.ManagerException;
import by.epam.javatraining.zarenok.finalproject.manager.util.ManagerUtil;

/**
 * An UserManager class provides operation with users in payment system.
 * 
 * @author Vasil Zaranok
 *
 */
public enum UserManager {

	/**
	 * An instance of UserManager
	 */
	INSTANCE;

	/**
	 * Returns an {@link User} object of user with related login and
	 * password in payment system.
	 * 
	 * @param login
	 *            login of related user
	 * @param password
	 *            password of related user
	 * @return an {@link User} object of user with related login and
	 *         password in payment system
	 * @throws {@link ManagerException} if an error occurs while working
	 *         with database and if login or password is null
	 */
	public User getUser(String login, String password) throws ManagerException {
		if (login == null) {
			throw new ManagerException("Argument login is null");
		} else if (password == null) {
			throw new ManagerException("Argument password is null");
		}
		Connection connection = null;
		try {
			connection = getConnection();
			IUserDAO userDAO = ManagerUtil.getDaoFactory().getUserDAO(
					connection);
			User user = userDAO.findByLoginPassword(login, password);
			return user;
		} catch (DAOException | ConnectionPoolException e) {
			throw new ManagerException(ERROR_DB, e);
		} finally {
			ManagerUtil.closeConnection(connection);
		}
	}

	private Connection getConnection() throws ConnectionPoolException {
		return ManagerUtil.getConnection();
	}
}
