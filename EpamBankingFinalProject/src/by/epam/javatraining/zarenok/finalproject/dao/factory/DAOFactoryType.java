package by.epam.javatraining.zarenok.finalproject.dao.factory;

/**
 * Type of concrete DAO factory. See also:
 * 
 * @see {@link DAOFactoryType#MY_SQL}
 * @see {@link DAOFactory}
 * @author Vasil Zaranok
 */
public enum DAOFactoryType {

	/**
	 * DAO factory for MySQL database.
	 */
	MY_SQL {
		@Override
		public DAOFactory getDaoFactory() {
			return MySQLDAOFactory.INSTANCE;
		}
	};
	
	/**
	 * Gets DAO Factory.
	 * @return DAO FActory
	 */
	public abstract DAOFactory getDaoFactory();
}
