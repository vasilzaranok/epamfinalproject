package by.epam.javatraining.zarenok.finalproject.dao.interfaces;

import java.math.BigDecimal;
import java.util.List;

import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.entity.AccountInfo;

/**
 * IAccountInfoDAO provides access to account in database.
 * 
 * @author Vasil Zaranok
 *
 */
public interface IAccountInfoDAO {

	/**
	 * Find account in database by its id.
	 * 
	 * @param id
	 *            id of related account
	 * @return an {@link AccountInfo} object, represents account by its id(null
	 *         if there is no account with this id in database)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public AccountInfo findById(int id) throws DAOException;

	/**
	 * Find all client's accounts.
	 * 
	 * @param id
	 *            id of related client
	 * @return list of {@link AccountInfo} objects, represents accounts, empty
	 *         list if there is no accounts found in database
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public List<AccountInfo> findByClientId(int id) throws DAOException;

	/**
	 * Find account, using which payment was made.
	 * 
	 * @param paymentId
	 *            id of payment
	 * @return an {@link AccountInfo} object, represents account(null if there
	 *         is no account found in database)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public AccountInfo findByPaymentId(int paymentId) throws DAOException;

	/**
	 * Find account, that credit card linked to.
	 * 
	 * @param creditCardId
	 *            id of credit card
	 * @return an {@link AccountInfo} object, represents account(null if there
	 *         is no account found in database)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public AccountInfo findByCreditCardId(int creditCardId) throws DAOException;

	/**
	 * Add account to database, and links it to client.
	 * 
	 * @param account
	 *            an {@link AccountInfo} object with information about new
	 *            account
	 * @param clientId
	 *            id of client
	 * @return id of added account
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database or if account data conflicts with existing accounts(have
	 *         same account number)
	 */
	public int add(AccountInfo account, int clientId) throws DAOException;

	/**
	 * Check if possible to add account to database
	 * 
	 * @param accountInfo
	 *            an {@link AccountInfo} object with information about new
	 *            account
	 * @return true if account possible to add account, and false if account
	 *         data conflicts with existing accounts(have same account number)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public boolean tryAdd(AccountInfo accountInfo) throws DAOException;

	/**
	 * Set blocking status on account
	 * 
	 * @param blocked
	 *            blocking status to set
	 * @param id
	 *            id of account
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public void setBlocked(boolean blocked, int id) throws DAOException;

	/**
	 * Increases balance of account by related sum.
	 * 
	 * @param sum
	 *            sum to increase
	 * @param id
	 *            id of account
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database, and if sum is illegal(null or not positive)
	 */
	public void increaseBalance(BigDecimal sum, int id) throws DAOException;

	/**
	 * Decreases balance of account by related sum
	 * 
	 * @param sum
	 *            sum to decrease
	 * @param id
	 *            id of account
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database, and if sum is illegal(null or not positive)
	 */
	public void decreaseBalance(BigDecimal sum, int id) throws DAOException;
}
