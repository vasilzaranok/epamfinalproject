package by.epam.javatraining.zarenok.finalproject.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * Abstract class for DAO implementation
 * @author Vasil Zaranok
 *
 */
public class AbstractDAO {
	
	protected Connection connection;
	protected static final String ERROR_DESCRIPTION = "Error "
			+ "in DAO Layer while query execution!";

	private Logger logger = LogManager.getLogger(AbstractDAO.class);

	public AbstractDAO(Connection connection) {
		this.connection = connection;
	}
	
	/**
	 * Safe closes statement.
	 * @param statement to close
	 */
	protected void close(Statement statement) {
		if (statement != null) {
			try {
				statement.close();
			} catch (SQLException e) {
				logger.error("Error while closing statement : ", e);
			}
		}
	}
	
	/**
	 * Safe closes result set.
	 * @param resultSet to close
	 */
	protected void close(ResultSet resultSet) {
		if (resultSet != null) {
			try {
				resultSet.close();
			} catch (SQLException e) {
				logger.error("Error while result set : ", e);
			}
		}
	}
}