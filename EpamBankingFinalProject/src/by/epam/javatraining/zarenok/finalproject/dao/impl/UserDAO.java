package by.epam.javatraining.zarenok.finalproject.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IUserDAO;
import by.epam.javatraining.zarenok.finalproject.entity.Role;
import by.epam.javatraining.zarenok.finalproject.entity.User;

/**
 * MySQL implementation of {@link IUserDAO}.
 * 
 * @author Vasil Zaranok
 *
 */
public class UserDAO extends AbstractDAO implements IUserDAO {
	private static final String ROLE_TYPE = "roleType";
	private static final String PASSWORD = "password";
	private static final String LOGIN = "login";
	private static final String ID_USER = "idUser";
	private static final String SQL_FIND_BY_ID = "SELECT"
			+ " user.idUser, user.login, user.password," + " role.roleType "
			+ "FROM user JOIN role "
			+ "ON user.Role_idRole=role.idRole WHERE idUser=?";
	private static final String SQL_FIND_BY_LOGIN = "SELECT"
			+ " user.idUser, user.login, user.password," + " role.roleType "
			+ "FROM user JOIN role "
			+ "ON user.Role_idRole=role.idRole WHERE login=?";
	private static final String SQL_FIND_ROLE_ID = "SELECT idRole"
			+ " FROM role where upper(roleType)=?";
	private static final String SQL_FIND = "SELECT"
			+ " user.idUser, user.login, user.password," + " role.roleType "
			+ "FROM user JOIN role "
			+ "ON user.Role_idRole=role.idRole WHERE login=?"
			+ " AND password=?";
	private static final String SQL_INSERT = "INSERT INTO user"
			+ "(login, password, Role_idRole) VALUE(?,?,?)";

	public UserDAO(Connection connection) {
		super(connection);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public User findById(int id) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_BY_ID);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt(ID_USER));
				user.setLogin(resultSet.getString(LOGIN));
				user.setPassword(resultSet.getString(PASSWORD));
				user.setRole(Role.valueOf(resultSet.getString(ROLE_TYPE)
						.toUpperCase()));
				return user;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public User findByLoginPassword(String login, String password)
			throws DAOException {
		if (login == null) {
			throw new DAOException("Argument login is null");
		} else if (password == null) {
			throw new DAOException("Argument password is null");
		}
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_BY_LOGIN);
			statement.setString(1, login);
			resultSet = statement.executeQuery();
			if (!resultSet.next()) {
				return null;
			}
			String userPassword = resultSet.getString(PASSWORD);
			if (!userPassword.equals(password)) {
				return null;
			}
			User user = new User();
			user.setId(resultSet.getInt(ID_USER));
			user.setLogin(resultSet.getString(LOGIN));
			user.setPassword(userPassword);
			user.setRole(Role.valueOf(resultSet.getString(ROLE_TYPE)
					.toUpperCase()));
			return user;
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public User findByLogin(String login) throws DAOException {
		if (login == null) {
			throw new DAOException("Argument login is null");
		}
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_BY_LOGIN);
			statement.setString(1, login);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				User user = new User();
				user.setId(resultSet.getInt(ID_USER));
				user.setLogin(resultSet.getString(LOGIN));
				String userPassword = resultSet.getString(PASSWORD);
				user.setPassword(userPassword);
				user.setRole(Role.valueOf(resultSet.getString(ROLE_TYPE)
						.toUpperCase()));
				return user;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int add(User user) throws DAOException {
		if (user == null) {
			throw new DAOException("Argument user is null");
		}
		PreparedStatement statement = null;
		try {
			int roleId = getRoleId(user);
			statement = connection.prepareStatement(SQL_INSERT);
			statement.setString(1, user.getLogin());
			statement.setString(2, user.getPassword());
			statement.setInt(3, roleId);
			statement.executeUpdate();
			return select(user);
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
		}
	}

	/**
	 * Selects user id by its data.
	 * 
	 * @param user
	 *            with data
	 * @return id of user
	 * @throws SQLException
	 * @throws DAOException
	 */
	private int select(User user) throws SQLException, DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND);
			statement.setString(1, user.getLogin());
			statement.setString(2, user.getPassword());
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				return resultSet.getInt(ID_USER);
			} else {
				throw new DAOException("User doesn't exist : " + user);
			}
		} finally {
			close(statement);
			close(resultSet);
		}
	}

	/**
	 * Selects role id by user.
	 * 
	 * @param user
	 * @return id of user role
	 * @throws SQLException
	 * @throws DAOException
	 */
	private int getRoleId(User user) throws SQLException, DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_ROLE_ID);
			statement.setString(1, user.getRole().toString());
			resultSet = statement.executeQuery();
			int roleId;
			if (resultSet.next()) {
				roleId = resultSet.getInt("idRole");
				return roleId;
			} else {
				throw new DAOException("Error in DAO layer, role:"
						+ user.getRole().toString() + " didn't find");
			}
		} finally {
			close(statement);
			close(resultSet);
		}
	}
}