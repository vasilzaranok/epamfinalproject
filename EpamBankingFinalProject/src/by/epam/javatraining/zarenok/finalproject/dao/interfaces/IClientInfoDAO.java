package by.epam.javatraining.zarenok.finalproject.dao.interfaces;

import java.util.List;

import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.entity.ClientInfo;

/**
 * IClientInfoDAO provides access to client data in database.
 * 
 * @author Vasil Zaranok
 *
 */
public interface IClientInfoDAO {

	/**
	 * Finds client by his id.
	 * 
	 * @param id
	 *            id of related client
	 * @return a {@link ClientInfo} object, represents client data(null if there
	 *         is no client found in database)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public ClientInfo findById(int id) throws DAOException;

	/**
	 * Finds client by user of payment system.
	 * 
	 * @param id
	 *            id of related user
	 * @return a {@link ClientInfo} object, represents client data(null if there
	 *         is no client found in database)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public ClientInfo findByUserId(int id) throws DAOException;

	/**
	 * Finds client by client data(full name and passport number).
	 * 
	 * @param clientInfo
	 *            {@link ClientInfo} object contains full name and passport
	 *            number
	 * @return a {@link ClientInfo} object, represents client data, includes his
	 *         id(null if there is no client found in database)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database or if clientInfo is illegal(it is null)
	 */
	public ClientInfo findByData(ClientInfo clientInfo) throws DAOException;

	/**
	 * Finds all clients in database.
	 * 
	 * @return list of {@link ClientInfo} objects, represents clients(empty list
	 *         if there is no clients found in database)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public List<ClientInfo> findAll() throws DAOException;

	/**
	 * Update data of this client.
	 * 
	 * @param client
	 *            {@link ClientInfo} object with id of client to update, and
	 *            data that replace old in database
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database or if client is null
	 */
	public void update(ClientInfo client) throws DAOException;

	/**
	 * Checks if new data of client to update doesn't conflict with any other
	 * client data.
	 * 
	 * @param client
	 *            {@link ClientInfo} object with id of client to update, and
	 *            data that replace old in database
	 * @return true if possible to update client, false if new data conflicts
	 *         with other client
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database or if client is null
	 */
	public boolean tryUpdate(ClientInfo client) throws DAOException;

	/**
	 * Add client to database.
	 * 
	 * @param client
	 *            client to add
	 * @return id of added client
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database or if client is null
	 */
	public int add(ClientInfo client) throws DAOException;

	/**
	 * Checks if data of client to add doesn't conflict with any other client
	 * data
	 * 
	 * @param clientInfo
	 *            client to check
	 * @return true if possible to add client and false if his data conflicts
	 *         with other client
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database or if clientInfo is null
	 */
	public boolean tryAdd(ClientInfo clientInfo) throws DAOException;

	/**
	 * Links user to client in database.
	 * @param clientId id of related client
	 * @param userId id of related user
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public void setUserById(int clientId, int userId) throws DAOException;

	/**
	 * Finds part of clients in database that exist in payment system.
	 * 
	 * @param from
	 *            from(inclusive) index of portion
	 * @param count count of elements
	 * @return list of this part(empty list if no clients found)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database or if from index is negative
	 */
	public List<ClientInfo> findAll(int from, int count) throws DAOException;

	/**
	 * Returns number of clients in database.
	 * @return number of clients in database
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public int totalCount() throws DAOException;
}
