package by.epam.javatraining.zarenok.finalproject.dao.impl;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IAccountInfoDAO;
import by.epam.javatraining.zarenok.finalproject.entity.AccountInfo;

/**
 * MySQL implementation of {@link IAccountInfoDAO}
 * 
 * @author Vasil Zaranok
 *
 */
public class AccountInfoDAO extends AbstractDAO implements IAccountInfoDAO {

	private static final String BALANCE = "balance";
	private static final String BLOCKED = "blocked";
	private static final String NUMBER = "number";
	private static final String ID_ACCOUNT = "idAccount";
	private static final String SQL_FIND_BY_ID = "SELECT * FROM "
			+ "account WHERE idAccount=?";
	private static final String SQL_FIND_BY_CLIENT_ID = "SELECT * FROM"
			+ " account WHERE Clients_idClient=?";
	private static final String SQL_UPDATE_BLOCKED = "UPDATE account"
			+ " SET blocked=? WHERE idAccount=?";
	private static final String SQL_INCREASE_BALANCE = "UPDATE account "
			+ "SET balance=balance+? WHERE idAccount=?";
	private static final String SQL_DECREASE_BALANCE = "UPDATE account"
			+ " SET balance=balance-? WHERE idAccount=?";
	private static final String SQL_FIND_BY_PAYMENT_ID = "SELECT * FROM "
			+ "account" + " WHERE idAccount=(SELECT Account_idAccount FROM"
			+ " creditcard WHERE idCreditCard="
			+ "(SELECT CreditCard_idCreditCard FROM "
			+ "payment WHERE idPayment=?))";
	private static final String SQL_FIND_BY_CREDIT_CARD_ID = "SELECT * FROM"
			+ " account" + " WHERE idAccount=(SELECT Account_idAccount "
			+ "from creditcard" + " WHERE idCreditCard=?)";
	private static final String SQL_SELECT_ID = "select idAccount from"
			+ " account WHERE number=?";
	private static final String SQL_INSERT = "INSERT INTO "
			+ "account(Clients_idClient, number,"
			+ "blocked,balance) VALUE(?,?,?,?)";

	public AccountInfoDAO(Connection connection) {
		super(connection);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AccountInfo findById(int id) throws DAOException {
		return findOne(SQL_FIND_BY_ID, id);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AccountInfo findByPaymentId(int paymentId) throws DAOException {
		return findOne(SQL_FIND_BY_PAYMENT_ID, paymentId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public AccountInfo findByCreditCardId(int creditCardId) throws DAOException {
		return findOne(SQL_FIND_BY_CREDIT_CARD_ID, creditCardId);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<AccountInfo> findByClientId(int id) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			List<AccountInfo> list = new ArrayList<AccountInfo>();
			statement = connection.prepareStatement(SQL_FIND_BY_CLIENT_ID);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			while (resultSet.next()) {
				AccountInfo accountInfo = getAccountInfo(resultSet);
				list.add(accountInfo);
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setBlocked(boolean blocked, int id) throws DAOException {
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_UPDATE_BLOCKED);
			statement.setBoolean(1, blocked);
			statement.setInt(2, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void increaseBalance(BigDecimal sum, int id) throws DAOException {
		if (sum == null) {
			throw new DAOException("Argument sum is null");
		} else if (sum.compareTo(BigDecimal.ZERO) <= 0) {
			throw new DAOException("Illegal argument sum : " + sum
					+ ", sum must be positive");
		}
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_INCREASE_BALANCE);
			statement.setBigDecimal(1, sum);
			statement.setInt(2, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void decreaseBalance(BigDecimal sum, int id) throws DAOException {
		if (sum == null) {
			throw new DAOException("Argument sum is null");
		} else if (sum.compareTo(BigDecimal.ZERO) <= 0) {
			throw new DAOException("Illegal argument sum : " + sum
					+ ", sum must be positive");
		}
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_DECREASE_BALANCE);
			statement.setBigDecimal(1, sum);
			statement.setInt(2, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int add(AccountInfo account, int clientId) throws DAOException {
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_INSERT);
			statement.setInt(1, clientId);
			statement.setString(2, account.getNumber());
			statement.setBoolean(3, account.isBlocked());
			statement.setBigDecimal(4, account.getBalance());
			statement.executeUpdate();
			statement.close();
			return selectId(account);
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean tryAdd(AccountInfo accountInfo) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_SELECT_ID);
			statement.setString(1, accountInfo.getNumber());
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				return false;
			} else {
				return true;
			}
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}

	/**
	 * Finds one AccountInfo object by SQL string
	 * 
	 * @param sql
	 *            string to find
	 * @param id
	 *            sql string parameter
	 * @return one AccountInfo object
	 * @throws DAOException
	 *             if error occurs while query execution
	 */
	private AccountInfo findOne(String sql, int id) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(sql);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			AccountInfo accountInfo = null;
			if (resultSet.next()) {
				accountInfo = new AccountInfo();
				accountInfo.setId(resultSet.getInt(ID_ACCOUNT));
				accountInfo.setNumber(resultSet.getString(NUMBER));
				accountInfo.setBlocked(resultSet.getBoolean(BLOCKED));
				accountInfo.setBalance(resultSet.getBigDecimal(BALANCE));
			}
			return accountInfo;
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}

	/**
	 * Select account id by account
	 * 
	 * @param account
	 *            to find id
	 * @return id of account
	 * @throws SQLException
	 */
	private int selectId(AccountInfo account) throws SQLException {
		PreparedStatement statement = connection
				.prepareStatement(SQL_SELECT_ID);
		statement.setString(1, account.getNumber());
		ResultSet resultSet = statement.executeQuery();
		resultSet.next();
		int id = resultSet.getInt(ID_ACCOUNT);
		resultSet.close();
		statement.close();
		return id;
	}

	/**
	 * Builds AccountInfo from result set
	 * 
	 * @param resultSet
	 * @return AccountInfo object
	 * @throws SQLException
	 */
	private AccountInfo getAccountInfo(ResultSet resultSet) throws SQLException {
		AccountInfo accountInfo = new AccountInfo();
		accountInfo.setId(resultSet.getInt(ID_ACCOUNT));
		accountInfo.setNumber(resultSet.getString(NUMBER));
		accountInfo.setBlocked(resultSet.getBoolean(BLOCKED));
		accountInfo.setBalance(resultSet.getBigDecimal(BALANCE));
		return accountInfo;
	}
}
