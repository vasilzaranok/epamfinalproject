package by.epam.javatraining.zarenok.finalproject.dao.interfaces;

import java.util.List;

import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.entity.CreditCard;

/**
 * ICreditCardDAO provides access to credit cards in database.
 * 
 * @author Vasil Zaranok
 *
 */
public interface ICreditCardDAO {

	/**
	 * Finds credit card by its id.
	 * 
	 * @param id
	 *            of related credit card
	 * @return {@link CreditCard} object, represents credit card(null if there
	 *         is no card found)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public CreditCard findById(int id) throws DAOException;

	/**
	 * Finds credit card by payment was made.
	 * 
	 * @param id
	 *            of related payment
	 * @return {@link CreditCard} object, represents credit card(null if there
	 *         is no card found)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public CreditCard findByPaymentId(int id) throws DAOException;

	/**
	 * Finds credit card by account id.
	 * 
	 * @param id
	 *            of related account
	 * @return {@link CreditCard} object, represents credit card(null if there
	 *         is no card found)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public List<CreditCard> findByAccountId(int id) throws DAOException;

	/**
	 * Finds credit cards by client id.
	 * 
	 * @param id
	 *            of related credit card
	 * @return list of {@link CreditCard} objects, represents credit card(empty
	 *         list if there is no cards found)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public List<CreditCard> findByClientId(int id) throws DAOException;

	/**
	 * Add credit card to account in database
	 * 
	 * @param card
	 *            credit card to add
	 * @param accountId
	 *            id of account
	 * @return id of added card
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database or card is null
	 */
	public int add(CreditCard card, int accountId) throws DAOException;

	/**
	 * Checks if card data to add doesn't conflict with any other credit card
	 * 
	 * @param card
	 *            credit card to check
	 * @return true if possible to add card, false if card data conflicts with
	 *         other card
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database or card is null
	 */
	public boolean tryAdd(CreditCard card) throws DAOException;
}
