package by.epam.javatraining.zarenok.finalproject.dao.factory;

import java.sql.Connection;

import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IAccountInfoDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IClientInfoDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.ICreditCardDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IPaymentInfoDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IPaymentTemplateDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IUserDAO;

/**
 * Abstract factory to create DAO objects.
 * @author Vasil Zaranok
 *
 */
public interface DAOFactory {
	
	/**
	 * Returns {@link IAccountInfoDAO} object.
	 * @param connection to create DAO object
	 * @return {@link IAccountInfoDAO} object
	 */
	public IAccountInfoDAO getAccountInfoDAO(Connection connection);
	
	/**
	 * Returns {@link IClientInfoDAO} object.
	 * @param connection to create DAO object
	 * @return {@link IClientInfoDAO} object
	 */
	public IClientInfoDAO getClientInfoDAO(Connection connection);
	
	/**
	 * Returns {@link IAccountInfoDAO} object.
	 * @param connection to create DAO object
	 * @return {@link IAccountInfoDAO} object
	 */
	public ICreditCardDAO getCreditCardDAO(Connection connection);
	
	/**
	 * Returns {@link IPaymentInfoDAO} object.
	 * @param connection to create DAO object
	 * @return {@link IPaymentInfoDAO} object
	 */
	public IPaymentInfoDAO getPaymentDAO(Connection connection);
	
	/**
	 * Returns {@link IPaymentTemplateDAO} object.
	 * @param connection to create DAO object
	 * @return {@link IPaymentTemplateDAO} object
	 */
	public IPaymentTemplateDAO getPaymentTemplateDAO(Connection connection);
	
	/**
	 * Returns {@link IUserDAO} object.
	 * @param connection to create DAO object
	 * @return {@link IUserDAO} object
	 */
	public IUserDAO getUserDAO(Connection connection);
}
