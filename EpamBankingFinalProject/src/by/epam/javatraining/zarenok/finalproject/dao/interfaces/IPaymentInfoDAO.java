package by.epam.javatraining.zarenok.finalproject.dao.interfaces;

import java.util.List;

import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.entity.PaymentInfo;

/**
 * IPaymentInfoDAO provides access to payment in database.
 * 
 * @author Vasil Zaranok
 *
 */
public interface IPaymentInfoDAO {

	/**
	 * Finds payment by its id
	 * 
	 * @param id
	 *            id of related payment
	 * @return {@link PaymentInfo} object, represents payment(is null if there
	 *         no payment found)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public PaymentInfo findById(int id) throws DAOException;

	/**
	 * Finds all payment that client made.
	 * 
	 * @param id
	 *            id of related client
	 * @return list of {@link PaymentInfo} objects, represents payments(empty
	 *         list if there no payments found)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public List<PaymentInfo> findByClientId(int clientId) throws DAOException;

	/**
	 * Finds part of all client's payments, that exist in payment system.
	 * 
	 * @param from
	 *            from(inclusive) index of portion
	 * @param count count of elements
	 * @param clientId
	 * @return list of this part
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database or from is negative
	 */
	public List<PaymentInfo> findByClientId(int from, int count, int clientId)
			throws DAOException;

	/**
	 * Returns number of all payments, that client made
	 * @param clientId id of related client
	 * @return
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public int totalPaymentsByClientId(int clientId) throws DAOException;

	/**
	 * Delete payment from database
	 * @param id id of related payment
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public void delete(int id) throws DAOException;

	/**
	 * Add payment to database.
	 * @param creditCardId id of credit card, using which client make payment
	 * @param paymentTemplateId id of payment template
	 * @param payment payment contains sum and other data to make payment
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database or if payment is null
	 */
	public void add(int creditCardId, int paymentTemplateId, PaymentInfo payment)
			throws DAOException;
}
