package by.epam.javatraining.zarenok.finalproject.dao.factory;

import java.sql.Connection;

import by.epam.javatraining.zarenok.finalproject.dao.impl.AccountInfoDAO;
import by.epam.javatraining.zarenok.finalproject.dao.impl.ClientInfoDAO;
import by.epam.javatraining.zarenok.finalproject.dao.impl.CreditCardDAO;
import by.epam.javatraining.zarenok.finalproject.dao.impl.PaymentInfoDAO;
import by.epam.javatraining.zarenok.finalproject.dao.impl.PaymentTemplateDAO;
import by.epam.javatraining.zarenok.finalproject.dao.impl.UserDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IAccountInfoDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IClientInfoDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.ICreditCardDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IPaymentInfoDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IPaymentTemplateDAO;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IUserDAO;

/**
 * MySQL DAO factory.
 * @author Vasil Zaranok
 *
 */
public enum MySQLDAOFactory implements DAOFactory {
	
	/**
	 * Singleton instance.
	 */
	INSTANCE;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public IAccountInfoDAO getAccountInfoDAO(Connection connection) {
		return new AccountInfoDAO(connection);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public IClientInfoDAO getClientInfoDAO(Connection connection) {
		return new ClientInfoDAO(connection);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ICreditCardDAO getCreditCardDAO(Connection connection) {
		return new CreditCardDAO(connection);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public IPaymentInfoDAO getPaymentDAO(Connection connection) {
		return new PaymentInfoDAO(connection);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public IPaymentTemplateDAO getPaymentTemplateDAO(Connection connection) {
		return new PaymentTemplateDAO(connection);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public IUserDAO getUserDAO(Connection connection) {
		return new UserDAO(connection);
	}
}
