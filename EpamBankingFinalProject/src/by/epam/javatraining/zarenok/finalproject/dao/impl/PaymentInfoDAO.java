package by.epam.javatraining.zarenok.finalproject.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IPaymentInfoDAO;
import by.epam.javatraining.zarenok.finalproject.entity.PaymentInfo;

/**
 * MySQL implementation of {@link PaymentInfoDAO}.
 * 
 * @author Vasil Zaranok
 *
 */
public class PaymentInfoDAO extends AbstractDAO implements IPaymentInfoDAO {

	private static final String PAYMENT_DATA = "paymentData";
	private static final String DATE = "date";
	private static final String SUM = "sum";
	private static final String ID_PAYMENT = "idPayment";
	private static final String COUNT = "count";
	private static final String SQL_INSERT = "INSERT INTO "
			+ "payment(CreditCard_idCreditCard, sum, date,"
			+ " PaymentTemplate_idPaymentTemplate, paymentData) "
			+ "VALUE(?,?,?,?,?)";
	private static final String SQL_FIND_BY_CLIENT_ID = "SELECT"
			+ " idPayment, sum,"
			+ " date, paymentData, creditcard.Account_Client_idClient"
			+ " FROM payment JOIN creditcard ON "
			+ "payment.CreditCard_idCreditCard=creditcard.idCreditCard"
			+ " WHERE creditcard.Account_Client_idClient=?";
	private static final String SQL_FIND_BY_ID = "SELECT * FROM payment"
			+ " WHERE idPayment=?";
	private static final String SQL_DELETE = "DELETE FROM payment"
			+ " WHERE idPayment=?";
	private static final String SQL_FIND_BY_CLIENT_ID_LIMIT = SQL_FIND_BY_CLIENT_ID
			+ " ORDER BY date DESC LIMIT ?,?";
	private static final String SQL_COUNT_BY_CLIENT_ID = "SELECT"
			+ " COUNT(*) as count" + " FROM payment JOIN creditcard ON"
			+ " payment.CreditCard_idCreditCard=creditcard.idCreditCard"
			+ " WHERE creditcard.Account_Client_idClient=?";

	public PaymentInfoDAO(Connection connection) {
		super(connection);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PaymentInfo findById(int id) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_BY_ID);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				PaymentInfo payment = getPayment(resultSet);
				return payment;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PaymentInfo> findByClientId(int clientId) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_BY_CLIENT_ID);
			statement.setInt(1, clientId);
			resultSet = statement.executeQuery();
			List<PaymentInfo> list = new ArrayList<PaymentInfo>();
			while (resultSet.next()) {
				PaymentInfo payment = getPayment(resultSet);
				list.add(payment);
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void delete(int id) throws DAOException {
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_DELETE);
			statement.setInt(1, id);
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void add(int creditCardId, int paymentTemplateId, PaymentInfo payment)
			throws DAOException {
		if (payment == null) {
			throw new DAOException("Argument payment is null");
		}
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_INSERT);
			statement.setInt(1, creditCardId);
			statement.setBigDecimal(2, payment.getSum());
			statement.setDate(3, payment.getPaymentTime());
			statement.setInt(4, paymentTemplateId);
			statement.setString(5, payment.getPaymentData());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
		}
	}

	/**
	 * Builds PaymentInfo from result set.
	 * 
	 * @param resultSet
	 * @return PaymentInfo object
	 * @throws SQLException
	 */
	private PaymentInfo getPayment(ResultSet resultSet) throws SQLException {
		PaymentInfo payment = new PaymentInfo();
		payment.setId(resultSet.getInt(ID_PAYMENT));
		payment.setSum(resultSet.getBigDecimal(SUM));
		payment.setPaymentTime(resultSet.getDate(DATE));
		payment.setPaymentData(resultSet.getString(PAYMENT_DATA));
		return payment;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PaymentInfo> findByClientId(int from, int count, int clientId)
			throws DAOException {
		if (from < 0) {
			throw new DAOException("Illegal argument from : " + from
					+ ", from shouldn't be negative");
		}
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection
					.prepareStatement(SQL_FIND_BY_CLIENT_ID_LIMIT);
			statement.setInt(1, clientId);
			statement.setInt(2, from);
			statement.setInt(3, count);
			resultSet = statement.executeQuery();
			List<PaymentInfo> list = new ArrayList<PaymentInfo>();
			while (resultSet.next()) {
				PaymentInfo payment = getPayment(resultSet);
				list.add(payment);
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int totalPaymentsByClientId(int clientId) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_COUNT_BY_CLIENT_ID);
			statement.setInt(1, clientId);
			resultSet = statement.executeQuery();
			resultSet.next();
			return resultSet.getInt(COUNT);
		} catch (SQLException e) {
			close(statement);
			close(resultSet);
			throw new DAOException(ERROR_DESCRIPTION, e);
		}
	}
}