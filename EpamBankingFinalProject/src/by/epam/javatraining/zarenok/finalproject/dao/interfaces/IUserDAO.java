package by.epam.javatraining.zarenok.finalproject.dao.interfaces;

import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.entity.User;

/**
 * IUserDAO provides access to users in database.
 * 
 * @author Vasil Zaranok
 *
 */
public interface IUserDAO {

	/**
	 * Finds user by its id.
	 * 
	 * @param id
	 *            of related user
	 * @return {@link User} object, represents payment system user(null if there
	 *         no user found)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database
	 */
	public User findById(int id) throws DAOException;

	/**
	 * Add user to database.
	 * @param user user to add
	 * @return id of added user
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database or if user is null
	 */
	public int add(User user) throws DAOException;

	/**
	 * Finds user by login and password.
	 * 
	 * @param login related login
	 * @param password related password
	 * @return {@link User} object, represents payment system user(null if there
	 *         no user found)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database or if login is null or if password is null
	 */
	public User findByLoginPassword(String login, String password)
			throws DAOException;

	/**
	 * Finds user by login.
	 * @param login related login
	 * @return {@link User} object, represents payment system user(null if there
	 *         no user found)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database or if login is null
	 */
	public User findByLogin(String login) throws DAOException;
}
