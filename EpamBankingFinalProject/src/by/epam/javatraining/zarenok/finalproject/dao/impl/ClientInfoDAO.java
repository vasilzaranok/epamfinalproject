package by.epam.javatraining.zarenok.finalproject.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IClientInfoDAO;
import by.epam.javatraining.zarenok.finalproject.entity.ClientInfo;

/**
 * MySQL implementation of {@link IClientInfoDAO}
 * 
 * @author Vasil Zaranok
 *
 */
public class ClientInfoDAO extends AbstractDAO implements IClientInfoDAO {

	private static final String COUNT = "count";
	private static final String PASSPORT_NUMBER = "passportNumber";
	private static final String MIDDLE_NAME = "middleName";
	private static final String SURNAME = "surname";
	private static final String FIRSTNAME = "firstName";
	private static final String ID_CLIENT = "idClient";
	private static final String SQL_FIND_BY_USER_ID = "SELECT "
			+ "idClient, surname, firstName," + " middleName , passportNumber "
			+ "FROM client WHERE Users_idUser=?";
	private static final String SQL_FIND_ALL = "SELECT * FROM client";
	private static final String SQL_FIND_BY_ID = "SELECT * FROM client"
			+ " WHERE idClient=?";
	private static final String SQL_INSERT = "INSERT INTO client"
			+ "(surname, firstName, middleName, passportNumber)"
			+ " VALUE(?, ?, ?, ?)";
	private static final String SQL_SELECT_ID = "SELECT idClient"
			+ " from client WHERE passportNumber=?";
	private static final String SQL_UPDATE = "UPDATE client"
			+ " SET surname=?, firstName=?, middleName=?, "
			+ "passportNumber=? WHERE idClient=?";
	private static final String SQL_SET_USER = "UPDATE client"
			+ " SET Users_idUser=? WHERE idClient=?";
	private static final String SQL_FIND_BY_PASSPORT_NUMBER = "SELECT * FROM"
			+ " client WHERE passportNumber=?";
	private static final String SQL_FIND = "SELECT * FROM client"
			+ " WHERE surname=? and firstName=? AND middlename=? "
			+ "AND passportNumber=?";
	private static final String SQL_COUNT = "SELECT COUNT(*)"
			+ " as count FROM client";
	private static final String SQL_FIND_ALL_LIMIT = "SELECT * FROM"
			+ " client ORDER BY surname LIMIT ?,?";

	public ClientInfoDAO(Connection connection) {
		super(connection);
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ClientInfo findById(int id) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			ClientInfo client = new ClientInfo();
			statement = connection.prepareStatement(SQL_FIND_BY_ID);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				client = getClientInfo(resultSet);
				return client;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ClientInfo findByUserId(int id) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_BY_USER_ID);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				ClientInfo client = new ClientInfo();
				client = getClientInfo(resultSet);
				return client;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ClientInfo> findAll() throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_ALL);
			resultSet = statement.executeQuery();
			List<ClientInfo> list = new ArrayList<ClientInfo>();
			while (resultSet.next()) {
				ClientInfo clientInfo = getClientInfo(resultSet);
				list.add(clientInfo);
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void update(ClientInfo client) throws DAOException {
		if (client == null) {
			throw new DAOException("Argument client is null");
		}
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_UPDATE);
			statement.setString(1, client.getSurname());
			statement.setString(2, client.getFirstname());
			statement.setString(3, client.getMiddlename());
			statement.setString(4, client.getPassportNumber());
			statement.setInt(5, client.getId());
			statement.executeUpdate();
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void setUserById(int clientId, int userId) throws DAOException {
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_SET_USER);
			statement.setInt(1, userId);
			statement.setInt(2, clientId);
			statement.executeUpdate();
			statement.close();
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public ClientInfo findByData(ClientInfo client) throws DAOException {
		if (client == null) {
			throw new DAOException("Argument client is null");
		}
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND);
			statement.setString(1, client.getSurname());
			statement.setString(2, client.getFirstname());
			statement.setString(3, client.getMiddlename());
			statement.setString(4, client.getPassportNumber());
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				ClientInfo clientFound = getClientInfo(resultSet);
				return clientFound;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int add(ClientInfo client) throws DAOException {
		if (client == null) {
			throw new DAOException("Argument client is null");
		}
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_INSERT);
			statement.setString(1, client.getSurname());
			statement.setString(2, client.getFirstname());
			statement.setString(3, client.getMiddlename());
			statement.setString(4, client.getPassportNumber());
			statement.executeUpdate();
			return selectId(client);
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean tryAdd(ClientInfo clientInfo) throws DAOException {
		if (clientInfo == null) {
			throw new DAOException("Argument clientInfo is null");
		}
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_SELECT_ID);
			statement.setString(1, clientInfo.getPassportNumber());
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				return false;
			} else {
				return true;
			}
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean tryUpdate(ClientInfo client) throws DAOException {
		if (client == null) {
			throw new DAOException("Argument client is null");
		}
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection
					.prepareStatement(SQL_FIND_BY_PASSPORT_NUMBER);
			statement.setString(1, client.getPassportNumber());
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				if (client.getId() != resultSet.getInt(ID_CLIENT)) {
					return false;
				} else {
					return true;
				}
			} else {
				return true;
			}
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	private int selectId(ClientInfo client) throws SQLException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_SELECT_ID);
			statement.setString(1, client.getPassportNumber());
			resultSet = statement.executeQuery();
			resultSet.next();
			int id = resultSet.getInt(ID_CLIENT);
			return id;
		} finally {
			close(statement);
			close(resultSet);
		}
	}
	
	/**
	 * Builds ClientInfo from result set.
	 * @param resultSet
	 * @return ClientInfo object
	 * @throws SQLException
	 */
	private ClientInfo getClientInfo(ResultSet resultSet) throws SQLException {
		ClientInfo client = new ClientInfo();
		client.setId(resultSet.getInt(ID_CLIENT));
		client.setFirstname(resultSet.getString(FIRSTNAME));
		client.setSurname(resultSet.getString(SURNAME));
		client.setMiddlename(resultSet.getString(MIDDLE_NAME));
		client.setPassportNumber(resultSet.getString(PASSPORT_NUMBER));
		return client;
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ClientInfo> findAll(int from, int count) throws DAOException {
		if (from < 0) {
			throw new DAOException("From parameter < 0 : " + from);
		}
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_ALL_LIMIT);
			statement.setInt(1, from);
			statement.setInt(2, count);
			resultSet = statement.executeQuery();
			List<ClientInfo> list = new ArrayList<ClientInfo>();
			while (resultSet.next()) {
				ClientInfo clientInfo = getClientInfo(resultSet);
				list.add(clientInfo);
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public int totalCount() throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_COUNT);
			resultSet = statement.executeQuery();
			resultSet.next();
			return resultSet.getInt(COUNT);
		} catch (SQLException e) {
			close(statement);
			close(resultSet);
			throw new DAOException(ERROR_DESCRIPTION, e);
		}
	}
}