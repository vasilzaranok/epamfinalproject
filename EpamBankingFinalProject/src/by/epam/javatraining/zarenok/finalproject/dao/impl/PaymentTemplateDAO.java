package by.epam.javatraining.zarenok.finalproject.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.IPaymentTemplateDAO;
import by.epam.javatraining.zarenok.finalproject.entity.PaymentTemplate;

/**
 * MySQL implementation of {@link IPaymentTemplateDAO}.
 * 
 * @author Vasil Zaranok
 *
 */
public class PaymentTemplateDAO extends AbstractDAO implements
		IPaymentTemplateDAO {

	private static final String REGEXP_DESCRIPTION = "regexpDescription";
	private static final String REGEXP = "regexp";
	private static final String DATA_DESCRIPTION = "description";
	private static final String UNP = "unp";
	private static final String NAME = "name";
	private static final String CODE = "code";
	private static final String ID_PAYMENT_TEMPLATE = "idPaymentTemplate";
	private static final String SQL_FIND_ALL = "SELECT"
			+ " paymenttemplatetranslation.regexpDescription,"
			+ " paymenttemplate.idPaymentTemplate, paymenttemplate.regexp,"
			+ " paymenttemplate.unp, paymenttemplate.code,"
			+ "paymenttemplatetranslation.name,"
			+ " paymenttemplatetranslation.description"
			+ " FROM paymenttemplate JOIN paymenttemplatetranslation ON"
			+ " paymenttemplate.idPaymentTemplate="
			+ "paymenttemplatetranslation.PaymentTemplate_idPaymentTemplate "
			+ "WHERE paymenttemplatetranslation.locale=?";
	private static final String SQL_FIND_BY_ID_LOCALE = "SELECT"
			+ " paymenttemplatetranslation.regexpDescription, "
			+ "paymenttemplate.idPaymentTemplate, paymenttemplate.regexp,"
			+ " paymenttemplate.unp, paymenttemplate.code,"
			+ "paymenttemplatetranslation.name,"
			+ " paymenttemplatetranslation.description"
			+ " FROM paymenttemplate JOIN paymenttemplatetranslation ON"
			+ " paymenttemplate.idPaymentTemplate="
			+ "paymenttemplatetranslation.PaymentTemplate_idPaymentTemplate "
			+ "WHERE paymenttemplatetranslation.locale=? AND paymenttemplate."
			+ "idPaymentTemplate=?";
	private static final String SQL_FIND_BY_PAYMENT_ID_LOCALE = "SELECT"
			+ " paymenttemplatetranslation.regexpDescription,"
			+ " paymenttemplate.idPaymentTemplate, paymenttemplate.regexp,"
			+ " paymenttemplate.unp, paymenttemplate.code,"
			+ "paymenttemplatetranslation.name,"
			+ " paymenttemplatetranslation.description"
			+ " FROM paymenttemplate JOIN paymenttemplatetranslation ON"
			+ " paymenttemplate.idPaymentTemplate="
			+ "paymenttemplatetranslation.PaymentTemplate_idPaymentTemplate "
			+ "where paymenttemplatetranslation.locale=? and paymenttemplate."
			+ "idPaymentTemplate="
			+ "(SELECT PaymentTemplate_idPaymentTemplate FROM"
			+ " payment WHERE idPayment=?)";

	public PaymentTemplateDAO(Connection connection) {
		super(connection);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PaymentTemplate findById(int id, Locale locale) throws DAOException {
		if (locale == null) {
			throw new DAOException("Argument locale is null");
		}
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_BY_ID_LOCALE);
			statement.setString(1, locale.getLanguage());
			statement.setInt(2, id);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				PaymentTemplate pTemplate = getPaymentTemplate(resultSet);
				return pTemplate;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<PaymentTemplate> findAll(Locale locale) throws DAOException {
		if (locale == null) {
			throw new DAOException("Argument locale is null");
		}
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_ALL);
			statement.setString(1, locale.getLanguage());
			resultSet = statement.executeQuery();
			List<PaymentTemplate> list = new ArrayList<PaymentTemplate>();
			while (resultSet.next()) {
				PaymentTemplate pTemplate = getPaymentTemplate(resultSet);
				list.add(pTemplate);
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public PaymentTemplate findByPaymentId(int id, Locale locale)
			throws DAOException {
		if (locale == null) {
			throw new DAOException("Argument locale is null");
		}
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection
					.prepareStatement(SQL_FIND_BY_PAYMENT_ID_LOCALE);
			statement.setString(1, locale.getLanguage());
			statement.setInt(2, id);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				PaymentTemplate pTemplate = getPaymentTemplate(resultSet);
				return pTemplate;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}

	/**
	 * Builds PaymentTemplate from result set.
	 * 
	 * @param resultSet
	 * @return PaymentTemplate objects
	 * @throws SQLException
	 */
	private PaymentTemplate getPaymentTemplate(ResultSet resultSet)
			throws SQLException {
		PaymentTemplate pTemplate = new PaymentTemplate();
		pTemplate.setId(resultSet.getInt(ID_PAYMENT_TEMPLATE));
		pTemplate.setCode(resultSet.getInt(CODE));
		pTemplate.setName(resultSet.getString(NAME));
		pTemplate.setDataDescription(resultSet.getString(DATA_DESCRIPTION));
		pTemplate.setUnp(resultSet.getString(UNP));
		pTemplate.setRegexp(resultSet.getString(REGEXP));
		pTemplate.setRegexpDescription(resultSet.getString(REGEXP_DESCRIPTION));
		return pTemplate;
	}
}