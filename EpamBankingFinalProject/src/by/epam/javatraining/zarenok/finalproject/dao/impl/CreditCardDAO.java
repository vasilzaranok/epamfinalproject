package by.epam.javatraining.zarenok.finalproject.dao.impl;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;

import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.dao.interfaces.ICreditCardDAO;
import by.epam.javatraining.zarenok.finalproject.entity.CreditCard;

/**
 * MySQL implementation of {@link ICreditCardDAO}.
 * 
 * @author Vasil Zaranok
 *
 */
public class CreditCardDAO extends AbstractDAO implements ICreditCardDAO {

	private static final String NUMBER = "number";
	private static final String EXPIRATION_DATE = "expirationDate";
	private static final String ID_CREDIT_CARD = "idCreditCard";
	private static final String SQL_FIND_BY_ACCOUNT_ID = "SELECT * "
			+ "FROM creditcard WHERE Account_idAccount=?";
	private static final String SQL_FIND_BY_CLIENT_ID = "SELECT *"
			+ " FROM creditcard WHERE Account_Client_idClient=?";
	private static final String SQL_FIND_BY_ID = "SELECT *"
			+ " FROM creditcard WHERE idCreditCard=?";
	private static final String SQL_FIND_BY_PAYMENT_ID = "SELECT * "
			+ "FROM creditcard WHERE idCreditCard="
			+ "(select payment.CreditCard_idCreditCard FROM payment "
			+ "WHERE idPayment=?)";
	private static final String SQL_INSERT_BY_ACCOUNT_ID = "INSERT INTO"
			+ " creditcard(number,expirationDate,Account_idAccount,"
			+ " Account_Client_idClient) value(?,?,?,"
			+ "(select Clients_idClient FROM account WHERE idAccount=?))";
	private static final String SQL_SELECT_ID = "SELECT idCreditCard"
			+ " FROM creditcard WHERE number=?";

	public CreditCardDAO(Connection connection) {
		super(connection);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CreditCard findById(int id) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_BY_ID);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				CreditCard card = new CreditCard();
				card.setId(resultSet.getInt(ID_CREDIT_CARD));
				card.setExpirationDate(resultSet.getDate(EXPIRATION_DATE));
				card.setNumber(resultSet.getString(NUMBER));
				return card;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CreditCard> findByAccountId(int id) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_BY_ACCOUNT_ID);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			List<CreditCard> list = new ArrayList<CreditCard>();
			while (resultSet.next()) {
				CreditCard card = new CreditCard();
				card.setId(resultSet.getInt(ID_CREDIT_CARD));
				card.setExpirationDate(resultSet.getDate(EXPIRATION_DATE));
				card.setNumber(resultSet.getString(NUMBER));
				list.add(card);
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<CreditCard> findByClientId(int id) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_BY_CLIENT_ID);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			List<CreditCard> list = new ArrayList<CreditCard>();
			while (resultSet.next()) {
				CreditCard card = new CreditCard();
				card.setId(resultSet.getInt(ID_CREDIT_CARD));
				card.setExpirationDate(resultSet.getDate(EXPIRATION_DATE));
				card.setNumber(resultSet.getString(NUMBER));
				list.add(card);
			}
			return list;
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public CreditCard findByPaymentId(int id) throws DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_FIND_BY_PAYMENT_ID);
			statement.setInt(1, id);
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				CreditCard card = new CreditCard();
				card.setId(resultSet.getInt(ID_CREDIT_CARD));
				card.setExpirationDate(resultSet.getDate(EXPIRATION_DATE));
				card.setNumber(resultSet.getString(NUMBER));
				return card;
			} else {
				return null;
			}
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public int add(CreditCard card, int accountId) throws DAOException {
		if (card == null) {
			throw new DAOException("Argument card is null");
		}
		PreparedStatement statement = null;
		try {
			statement = connection.prepareStatement(SQL_INSERT_BY_ACCOUNT_ID);
			statement.setString(1, card.getNumber());
			statement.setDate(2, card.getExpirationDate());
			statement.setInt(3, accountId);
			statement.setInt(4, accountId);
			statement.executeUpdate();
			return selectId(card);
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public boolean tryAdd(CreditCard card) throws DAOException {
		if (card == null) {
			throw new DAOException("Argument card is null");
		}
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_SELECT_ID);
			statement.setString(1, card.getNumber());
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				return false;
			} else {
				return true;
			}
		} catch (SQLException e) {
			throw new DAOException(ERROR_DESCRIPTION, e);
		} finally {
			close(statement);
			close(resultSet);
		}
	}

	/**
	 * Selects id of credit card.
	 * 
	 * @param card
	 *            to find id
	 * @return id of credit card
	 * @throws SQLException
	 * @throws DAOException
	 */
	private int selectId(CreditCard card) throws SQLException, DAOException {
		PreparedStatement statement = null;
		ResultSet resultSet = null;
		try {
			statement = connection.prepareStatement(SQL_SELECT_ID);
			statement.setString(1, card.getNumber());
			resultSet = statement.executeQuery();
			if (resultSet.next()) {
				int id = resultSet.getInt(ID_CREDIT_CARD);
				return id;
			} else {
				throw new DAOException("Card doesnt't exist : "
						+ card.getNumber());
			}
		} finally {
			close(statement);
			close(resultSet);
		}
	}
}