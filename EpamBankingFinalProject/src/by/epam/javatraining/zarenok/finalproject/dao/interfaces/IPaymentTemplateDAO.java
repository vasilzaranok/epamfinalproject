package by.epam.javatraining.zarenok.finalproject.dao.interfaces;

import java.util.List;
import java.util.Locale;

import by.epam.javatraining.zarenok.finalproject.dao.exception.DAOException;
import by.epam.javatraining.zarenok.finalproject.entity.PaymentTemplate;

/**
 * IPaymentTemplateDAO provides access to payment templates in database.
 * 
 * @author Vasil Zaranok
 *
 */
public interface IPaymentTemplateDAO {

	/**
	 * Finds payment template by its id.
	 * 
	 * @param id
	 *            id of payment template
	 * @param locale
	 *            locale to specify language of strings, that contains payment
	 *            template object
	 * @return {@link PaymentTemplate} object, represents payment template
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database or locale is null
	 */
	public PaymentTemplate findById(int id, Locale locale) throws DAOException;

	/**
	 * Finds all payment templates in database
	 * 
	 * @param locale
	 *            locale to specify language of strings, that contains payment
	 *            template objects
	 * @return list of {@link PaymentTemplate} objects, represents payment
	 *         templates(empty list if there no payment templates found)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database or if locale is null
	 */
	public List<PaymentTemplate> findAll(Locale locale) throws DAOException;

	/**
	 * Finds payment template by payment.
	 * 
	 * @param id
	 *            id of payment
	 * @param locale
	 *            locale to specify language of strings, that contains payment
	 *            template object
	 * @return {@link PaymentTemplate} object, represents payment template(null
	 *         if there no payment template found)
	 * @throws {@link DAOException} if error occurs while processing queries to
	 *         database or if locale is null
	 */
	public PaymentTemplate findByPaymentId(int id, Locale locale)
			throws DAOException;
}
