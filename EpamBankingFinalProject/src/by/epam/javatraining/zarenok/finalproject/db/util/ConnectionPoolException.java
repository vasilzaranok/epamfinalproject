package by.epam.javatraining.zarenok.finalproject.db.util;

/**
 * Thrown if errors occur while connection pooling.
 * @author Vasil Zaranok
 *
 */
public class ConnectionPoolException extends Exception {
	
private static final long serialVersionUID = 1L;
	
	public ConnectionPoolException() {	}

	public ConnectionPoolException(String message, Throwable cause) {
		super(message, cause);
	}

	public ConnectionPoolException(String message) {
		super(message);
	}

	public ConnectionPoolException(Throwable cause) {
		super(cause);
	}
}