package by.epam.javatraining.zarenok.finalproject.db.util;

import by.epam.javatraining.zarenok.finalproject.db.util.pool.own.ConnectionPool;

/**
 * ConnectionPoolManager allows control connection pooling without direct access
 * to pool
 * 
 * @author Vasil Zaranok
 *
 */
public class ConnectionPoolManager {

	private ConnectionPoolManager() {
	}

	/**
	 * Starts pool
	 */
	public static synchronized void startPool() {
		ConnectionPool.startPool();
	}

	/**
	 * Stop pool
	 */
	public static synchronized void stopPool() {
		ConnectionPool.stopPool();
	}

	/**
	 * 
	 * @return {@link IConnectionPool} proxy interface of pool
	 */
	public static IConnectionPool getPool() {
		return ConnectionPool.getInstance();
	}
}