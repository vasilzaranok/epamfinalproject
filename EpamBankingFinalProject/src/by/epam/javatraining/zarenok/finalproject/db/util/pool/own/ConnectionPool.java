package by.epam.javatraining.zarenok.finalproject.db.util.pool.own;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.Properties;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import by.epam.javatraining.zarenok.finalproject.db.util.ConnectionPoolException;
import by.epam.javatraining.zarenok.finalproject.db.util.IConnectionPool;
import by.epam.javatraining.zarenok.finalproject.resourceutil.DBConfigurationManager;

/**
 * ConnectionPool custom connection pool.
 * @author Vasil Zaranok
 *
 */
public class ConnectionPool implements IConnectionPool {

	private static ConnectionPool instance;
	private static volatile boolean instanceCreated;

	private static final String INTERRUPT_THREAD = "Interrupt thread";
	private static final String VALIDATION_QUERY = "SELECT 1";
	private static final long MAX_WAIT_FOR_GET_CONNECTION = Long
			.parseLong(DBConfigurationManager
					.getProperty("ownPool.maxWaitForGetConnection"));

	private BlockingQueue<ProxyConnection> idle;
	private BlockingQueue<ProxyConnection> busy;
	private AtomicInteger active = new AtomicInteger(0);
	private PoolProperties properties;
	private PoolCleaner cleaner;

	private final static Logger logger = LogManager
			.getLogger(ConnectionPool.class);;

	private ConnectionPool() {
		initProperties();
		busy = new ArrayBlockingQueue<ProxyConnection>(
				properties.getMaxActive(), false);
		idle = new ArrayBlockingQueue<ProxyConnection>(
				properties.getMaxActive(), false);

		int poolSize = properties.getInitialSize();
		if (poolSize > properties.getMaxActive()) {
			poolSize = properties.getMaxActive();
			properties.setInitialSize(poolSize);
		}
		if (properties.getMinIdle() > properties.getMaxActive()) {
			properties.setMinIdle(properties.getMaxActive());
		}
		for (int i = 0; i < poolSize; i++) {
			ProxyConnection connection = createConnection();
			offerToIdle(connection);
		}
		initCleaner();
	}

	public static ConnectionPool getInstance() {
		if (!instanceCreated) {
			throw new IllegalStateException(
					"Getting instance of pool before that starts");
		}
		return instance;
	}

	/**
	 * Start Connection Pool
	 * @throws IllegalStateException if pool already started
	 */
	public static synchronized void startPool() {
		if (!instanceCreated) {
			if (instance == null) {
				instance = new ConnectionPool();
				instanceCreated = true;
			} else {
				throw new IllegalStateException("Unable start pool");
			}
			logger.info("Connection pool started,"
					+ " number of idle connections : "
					+ instance.idle.size());
		} else {
			throw new IllegalStateException("Start pool that already created");
		}
	}

	/**
	 * Stop Connection Pool
	 * @throws IllegalStateException if pool doesn't created
	 */
	public static synchronized void stopPool() {
		if (instanceCreated) {
			instance.cleaner.stopCleaner();
			instance.closeAllRealConnections();
			instance = null;
			instanceCreated = false;
			logger.info("Connection pool stoped");
		} else {
			throw new IllegalStateException("Stop pool that doesn't created");
		}
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public Connection getConnection() throws ConnectionPoolException {
		ProxyConnection connection = null;
		long startTime = System.currentTimeMillis();
		long waitTime = 0;
		do {
			try {
				connection = takeFromIdle();
				checkMinIdle(startTime);
			} catch (InterruptedException e) {
				logger.warn(INTERRUPT_THREAD, new ConnectionPoolException(e));
			}
			if (validate(connection)) {
				long now = System.currentTimeMillis();
				connection.setTakingTime(now);
				return connection;
			}
			long now = System.currentTimeMillis();
			waitTime = now - startTime;
		} while (waitTime < MAX_WAIT_FOR_GET_CONNECTION);
		throw new ConnectionPoolException("Error while get connection!");
	}

	/**
	 * Checks abandoned connections and return they to idle queue.
	 */
	void checkAbandoned() {
		logger.debug("busy: " + busy.size() + " idle: " + idle.size()
				+ "active: " + active.get());
		if (busy.size() == 0) {
			return;
		}
		Iterator<ProxyConnection> iterator = busy.iterator();
		while (iterator.hasNext()) {
			ProxyConnection connection = iterator.next();
			if (idle.contains(connection))
				continue;
			long takingTime = connection.getTakingTime();
			long now = System.currentTimeMillis();
			long busyTime = now - takingTime;
			if (busyTime > properties.getRemoveAbandonedTimeout()) {
				if (validate(connection)) {
					offerToIdle(connection);
				}
				iterator.remove();
				logger.warn("Remove Abandoned connection!");
			}
		}
	}

	/**
	 * Returns connection from user after using them.
	 * @param connection to return
	 */
	void returnConnection(ProxyConnection connection) {
		if (connection == null) {
			return;
		}
		try {
			connection.setAutoCommit(true);
		} catch (SQLException e) {
			closeRealConnection(connection);
			logger.warn("Unable set auto commit", e);
			return;
		}
		busy.remove(connection);
		if (validate(connection)) {
			offerToIdle(connection);
		}
	}
	
	/**
	 * Checks minimum number idle connections and create new if needed.
	 * @param startTime
	 */
	private void checkMinIdle(long startTime) {
		long wait = System.currentTimeMillis() - startTime;
		while (idle.size() < properties.getMinIdle()
				&& wait < MAX_WAIT_FOR_GET_CONNECTION) {
			ProxyConnection connection = createConnection();
			if (connection != null) {
				offerToIdle(connection);
			}
			wait = System.currentTimeMillis() - startTime;
		}
	}

	PoolProperties getProperties() {
		return properties;
	}

	private void offerToIdle(ProxyConnection connection) {
		if (!idle.offer(connection)) {
			closeRealConnection(connection);
		}
	}

	private ProxyConnection takeFromIdle() throws InterruptedException {
		ProxyConnection connection = idle.take();
		if (!busy.offer(connection)) {
			offerToIdle(connection);
			return null;
		}
		return connection;
	}
	
	/**
	 * Init cleaner of abandoned connections.
	 */
	private void initCleaner() {
		cleaner = new PoolCleaner(this);
		cleaner.startCleaner();
	}
	
	/**
	 * Create connection with database.
	 * @return ProxyConnection object
	 */
	private ProxyConnection createConnection() {
		if (active.get() >= properties.getMaxActive()) {
			return null;
		}
		try {
			Class.forName(properties.getDriverClassName());
			Properties prop = new Properties();
			prop.put("user", properties.getUsername());
			prop.put("password", properties.getPassword());
			prop.put("autoReconnect", "true");
			prop.put("userUnicode", "true");
			prop.put("characterEncoding", "UTF-8");
			Connection connection = DriverManager.getConnection(
					properties.getUrl(), prop);
			if (connection != null) {
				active.incrementAndGet();
				return new ProxyConnection(connection, this);
			}
			return null;
		} catch (ClassNotFoundException | SQLException e) {
			logger.error("Cannot create connection : ",
					new ConnectionPoolException(e));
		}
		return null;
	}
	
	/**
	 * Validates connection and close it if that not valid.
	 * @param connection to validate
	 * @return
	 */
	private boolean validate(ProxyConnection connection) {
		if (connection == null)
			return false;
		try {
			PreparedStatement statement = connection
					.prepareStatement(VALIDATION_QUERY);
			statement.executeQuery();
			statement.close();
			return true;
		} catch (SQLException e) {
			logger.warn("Find not valid connection",
					new ConnectionPoolException(e));
			closeRealConnection(connection);
			return false;
		}
	}
	
	/**
	 * Close connection with database.
	 * @param connection to close
	 */
	private void closeRealConnection(ProxyConnection connection) {
		try {
			if (connection != null) {
				active.decrementAndGet();
				connection.closeRealConnection();
			}
		} catch (SQLException e) {
			logger.warn("Failed to close connection",
					new ConnectionPoolException(e));
		}
	}
	
	/**
	 * Close all connections with database.
	 */
	private void closeAllRealConnections() {
		for (Iterator<ProxyConnection> iterator = idle.iterator(); iterator
				.hasNext();) {
			ProxyConnection connection = iterator.next();
			closeRealConnection(connection);
			iterator.remove();
		}
		for (Iterator<ProxyConnection> iterator = busy.iterator(); iterator
				.hasNext();) {
			ProxyConnection connection = iterator.next();
			closeRealConnection(connection);
			iterator.remove();
		}
	}

	/**
	 * Initialize pool properties
	 */
	private void initProperties() {
		properties = new PoolProperties();
		properties.setDriverClassName(DBConfigurationManager
				.getProperty("db.driverClassName"));
		String url = DBConfigurationManager.getProperty("db.url");
		properties.setUrl(url);
		String username = DBConfigurationManager.getProperty("db.username");
		properties.setUsername(username);
		String password = DBConfigurationManager.getProperty("db.password");
		properties.setPassword(password);

		int initialSize = Integer.parseInt(DBConfigurationManager
				.getProperty("ownPool.initialSize"));
		properties.setInitialSize(initialSize);
		int maxActive = Integer.parseInt(DBConfigurationManager
				.getProperty("ownPool.maxActive"));
		properties.setMaxActive(maxActive);
		int minIdle = Integer.parseInt(DBConfigurationManager
				.getProperty("ownPool.minIdle"));
		properties.setMinIdle(minIdle);
		int timeBetweenEvictionRunsMillis = Integer
				.parseInt(DBConfigurationManager
						.getProperty("ownPool.timeBetweenEvictionRunsMillis"));
		properties
				.setTimeBetweenEvictionRunsMillis(timeBetweenEvictionRunsMillis);
		int removeAbandonedTimeout = Integer.parseInt(DBConfigurationManager
				.getProperty("ownPool.removeAbandonedTimeout"));
		properties.setRemoveAbandonedTimeout(removeAbandonedTimeout);
	}
}