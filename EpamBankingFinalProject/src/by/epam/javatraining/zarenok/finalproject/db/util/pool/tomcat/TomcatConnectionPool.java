package by.epam.javatraining.zarenok.finalproject.db.util.pool.tomcat;

import java.sql.Connection;
import java.sql.SQLException;

import org.apache.tomcat.jdbc.pool.DataSource;
import org.apache.tomcat.jdbc.pool.PoolProperties;

import by.epam.javatraining.zarenok.finalproject.db.util.ConnectionPoolException;
import by.epam.javatraining.zarenok.finalproject.db.util.IConnectionPool;
import by.epam.javatraining.zarenok.finalproject.resourceutil.DBConfigurationManager;

/**
 * Apache Tomcat connection pool.
 * @author Vasil Zaranok
 *
 */
public enum TomcatConnectionPool implements IConnectionPool {
	INSTANCE;

	private DataSource datasource;

	private TomcatConnectionPool() {
		PoolProperties p = new PoolProperties();
		String url = DBConfigurationManager.getProperty("db.url");
		p.setUrl(url);
		String driverClassName = DBConfigurationManager
				.getProperty("db.driverClassName");
		p.setDriverClassName(driverClassName);
		p.setUsername(DBConfigurationManager.getProperty("db.username"));
		p.setPassword(DBConfigurationManager.getProperty("db.password"));
		int maxActive = Integer.parseInt(DBConfigurationManager
				.getProperty("tomcatPool.maxActive"));
		p.setMaxActive(maxActive);
		int minIdle = Integer.parseInt(DBConfigurationManager
				.getProperty("tomcatPool.minIdle"));
		p.setMinIdle(minIdle);
		int time = Integer.parseInt(DBConfigurationManager
				.getProperty("tomcatPool.timeBetweenEvictionRunsMillis"));
		p.setTimeBetweenEvictionRunsMillis(time);
		int abandonedTimeout = Integer.parseInt(DBConfigurationManager
				.getProperty("tomcatPool.removeAbandonedTimeout"));
		p.setRemoveAbandonedTimeout(abandonedTimeout);
		int initialSize = Integer.parseInt(DBConfigurationManager
				.getProperty("tomcatPool.initialSize"));
		p.setInitialSize(initialSize);

		p.setJmxEnabled(true);
		p.setTestWhileIdle(false);
		p.setTestOnBorrow(true);
		p.setValidationQuery("SELECT 1");
		p.setTestOnReturn(false);
		p.setValidationInterval(30000);
		p.setMaxWait(10000);
		p.setMinEvictableIdleTimeMillis(30000);
		p.setLogAbandoned(true);
		p.setRemoveAbandoned(true);
		p.setJdbcInterceptors("org.apache.tomcat.jdbc.pool.interceptor.ConnectionState;"
				+ "org.apache.tomcat.jdbc.pool.interceptor.StatementFinalizer");
		datasource = new DataSource();
		datasource.setPoolProperties(p);
	}

	@Override
	public Connection getConnection() throws ConnectionPoolException {
		try {
			return datasource.getConnection();
		} catch (SQLException e) {
			throw new ConnectionPoolException(
					"Error while get connection from pool", e);
		}
	}
}
