package by.epam.javatraining.zarenok.finalproject.db.util;

import java.sql.Connection;

/**
 * Proxy interface of pool. Allows get connections from pool.
 * 
 * @author Vasil Zaranok
 *
 */
public interface IConnectionPool {

	/**
	 * Gets connection from pool.
	 * 
	 * @return pooled connection with database
	 * @throws ConnectionPoolException
	 *             if error occurs while connection pooling
	 */
	public Connection getConnection() throws ConnectionPoolException;
}
