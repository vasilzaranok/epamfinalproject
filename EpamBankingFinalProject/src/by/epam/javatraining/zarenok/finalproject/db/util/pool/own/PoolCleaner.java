package by.epam.javatraining.zarenok.finalproject.db.util.pool.own;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * PoolCleaner checks abandoned connections and return it to idle state.
 * @author Vasil Zaranok
 *
 */
class PoolCleaner {

	private volatile boolean stopCleaner = false;

	private ConnectionPool pool;
	private long timeToSleep;
	private ExecutorService executor = Executors.newSingleThreadExecutor();
	private CountDownLatch countDown = new CountDownLatch(1);

	private Logger logger = LogManager.getLogger(PoolCleaner.class);

	PoolCleaner(ConnectionPool connectionPool) {
		pool = connectionPool;
		timeToSleep = pool.getProperties().getTimeBetweenEvictionRunsMillis();
	}
	
	/**
	 * Start cleaner.
	 */
	void startCleaner() {
		executor.execute(new Runnable() {
			public void run() {
				while (!stopCleaner) {
					try {
						countDown.await(timeToSleep, TimeUnit.MILLISECONDS);
					} catch (InterruptedException e) {
						logger.error("Cleaner terminated!");
						Thread.currentThread().interrupt();
					}
					if (!stopCleaner) {
						logger.debug("Cleaner works");
						pool.checkAbandoned();
					}
				}
			}
		});
	}
	
	/**
	 * Stop cleaner.
	 */
	void stopCleaner() {
		stopCleaner = true;
		countDown.countDown();
		executor.shutdown();
		try {
			if (!executor.awaitTermination(2, TimeUnit.SECONDS)) {
				executor.shutdownNow();
				if (!executor.awaitTermination(2, TimeUnit.SECONDS)) {
					logger.error("Cleaner did not terminate");
				}
			}
		} catch (InterruptedException ie) {
			executor.shutdownNow();
			Thread.currentThread().interrupt();
		}
	}
}
