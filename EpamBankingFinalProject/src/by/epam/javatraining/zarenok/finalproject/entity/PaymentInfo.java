package by.epam.javatraining.zarenok.finalproject.entity;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.Calendar;

/**
 * Payment represents general(sum, time and data) information about payment,
 * that client made, without information about credit card and other.
 * 
 * @author Vasil Zaranok
 *
 */
public class PaymentInfo extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8826626012222332708L;
	private BigDecimal sum;
	private Date paymentTime;
	private String paymentData;

	public BigDecimal getSum() {
		return sum;
	}

	public void setSum(BigDecimal sum) {
		this.sum = sum;
	}

	public Date getPaymentTime() {
		return paymentTime;
	}

	public void setPaymentTime(Date paymentTime) {
		this.paymentTime = paymentTime;
	}

	public String getPaymentData() {
		return paymentData;
	}

	public void setPaymentData(String paymentData) {
		this.paymentData = paymentData;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((paymentData == null) ? 0 : paymentData.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentInfo other = (PaymentInfo) obj;
		if (paymentData == null) {
			if (other.paymentData != null)
				return false;
		} else if (!paymentData.equals(other.paymentData))
			return false;
		if (sum == null) {
			if (other.sum != null)
				return false;
		} else if (sum.compareTo(other.sum) != 0)
			return false;
		if (paymentTime == null) {
			if (other.paymentTime != null)
				return false;
		} else if (!equalsDate(other))
			return false;
		return true;
	}

	private boolean equalsDate(PaymentInfo other) {
		Calendar calendarThis = Calendar.getInstance();
		calendarThis.setTimeInMillis(paymentTime.getTime());
		Calendar calendarOther = Calendar.getInstance();
		calendarOther.setTimeInMillis(other.getPaymentTime().getTime());
		if (calendarThis.get(Calendar.YEAR) != calendarOther.get(Calendar.YEAR)
				|| calendarThis.get(Calendar.MONTH) != calendarOther
						.get(Calendar.MONTH)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "PaymentInfo [sum=" + sum + ", paymentTime=" + paymentTime
				+ ", paymentData=" + paymentData + ", id=" + id + "]";
	}
	
	@Override
	public Object clone() {
		PaymentInfo copy = (PaymentInfo) super.clone();
		if (paymentTime != null) {
			copy.paymentTime = (Date) paymentTime.clone();
		}
		return copy;
	}
}
