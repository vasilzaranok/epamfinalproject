package by.epam.javatraining.zarenok.finalproject.entity;

import java.io.Serializable;

/**
 * Payment represents all information about payment, that client made.
 * @author Vasil Zaranok
 *
 */
public class Payment implements Serializable, Cloneable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2443683998112104366L;
	private CreditCard creditCard;
	private PaymentInfo paymentInfo;
	private PaymentTemplate paymentTemplate;
	
	public PaymentTemplate getPaymentTemplate() {
		return paymentTemplate;
	}

	public void setPaymentTemplate(PaymentTemplate paymentTemplate) {
		this.paymentTemplate = paymentTemplate;
	}

	public CreditCard getCreditCard() {
		return creditCard;
	}
	
	public void setCreditCard(CreditCard creditCard) {
		this.creditCard = creditCard;
	}
	
	public PaymentInfo getPaymentInfo() {
		return paymentInfo;
	}

	public void setPaymentInfo(PaymentInfo paymentInfo) {
		this.paymentInfo = paymentInfo;
	}

	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((creditCard == null) ? 0 : creditCard.hashCode());
		result = prime * result
				+ ((paymentInfo == null) ? 0 : paymentInfo.hashCode());
		result = prime * result
				+ ((paymentTemplate == null) ? 0 : paymentTemplate.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Payment other = (Payment) obj;
		if (creditCard == null) {
			if (other.creditCard != null)
				return false;
		} else if (!creditCard.equals(other.creditCard))
			return false;
		if (paymentInfo == null) {
			if (other.paymentInfo != null)
				return false;
		} else if (!paymentInfo.equals(other.paymentInfo))
			return false;
		if (paymentTemplate == null) {
			if (other.paymentTemplate != null)
				return false;
		} else if (!paymentTemplate.equals(other.paymentTemplate))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Payment [creditCard=" + creditCard + ", paymentInfo="
				+ paymentInfo + ", paymentTemplate=" + paymentTemplate + "]";
	}
	
	@Override
	public Object clone() {
		Payment copy = null;
		try {
			copy = (Payment) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new AssertionError(e);
		}
		if (creditCard != null) {
			copy.creditCard = (CreditCard) creditCard.clone();
		}
		if (paymentInfo != null) {
			copy.paymentInfo = (PaymentInfo) paymentInfo.clone();
		}
		if (paymentTemplate != null) {
			copy.paymentTemplate = (PaymentTemplate) paymentTemplate.clone();
		}
		return copy;
	}
}
