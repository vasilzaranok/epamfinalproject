package by.epam.javatraining.zarenok.finalproject.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Account represents all information about client account and credit
 * cards linked to this account.
 * 
 * @author Vasil Zaranok
 *
 */
public class Account implements Serializable, Cloneable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3075482891572043449L;
	private AccountInfo accountInfo;
	private List<CreditCard> creditCards;
	
	/**
	 * Constructs an empty account object 
	 */
	public Account() {
	}
	
	public AccountInfo getAccountInfo() {
		return accountInfo;
	}

	public void setAccountInfo(AccountInfo accountInfo) {
		this.accountInfo = accountInfo;
	}

	public List<CreditCard> getCreditCards() {
		return creditCards;
	}

	public void setCreditCards(List<CreditCard> creditCards) {
		this.creditCards = creditCards;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((accountInfo == null) ? 0 : accountInfo.hashCode());
		result = prime * result
				+ ((creditCards == null) ? 0 : creditCards.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Account other = (Account) obj;
		if (accountInfo == null) {
			if (other.accountInfo != null)
				return false;
		} else if (!accountInfo.equals(other.accountInfo))
			return false;
		if (creditCards == null) {
			if (other.creditCards != null)
				return false;
		} else if (!creditCards.equals(other.creditCards))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Account [accountInfo=" + accountInfo + ", creditCards="
				+ creditCards + "]";
	}
	
	@Override
	public Object clone() {
		Account copy;
		try {
			copy = (Account) super.clone();
		} catch (CloneNotSupportedException e) {
			throw new AssertionError(e);
		}
		if (accountInfo != null) {
			copy.accountInfo = (AccountInfo) accountInfo.clone();
		}
		if (creditCards != null) {
			copy.creditCards = new ArrayList<CreditCard>(creditCards);
		}
		return copy;
	}
}
