package by.epam.javatraining.zarenok.finalproject.entity;

import java.sql.Date;
import java.util.Calendar;

/**
 * CreditCard represents information(number and expiration date) about client
 * credit card.
 * 
 * @author Vasil Zaranok
 *
 */
public class CreditCard extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8773947414144078767L;
	private String number;
	private Date expirationDate;

	public CreditCard() {
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public Date getExpirationDate() {
		return expirationDate;
	}

	public void setExpirationDate(Date expirationDate) {
		this.expirationDate = expirationDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CreditCard other = (CreditCard) obj;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		if (expirationDate == null) {
			if (other.expirationDate != null)
				return false;
		} else if (!equalsDate(other))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CreditCard [number=" + number + ", expirationDate="
				+ expirationDate + ", id=" + id + "]";
	}

	private boolean equalsDate(CreditCard other) {
		Calendar calendarThis = Calendar.getInstance();
		calendarThis.setTimeInMillis(expirationDate.getTime());
		Calendar calendarOther = Calendar.getInstance();
		calendarOther.setTimeInMillis(other.getExpirationDate().getTime());
		if (calendarThis.get(Calendar.YEAR) != calendarOther.get(Calendar.YEAR)
				|| calendarThis.get(Calendar.MONTH) != calendarOther
						.get(Calendar.MONTH)) {
			return false;
		}
		return true;
	}
	
	@Override
	public Object clone() {
		CreditCard copy = (CreditCard) super.clone();
		if (expirationDate != null) {
			copy.expirationDate = (Date) expirationDate.clone();
		}
		return copy;
	}
}
