package by.epam.javatraining.zarenok.finalproject.entity;

/**
 * PaymentTemplate represents information about payment template. Using payment
 * template clients may make payments
 * 
 * @author Vasil Zaranok
 *
 */
public class PaymentTemplate extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 319329486285320772L;
	private String name;
	private String unp;
	private int code;
	private String dataDescription;
	private String regexp;
	private String regexpDescription;

	public String getRegexpDescription() {
		return regexpDescription;
	}

	public void setRegexpDescription(String regexpDescription) {
		this.regexpDescription = regexpDescription;
	}

	public String getDataDescription() {
		return dataDescription;
	}

	public void setDataDescription(String dataDescription) {
		this.dataDescription = dataDescription;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUnp() {
		return unp;
	}

	public void setUnp(String unp) {
		this.unp = unp;
	}

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getRegexp() {
		return regexp;
	}

	public void setRegexp(String regexp) {
		this.regexp = regexp;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + code;
		result = prime * result
				+ ((dataDescription == null) ? 0 : dataDescription.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((regexp == null) ? 0 : regexp.hashCode());
		result = prime
				* result
				+ ((regexpDescription == null) ? 0 : regexpDescription
						.hashCode());
		result = prime * result + ((unp == null) ? 0 : unp.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		PaymentTemplate other = (PaymentTemplate) obj;
		if (code != other.code)
			return false;
		if (dataDescription == null) {
			if (other.dataDescription != null)
				return false;
		} else if (!dataDescription.equals(other.dataDescription))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (regexp == null) {
			if (other.regexp != null)
				return false;
		} else if (!regexp.equals(other.regexp))
			return false;
		if (regexpDescription == null) {
			if (other.regexpDescription != null)
				return false;
		} else if (!regexpDescription.equals(other.regexpDescription))
			return false;
		if (unp == null) {
			if (other.unp != null)
				return false;
		} else if (!unp.equals(other.unp))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PaymentTemplate [name=" + name + ", unp=" + unp + ", code="
				+ code + ", dataDescription=" + dataDescription + ", regexp="
				+ regexp + ", regexpDescription=" + regexpDescription + ", id="
				+ id + "]";
	}
	
	@Override
	public Object clone() {
		return super.clone();
	}
}
