package by.epam.javatraining.zarenok.finalproject.entity;

/**
 * Role of user in payment system. 
 * @author Vasil Zaranok
 *
 */
public enum Role {
	
	/**
	 * Administrator role
	 */
	ADMIN,
	
	/**
	 * Client role
	 */
	CLIENT,
	
	/**
	 * Guest role
	 */
	GUEST
}
