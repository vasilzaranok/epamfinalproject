package by.epam.javatraining.zarenok.finalproject.entity;

import java.math.BigDecimal;

/**
 * AccountInfo represents general information(number, balance and blocking
 * status) about client account, without information about credit cards.
 * 
 * @author Vasil Zaranok
 *
 */
public class AccountInfo extends Entity {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7112005949138140710L;
	private String number;
	private boolean blocked;
	private BigDecimal balance;

	public AccountInfo() {
	}

	public String getNumber() {
		return number;
	}

	public void setNumber(String number) {
		this.number = number;
	}

	public boolean isBlocked() {
		return blocked;
	}

	public void setBlocked(boolean blocked) {
		this.blocked = blocked;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((balance == null) ? 0 : balance.hashCode());
		result = prime * result + (blocked ? 1231 : 1237);
		result = prime * result + ((number == null) ? 0 : number.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		AccountInfo other = (AccountInfo) obj;
		if (balance == null) {
			if (other.balance != null)
				return false;
		} else if (!balance.equals(other.balance))
			return false;
		if (blocked != other.blocked)
			return false;
		if (number == null) {
			if (other.number != null)
				return false;
		} else if (!number.equals(other.number))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Account [number=" + number + ", blocked=" + blocked
				+ ", balance=" + balance + ", id=" + id + "]";
	}
	
	@Override
	public Object clone() {
		return super.clone();
	}
}
